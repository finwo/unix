Newsgroups: news.software.b
Subject: C News patch CR.C

This is the second part of a two-part patch, the first being CR.B, for
the Cleanup Release of C News.  The distribution files
on ftp.cs.toronto.edu and zoo.toronto.edu have been updated to match.
See the README.changes diff below for what's been done.

start of patch CR.C
(suggested archive name:  patchCR.C)
apply with   patch -p0 <thisfile

Prereq: CR.B
*** README.mastercopy	Sun Nov 27 20:36:32 1994
--- README	Sun Nov 27 20:16:16 1994
***************
*** 1,4 ****
! Cleanup Release of C News, with patch CR.B (which needs .C).	Nov 1994
  
  The current C News distribution can be retrieved by anonymous FTP from
  ftp.cs.toronto.edu (file pub/c-news/c-news.tar.Z) or ftp.zoo.toronto.edu
--- 1,4 ----
! Cleanup Release of C News, with patch CR.C.			Nov 1994
  
  The current C News distribution can be retrieved by anonymous FTP from
  ftp.cs.toronto.edu (file pub/c-news/c-news.tar.Z) or ftp.zoo.toronto.edu



*** README.changes.mastercopy	Sun Nov 27 20:36:33 1994
--- README.changes	Sun Nov 27 20:35:32 1994
***************
*** 1,3 ****
--- 1,20 ----
+ High points of patch CR.C:
+ This is the second part of a two-part patch, the first being CR.B.  Install
+ both parts, this one second.  In this part...  A nasty bug in newsgroup
+ removal (which could wipe out a number of files in NEWSCTL) has been fixed.
+ There is a new command, checkactive, which does a fairly rigorous check of
+ the contents of the active file; several commands which modify the active
+ file now use checkactive to check their new version before installing it.
+ Mergeactive now adds directories for the new newsgroups.  The badexpiry
+ command, which looks for newsgroups with many articles with long expiry
+ dates, has been improved and is now documented.  The newsdb(5) manpage
+ now warns reader implementors about non-atomic updates.  Postnews retains
+ the user's PATH for use within the editor.  dostatfs.c has been made a bit
+ more portable (I hope), or at least easier to fix when it does break --
+ the statfs() system call is not very portable, it turns out.  queuelen.old
+ has been renamed queuelen.vo for internal reasons.  Plus the usual minor
+ cleanups and fixes.
+ 
  High points of patch CR.B:
  This is the first part of a two-part patch, the second being CR.C.  Install
  both parts, this one first.  In this part...  The viauux -g option now accepts



*** conf/versionname.mastercopy	Sun Nov 27 20:36:34 1994
--- conf/versionname	Sun Nov 27 20:16:25 1994
***************
*** 1 ****
! Cleanup Release, with patch CR.B but not .C
--- 1 ----
! Cleanup Release, with patch CR.C



*** maint/mergeactive.mastercopy	Sun Nov 27 20:36:34 1994
--- maint/mergeactive	Thu Nov 10 00:05:35 1994
***************
*** 34,59 ****
  cat $* >$tmp
  
  # do some checking
! awk 'NF != 4 || $1 ~ /\// || $4 !~ /^([ynmx]|=[^\/]+)$/' $tmp >$err
  if test -s $err
  then
! 	echo "$0: `wc -l <$err` bad lines in input" >&2
  	exit
  fi
- awk '{ print $1 }' $tmp | sort | uniq -d >$err
- if test -s $err
- then
- 	echo "$0: `wc -l <$err` duplicate newsgroups in input" >&2
- 	exit
- fi
- awk '$4 ~ /^=/ { print substr($4, 2) }' $tmp | sort -u >$tmpb
- awk '{ print $1 }' active $tmp | sort -u | comm -13 - $tmpb >$err
- if test -s $err
- then
- 	echo "$0: `wc -l <$err` groups are =ed to nonexistent groups" >&2
- 	exit
- fi
- awk '{ print $1, $4 }' $tmp | sort >$tmpb
  
  # now we need to make sure things don't change underfoot...
  lock LOCK $$ 10 || exit
--- 34,46 ----
  cat $* >$tmp
  
  # do some checking
! checkactive -q $tmp >$err 2>&1
  if test -s $err
  then
! 	echo "$0: problems in input:" >&2
! 	cat $err >&2
  	exit
  fi
  
  # now we need to make sure things don't change underfoot...
  lock LOCK $$ 10 || exit
***************
*** 60,73 ****
  locks=LOCK
  
  # let's be paranoid...
! awk 'NF != 4' active >$err
  if test -s $err
  then
! 	echo "$0: $NEWSCTL/active contains bad lines!" >&2
  	exit
  fi
  
  # pick out the new ones
  sort active | join -a2 - $tmpb | awk 'NF == 2' >$tmp
  
  # and away we go...
--- 47,62 ----
  locks=LOCK
  
  # let's be paranoid...
! checkactive -q active >$err 2>&1
  if test -s $err
  then
! 	echo "$0: problems in $NEWSCTL/active:" >&2
! 	cat $err >&2
  	exit
  fi
  
  # pick out the new ones
+ awk '{ print $1, $4 }' $tmp | sort >$tmpb
  sort active | join -a2 - $tmpb | awk 'NF == 2' >$tmp
  
  # and away we go...
***************
*** 89,94 ****
--- 78,86 ----
  mv active active.old || exit
  mv active.tmp active || exit
  mv active.times active.times.o && mv actimes.tmp active.times
+ 
+ # and add any necessary directories
+ adddirs
  
  # looks like it worked
  status=0



*** maint/badexpiry.mastercopy	Sun Nov 27 20:36:35 1994
--- maint/badexpiry	Tue Oct 18 21:52:11 1994
***************
*** 1,7 ****
  #! /bin/sh
! # Reports groups with more than 25 articles
! # or 250KB of articles with expiry dates more than 10 days after their
! # arrival dates.
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
--- 1,7 ----
  #! /bin/sh
! # Reports groups with more than $1 articles, >90% of all articles, or
! # or >$2KB of articles with expiry dates more than 10 days after their
! # arrival dates.  Defaults 15 and 500.
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
***************
*** 8,22 ****
  PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
  umask $NEWSUMASK
  
  cd $NEWSARTS
! egrep '[0-9]	' $NEWSCTL/history |
! 	awk -F'	' '{
! 			n = split($2, f, "~")
! 			if (f[2] != "-" && f[2]-f[1] > 864000) {
! 				n = split($3, f, " ")
! 				for (i = 1; i <= n; i++)
! 					print f[i]
! 			}
  	}' | tr . / | xargs $NEWSBIN/sizeof -i | sed 's;/[^/]* ; ;' |
  	( sort ; echo dummy ) |
  	awk '{
--- 8,28 ----
  PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
  umask $NEWSUMASK
  
+ case "$#" in
+ 0)	minarts=15 ; minsize=500	;;
+ 2)	minarts=$1 ; minsize=$2		;;
+ *)	echo "Usage: $0 [minarts minsize]" >&2 ; exit 2	;;
+ esac
+ 
  cd $NEWSARTS
! egrep -v '~-' $NEWSCTL/history |
! 	awk -F'	' 'NF > 2 {
! 		n = split($2, f, "~")
! 		if (f[2] != "-" && f[2]-f[1] > 864000) {
! 			n = split($3, f, " ")
! 			for (i = 1; i <= n; i++)
! 				print f[i]
! 		}
  	}' | tr . / | xargs $NEWSBIN/sizeof -i | sed 's;/[^/]* ; ;' |
  	( sort ; echo dummy ) |
  	awk '{
***************
*** 29,32 ****
  		}
  		n++
  		size += $2
! 	}' | tr / . | awk '$2 > 25 || $3 > 250000'
--- 35,56 ----
  		}
  		n++
  		size += $2
! 	}' |
! 	while read group narts size
! 	do
! 		npresent=`ls -f $group | egrep '^[0-9]+$' | wc -l`
! 		echo $group $narts $npresent $size
! 	done | tr / . |
! 	awk '	{ bad = "" }
! 		$2 > '$minarts' {
! 			bad = bad ", " $2 " articles"
! 		}
! 		$2 > 0.9*$3 {
! 			bad = bad ", " $2 " out of " $3 " articles"
! 		}
! 		$4 > '$minsize'*1024 {
! 			bad = bad ", " int(($4+512)/1024) "KB"
! 		}
! 		bad != "" {
! 			print $1, "has", substr(bad, 3), "on long expiry"
! 		}'



*** maint/newsdaily.mastercopy	Sun Nov 27 20:36:35 1994
--- maint/newsdaily	Sun Nov 27 17:09:47 1994
***************
*** 86,91 ****
--- 86,102 ----
  	) >>$gripes
  fi
  
+ # do a quick check of the active file
+ checkactive -q >$tmp
+ if test -s $tmp
+ then
+ 	(
+ 		echo 'checkactive reports problems in active file:'
+ 		cat $tmp
+ 		echo
+ 	) >>$gripes
+ fi
+ 
  # sweep log file, once, for suspect lines
  egrep '`
  ancient
***************
*** 158,167 ****
--- 169,183 ----
  
  # and do other daily chores
  $NEWSBIN/relay/dodelayed
+ 
  if test -s dirs.tbd
  then
  	while read dir
  	do
+ 		if test ! -d $NEWSARTS/$dir
+ 		then
+ 			continue		# NOTE CONTINUE
+ 		fi
  		cd $NEWSARTS/$dir
  		if test " `ls | egrep '^[0-9]+$' | wc -l`" -gt 0
  		then
***************
*** 186,190 ****
--- 202,207 ----
  			done
  		fi
  	done <dirs.tbd >dirs.ttd
+ 	cd $NEWSCTL
  	mv dirs.ttd dirs.tbd
  fi



*** maint/checkactive.mastercopy	Sun Nov 27 20:36:36 1994
--- maint/checkactive	Mon Nov 14 16:57:13 1994
***************
*** 0 ****
--- 1,106 ----
+ #! /bin/sh
+ # checkactive - check an active file for worrisome signs
+ # checkactive file
+ 
+ # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
+ . ${NEWSCONFIG-/etc/news/bin/config}
+ 
+ PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
+ umask $NEWSUMASK
+ 
+ quick=n
+ for dummy
+ do
+ 	case "$1" in
+ 	-q)	quick=y		;;
+ 	--)	shift ; break	;;
+ 	-*)	echo "$0: unknown option \`$1'" >&2 ; exit 2	;;
+ 	*)	break		;;
+ 	esac
+ 	shift
+ done
+ case $# in
+ 0)	file=$NEWSCTL/active	;;
+ 1)	file=$1			;;
+ *)	echo "Usage: $0 file" >&2 ; exit 2	;;
+ esac
+ 
+ tmp=/tmp/ca$$
+ tmpb=/tmp/cb$$
+ status=1
+ trap 'rm -f $tmp $tmpb ; trap 0 ; exit $status' 0 1 2 15
+ 
+ if test ! -s $file
+ then
+ 	echo "input is empty"
+ 	exit
+ fi
+ 
+ awk 'BEGIN { status = 0 ; q = "\"" }
+ { where = "line " NR ":" }
+ NF != 4 { print where, "has", NF, "not 4 fields" ; status = 1 }
+ $1 !~ /^[a-z][a-z0-9+_-]*(\.([a-z0-9+_-]+|=\?([a-zA-Z0-9=.?+_-]|\/)*\?=))*$/ {
+ 	# not fully rigorous, but a good first cut
+ 	print where, "newsgroup name", q $1 q, "invalid" ; status = 1
+ }
+ $2 !~ /^[0-9]+$/ { print where, q $2 q, "is non-numeric" ; status = 1 }
+ $3 !~ /^[0-9]+$/ { print where, q $3 q, "is non-numeric" ; status = 1 }
+ $4 !~ /^([ynmx]|=.*)$/ {
+ 	print where, "field 4 (" q $4 q ") invalid" ; status = 1
+ }
+ 0+$3 < 1 { print where, "field 3 (" q $3 q ") zero or negative" ; status = 1 }
+ 1+$2 < 0+$3 { print where, q $2 q, "<", q $3 q ; status = 1 }
+ END { exit status }' $file || exit
+ 
+ awk '{print $1}' $file | sort | uniq -d >$tmp
+ if test -s $tmp
+ then
+ 	echo `wc -l <$tmp` "duplicate newsgroup name(s):"
+ 	cat $tmp
+ 	exit
+ fi
+ 
+ awk '$4 ~ /^=/ { print substr($4, 2) }' $file | sort -u >$tmp
+ awk '{ print $1 }' $file | sort -u | comm -13 - $tmp >$tmpb
+ if test -s $tmpb
+ then
+ 	echo `wc -l <$tmpb` "newsgroup(s) are =ed to nonexistent groups:"
+ 	cat $tmpb
+ 	exit
+ fi
+ 
+ me="`newshostname`"
+ gngppat=`awk -f $NEWSBIN/canonsys.awk $NEWSCTL/sys |
+ 	egrep "^($me|ME)[:/]" |
+ 	awk -F: '
+ {
+ 	fields = split($2, field2, "/")		# split ngs/dists
+ 	nngs = split(field2[1], ngs, ",")	# split ng,ng,ng
+ 	for (i = 1; i < nngs; i++)		# print field2[1] robustly
+ 		printf "%s,", ngs[i]
+ 	printf "%s\n", ngs[nngs]
+ 	exit
+ }' `
+ awk '$1 !~ /^(control|junk)$/ { print $1 }' $file | gngp -a -v "$gngppat" >$tmp
+ if test -s $tmp
+ then
+ 	echo `wc -l <$tmp` "newsgroup(s) are disallowed by ME line of sys file:"
+ 	cat $tmp
+ 	exit
+ fi
+ 
+ case "$quick" in
+ y)	status=0 ; exit	;;
+ esac
+ 
+ # this is pretty slow
+ while read newsgroup rest
+ do
+ 	echo "$newsgroup" | awk -f $NEWSBIN/namecheck.awk
+ done <$file | tee $tmp
+ if test -s $tmp
+ then
+ 	exit
+ fi
+ 
+ status=0		# and the trap does the actual exit



*** maint/makefile.mastercopy	Sun Nov 27 20:36:36 1994
--- maint/makefile	Mon Nov 14 16:58:27 1994
***************
*** 4,10 ****
  BINS=histinfo
  NORMAL=$(BINS) newsdaily newswatch newsboot addgroup delgroup adddirs \
  	act.to.times histfrom newsfrom addmissing newshist badexpiry \
! 	newsflag mkhistory locknews histdups logroll mergeactive
  CUSTOM=spaceshort
  UI=cnewsdo
  DPROGS=$(NORMAL) $(CUSTOM)
--- 4,11 ----
  BINS=histinfo
  NORMAL=$(BINS) newsdaily newswatch newsboot addgroup delgroup adddirs \
  	act.to.times histfrom newsfrom addmissing newshist badexpiry \
! 	newsflag mkhistory locknews histdups logroll mergeactive \
! 	checkactive
  CUSTOM=spaceshort
  UI=cnewsdo
  DPROGS=$(NORMAL) $(CUSTOM)
***************
*** 29,34 ****
--- 30,36 ----
  cmp:	$(ALL)
  	@$(IN) $(DEST) $(DPROGS)
  	@$(IN) $(UIBIN) $(UI)
+ 	$(DEST)/checkactive -q
  
  $(BINS):	$(LIB)
  
***************
*** 39,56 ****
  FIXMIDFIELD='s/	[1-9][0-9]*~/	1~/'
  
  ../util/dbz:
! 	( cd ../util ; $(MAKE) dbz )
  
! rsetup:	$(PROGS) ../util/dbz
  	$(MX) $(PROGS)
  	mkdir bin maint
  	cp histinfo histdups maint
! 	cp ../util/dbz bin
  	echo 'exit 0' >bin/lock
  	echo 'exit 0' >bin/unlock
  	echo 'echo here' >bin/newshostname
  	echo 'echo 0' >bin/now
  	$(MX) maint/* bin/*
  	mkdir arts arts/foo arts/bar arts/bar/ugh arts/urp
  	mkdir arts/bletch arts/lost+found
  	echo nevermore >arts/lost+found/1
--- 41,62 ----
  FIXMIDFIELD='s/	[1-9][0-9]*~/	1~/'
  
  ../util/dbz:
! 	cd ../util ; $(MAKE) dbz
  
! ../util/gngp:
! 	cd ../util ; $(MAKE) gngp
! 
! rsetup:	$(PROGS) ../util/dbz ../util/gngp ../util/namecheck.awk ../util/canonsys.awk
  	$(MX) $(PROGS)
  	mkdir bin maint
  	cp histinfo histdups maint
! 	cp ../util/dbz ../util/gngp bin
  	echo 'exit 0' >bin/lock
  	echo 'exit 0' >bin/unlock
  	echo 'echo here' >bin/newshostname
  	echo 'echo 0' >bin/now
  	$(MX) maint/* bin/*
+ 	cp ../util/namecheck.awk ../util/canonsys.awk .
  	mkdir arts arts/foo arts/bar arts/bar/ugh arts/urp
  	mkdir arts/bletch arts/lost+found
  	echo nevermore >arts/lost+found/1
***************
*** 78,83 ****
--- 84,90 ----
  	echo '<bar.ugh/13@trash>	0~0	bar.ugh/13' >>history.gbase
  	echo 'Message-ID: illegal' >arts/bar/ugh/13
  	sed $(FIXMIDFIELD) history.gbase | sort >history.good
+ 	echo 'here/there:foo,bar,bletch/nonsense' >sys
  
  r:	$(PROGS) rclean rsetup
  	: "mkhistory"
***************
*** 114,130 ****
  	cmp history.good history.fixed || diff history.good history.fixed
  	test ! -s addm.out ;
  	test ! -s addm.diag ;
! 	: give mergeactive a try
  	echo "foo 77 01 m" >active
  	echo "bar 99 77 y" >>active
  	echo "foo 0 nobody" >active.times
  	echo "bar 0 nobody" >>active.times
  	echo "foo 22 21 m" >active.new
  	echo "bletch 777 002 =foo" >>active.new
! 	cp active active.good
! 	echo "bletch 0000000000 00001 =foo" >>active.good
! 	cp active.times actimes.good
! 	echo "bletch 0 regression@here" >>actimes.good
  	$(HERE) ./mergeactive active.new >ma.out 2>ma.diag
  	test ! -s ma.out ;
  	test ! -s ma.diag ;
--- 121,143 ----
  	cmp history.good history.fixed || diff history.good history.fixed
  	test ! -s addm.out ;
  	test ! -s addm.diag ;
! 	: quick verification that checkactive at least runs, not a good test...
  	echo "foo 77 01 m" >active
  	echo "bar 99 77 y" >>active
+ 	echo "bletch 0000000000 00001 =foo" >>active
+ 	$(HERE) ./checkactive >ca.out 2>ca.diag
+ 	test ! -s ca.out ;
+ 	test ! -s ca.diag ;
+ 	: give mergeactive a try
  	echo "foo 0 nobody" >active.times
  	echo "bar 0 nobody" >>active.times
+ 	echo "bletch 0 regression@here" >>active.times
  	echo "foo 22 21 m" >active.new
  	echo "bletch 777 002 =foo" >>active.new
! 	mv active active.good
! 	egrep -v bletch active.good >active
! 	mv active.times actimes.good
! 	egrep -v bletch actimes.good >active.times
  	$(HERE) ./mergeactive active.new >ma.out 2>ma.diag
  	test ! -s ma.out ;
  	test ! -s ma.diag ;
***************
*** 135,140 ****
--- 148,154 ----
  rclean:
  	rm -f history history.pag history.dir active active.* active.times*
  	rm -f history.n* history.* *.out *.diag *.good *.fixed actimes.*
+ 	rm -f namecheck.awk canonsys.awk sys
  	rm -rf arts maint bin
  
  clean:	rclean



*** man/expire.8cn.mastercopy	Sun Nov 27 20:36:37 1994
--- man/expire.8cn	Wed Oct 19 00:15:25 1994
***************
*** 4,10 ****
  .ds b /usr/libexec/news
  .\" =()<.ds c @<NEWSCTL>@>()=
  .ds c /etc/news
! .TH EXPIRE 8CN "3 Oct 1994"
  .BY "C News"
  .SH NAME
  expire, doexpire, expireiflow \- expire old news
--- 4,10 ----
  .ds b /usr/libexec/news
  .\" =()<.ds c @<NEWSCTL>@>()=
  .ds c /etc/news
! .TH EXPIRE 8CN "19 Oct 1994"
  .BY "C News"
  .SH NAME
  expire, doexpire, expireiflow \- expire old news
***************
*** 364,370 ****
  \*c/history.o	history file as of last expiry
  \*c/history.n*	new history file and \fIdbm\fR files abuilding
  \*c/LOCKexpire	\fIdoexpire\fR's lock file
- \*b/expire/*	various auxiliaries
  .SH SEE ALSO
  inews(1CN),
  dbm(3),
--- 364,369 ----



*** man/newsdb.5.mastercopy	Sun Nov 27 20:36:37 1994
--- man/newsdb.5	Fri Nov 18 11:12:01 1994
***************
*** 26,32 ****
  .fi
  .LP
  ..
! .TH NEWSDB 5 "9 Sept 1994"
  .BY "C News"
  .SH NAME
  newsdb \- USENET network news database: article tree, history, active, active.times
--- 26,32 ----
  .fi
  .LP
  ..
! .TH NEWSDB 5 "18 Nov 1994"
  .BY "C News"
  .SH NAME
  newsdb \- USENET network news database: article tree, history, active, active.times
***************
*** 157,162 ****
--- 157,169 ----
  .B \*c/history.dir
  and
  .BR \*c/history.pag .
+ .PP
+ Also usually present now are
+ .I overview
+ files, one per newsgroup;
+ see
+ .IR newsoverview (5)
+ for more detail on them.
  .SH EXAMPLES
  A trivial
  .I active
***************
*** 206,208 ****
--- 213,226 ----
  invented
  by Mark Moraes and Geoff Collyer
  as part of the C News project.
+ .SH BUGS
+ Reader implementors are warned that the
+ various files mentioned here\(emthe
+ .I active
+ file,
+ the
+ .I history
+ file,
+ the article tree itself,
+ and the overview files\(emin general are not updated perfectly simultaneously.
+ Nor are they necessarily updated in a predictable order.



*** man/badexpiry.8cn.mastercopy	Sun Nov 27 20:36:38 1994
--- man/badexpiry.8cn	Tue Oct 18 21:55:30 1994
***************
*** 0 ****
--- 1,41 ----
+ .\" =()<.ds a @<NEWSARTS>@>()=
+ .ds a /var/news
+ .\" =()<.ds b @<NEWSBIN>@>()=
+ .ds b /usr/libexec/news
+ .\" =()<.ds c @<NEWSCTL>@>()=
+ .ds c /etc/news
+ .TH BADEXPIRY 8CN "18 Oct 1994"
+ .BY "C News"
+ .SH NAME
+ badexpiry \- find newsgroups with odd use of explicit expiry dates
+ .SH SYNOPSIS
+ .B badexpiry
+ [ mincount minsize ]
+ .SH DESCRIPTION
+ .I Badexpiry
+ reports (on standard output) newsgroups
+ with suspicious patterns of use of explicit
+ expiry dates in articles.
+ Specifically, it reports groups with more than
+ .I mincount
+ (default 15) articles,
+ more than 90% of all articles,
+ or more than
+ .I minsize
+ (default 500) KB of articles having explicit expiry dates
+ more than 10 days after their arrival dates.
+ .PP
+ Such reports are not necessarily indicative of abuses\(emfor example,
+ .I news.announce.newusers
+ legitimately has most or all of its articles on long expiry\(embut
+ investigation may be in order.
+ .SH FILES
+ \*c/history
+ .br
+ \*a/*/*/...
+ .SH SEE ALSO
+ news(5), newsdb(5)
+ .SH HISTORY
+ Written for C News by Henry Spencer.
+ .SH BUGS
+ The percentage and age thresholds ought to be settable.



*** man/checkactive.8.mastercopy	Sun Nov 27 20:36:38 1994
--- man/checkactive.8	Mon Nov 14 17:01:41 1994
***************
*** 0 ****
--- 1,43 ----
+ .\" =()<.ds a @<NEWSARTS>@>()=
+ .ds a /var/news
+ .\" =()<.ds b @<NEWSBIN>@>()=
+ .ds b /usr/libexec/news
+ .\" =()<.ds c @<NEWSCTL>@>()=
+ .ds c /etc/news
+ .TH CHECKACTIVE 8CN "9 Nov 1994"
+ .BY "C News"
+ .SH NAME
+ checkactive \- check format of news active file
+ .SH SYNOPSIS
+ .B checkactive
+ [
+ .B \-q
+ ] [ file ]
+ .SH DESCRIPTION
+ .I Checkactive
+ checks the format of news's
+ .I active
+ file
+ (or the named
+ .IR file ,
+ if specified),
+ and also checks the file for consistency with the
+ .I sys
+ file.
+ Complaints, if any, are produced on standard output.
+ .PP
+ The
+ .B \-q
+ (quick) option suppresses one test,
+ precise checking of the format of newsgroup names,
+ which is relatively slow for a large
+ .I active
+ file.
+ .SH SEE ALSO
+ newsdb(5)
+ .SH HISTORY
+ Written at U of Toronto by Henry Spencer.
+ .SH BUGS
+ There's no good reason why name-format checking
+ .I has
+ to be so slow.



*** nov/expovguts.c.mastercopy	Sun Nov 27 20:36:39 1994
--- nov/expovguts.c	Mon Oct 31 14:07:21 1994
***************
*** 59,69 ****
  	}
  
  	start = atol(argv[optind+1]);
! 	stop = atol(argv[optind]) + 100;	/* arbitrary slop */
! 	if (start >= stop || start == 0) {
! 		fprintf(stderr, "%s: min is 0 or exceeds max\n", progname);
  		exit(2);
  	}
  	if (debug)
  		printf("start %ld, stop %ld\n", start, stop);
  
--- 59,71 ----
  	}
  
  	start = atol(argv[optind+1]);
! 	stop = atol(argv[optind]);		/* tentatively */
! 	if (start > stop+1 || start == 0) {
! 		fprintf(stderr, "%s: min (%s) is 0 or exceeds max (%s) + 1\n",
! 				progname, argv[optind+1], argv[optind]);
  		exit(2);
  	}
+ 	stop += 100;				/* a bit of headroom */
  	if (debug)
  		printf("start %ld, stop %ld\n", start, stop);
  



*** readnews/postnews.mastercopy	Sun Nov 27 20:36:39 1994
--- readnews/postnews	Wed Nov  2 00:16:59 1994
***************
*** 4,10 ****
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
! PATH=$NEWSCTL/bin:$NEWSBIN/inject:$NEWSBIN:$NEWSPATH	# but do not export it
  umask 077				# private
  
  tmp=/tmp/pn$$
--- 4,13 ----
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
! oldpath="$PATH"
! newpath=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH
! PATH=$newpath
! export PATH
  umask 077				# private
  
  tmp=/tmp/pn$$
***************
*** 82,88 ****
--- 85,93 ----
  echo REPLACE THIS LINE WITH YOUR TEXT >>$tmp
  
  trap : 2
+ PATH="$oldpath"
  $edit $tmp
+ PATH=$newpath
  trap "$term" 2
  
  while egrep '^(DELETE|REPLACE) THIS LINE' $tmp >/dev/null
***************
*** 99,107 ****
  
  	echo 'Editing again...  Please check it over carefully.'
  	trap : 2
  	$edit $tmp
  	trap "$term" 2
  done
  
  echo 'Posting...'
! inews -h <$tmp
--- 104,114 ----
  
  	echo 'Editing again...  Please check it over carefully.'
  	trap : 2
+ 	PATH="$oldpath"
  	$edit $tmp
+ 	PATH=$newpath
  	trap "$term" 2
  done
  
  echo 'Posting...'
! $NEWSBIN/inject/inews -h <$tmp



*** readnews/makefile.mastercopy	Sun Nov 27 20:36:40 1994
--- readnews/makefile	Wed Nov  2 00:05:59 1994
***************
*** 36,39 ****
  r rclean:	$(ALL)
  
  clean:
! 	rm -f *.o core $(BINS)
--- 36,39 ----
  r rclean:	$(ALL)
  
  clean:
! 	rm -f *.o core $(BINS) .newsrc



*** relay/regress/regress.mastercopy	Sun Nov 27 20:36:41 1994
--- relay/regress/regress	Mon Oct 24 17:03:38 1994
***************
*** 14,20 ****
  (cd tmp; tar -xf -) <master.tar
  rm -f master.tar
  cd tmp
! chmod u+w *
  
  echo running relaynews...
  ./run
--- 14,20 ----
  (cd tmp; tar -xf -) <master.tar
  rm -f master.tar
  cd tmp
! chmod u+w * */*
  
  echo running relaynews...
  ./run



*** relay/fileart.c.mastercopy	Sun Nov 27 20:36:41 1994
--- relay/fileart.c	Tue Nov 15 12:54:00 1994
***************
*** 252,258 ****
  		lp->l_dev = statb.st_dev;
  	free(destdir);
  	for (plp = link1; plp < lp; plp++)
! 		if (plp->l_dev == lp->l_dev && plp->l_dev != -1)
  			break;
  	if (plp < lp)			/* yes, we do */
  		if (plp->l_type == 'l') {
--- 252,258 ----
  		lp->l_dev = statb.st_dev;
  	free(destdir);
  	for (plp = link1; plp < lp; plp++)
! 		if (plp->l_dev == lp->l_dev && plp->l_dev != (dev_t)-1)
  			break;
  	if (plp < lp)			/* yes, we do */
  		if (plp->l_type == 'l') {



*** util/newshostname.mastercopy	Sun Nov 27 20:36:42 1994
--- util/newshostname	Wed Nov 23 00:00:58 1994
***************
*** 4,10 ****
  . ${NEWSCONFIG-/etc/news/bin/config}
  
  PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
- umask $NEWSUMASK
  
  cat $NEWSCTL/whoami
- exit 0
--- 4,8 ----



*** util/dostatfs.c.mastercopy	Sun Nov 27 20:36:42 1994
--- util/dostatfs.c	Thu Nov 10 19:20:23 1994
***************
*** 6,32 ****
  #include <string.h>
  #include <sys/types.h>
  
! #include <sys/mount.h>
  
! #ifndef MNAMELEN
! /* oops, this does not seem to be a 4.4BSD */
  
! /* maybe it's SunOS? */
  #include <sys/vfs.h>
! 
! /* yet more silly variations... */
  #ifdef _AIX
  #include <sys/statfs.h>
  #endif
! 
! #endif	/* MNAMELEN */
  
! #ifdef BSD4_4
! #define	f_fsize	f_bsize		/* idiotic incompatible naming in 4.4 */
  #endif
  
- extern int debug;
  
  extern void error();
  
  /*
--- 6,46 ----
  #include <string.h>
  #include <sys/types.h>
  
! /*
!  * Unfortunately, the whereabouts of the necessary struct, the exact
!  * calling convention for statfs(), and the name of the "units in which
!  * free space is expressed" member, are rather system-specific.  Here's
!  * a few attempts...
!  */
  
! /* First, pick up one popular header on general principles. */
! #include <sys/mount.h>
  
! /* Second, assorted variations... */
! #ifdef BSD4_4
! #define	UNIT	f_bsize
! #endif
! #ifdef sun
  #include <sys/vfs.h>
! #endif
  #ifdef _AIX
  #include <sys/statfs.h>
  #endif
! #ifdef M_XENIX		/* SCO */
! #include <sys/statfs.h>
! #define	STATFS(fs, result)	statfs(fs, &result, (int)sizeof(result), 0)
! #endif
  
! /* Finally, some defaults to simplify the above. */
! #ifndef UNIT
! #define	UNIT	f_fsize
  #endif
+ #ifndef STATFS
+ #define	STATFS(fs, result)	statfs(fs, &result)
+ #endif
  
  
+ extern int debug;
  extern void error();
  
  /*
***************
*** 45,61 ****
  #	define	LOTS	10000
  	register long iperfile = filesize/bperi + 1;
  
! 	if (statfs(fileonfs, &info) < 0)
  		error("cannot do statfs(%s)", fileonfs);
  	if (debug)
  		fprintf(stderr, "bsize %ld, avail %ld, inodes %ld\n",
! 				info.f_fsize, info.f_bavail, info.f_ffree);
  
  	n = LOTS;
  	if (info.f_bavail <= wantspace)
  		n = 0;
! 	else if (info.f_fsize > 0 && filesize > 0)
! 		n = (info.f_bavail - wantspace) / (filesize/info.f_fsize + 1);
  
  	if (info.f_ffree < 0)		/* information unavailable */
  		;			/* bypass check, and pray */
--- 59,75 ----
  #	define	LOTS	10000
  	register long iperfile = filesize/bperi + 1;
  
! 	if (STATFS(fileonfs, info) < 0)
  		error("cannot do statfs(%s)", fileonfs);
  	if (debug)
  		fprintf(stderr, "bsize %ld, avail %ld, inodes %ld\n",
! 				info.UNIT, info.f_bavail, info.f_ffree);
  
  	n = LOTS;
  	if (info.f_bavail <= wantspace)
  		n = 0;
! 	else if (info.UNIT > 0 && filesize > 0)
! 		n = (info.f_bavail - wantspace) / (filesize/info.UNIT + 1);
  
  	if (info.f_ffree < 0)		/* information unavailable */
  		;			/* bypass check, and pray */



*** util/makefile.mastercopy	Sun Nov 27 20:36:43 1994
--- util/makefile	Sun Nov 27 18:17:11 1994
***************
*** 88,95 ****
  dospacefor:	dowhatever.o do$(SPACEFOR).o
  	$(CC) $(LDFLAGS) dowhatever.o do$(SPACEFOR).o $(LIBS) -o $@
  
! queuelen.pre:	queuelen.old
! 	sed '/grade=d/s//grade=./' queuelen.old >$@
  
  queuelen.null:
  	echo '#! /bin/sh' >$@
--- 88,95 ----
  dospacefor:	dowhatever.o do$(SPACEFOR).o
  	$(CC) $(LDFLAGS) dowhatever.o do$(SPACEFOR).o $(LIBS) -o $@
  
! queuelen.pre:	queuelen.vo
! 	sed '/grade=d/s//grade=./' queuelen.vo >$@
  
  queuelen.null:
  	echo '#! /bin/sh' >$@



*** util/queuelen.vo.mastercopy	Sun Nov 27 20:36:43 1994
--- util/queuelen.vo	Thu Sep 15 18:24:51 1994
***************
*** 0 ****
--- 1,28 ----
+ #! /bin/sh
+ # Find size of current queue of news outbound to $1.  Ancient-uucp version.
+ 
+ # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
+ . ${NEWSCONFIG-/etc/news/bin/config}
+ 
+ PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
+ umask $NEWSUMASK
+ 
+ if test -d /usr/spool/uucp
+ then
+ 	cd /usr/spool/uucp
+ elif test -d /var/spool/uucp
+ then
+ 	cd /var/spool/uucp
+ else
+ 	echo "$0: can't find uucp spool directory" >&2
+ 	echo 9999
+ 	exit 1
+ fi
+ grade=d
+ 
+ case "$1" in
+ ?|??|???|????|?????|??????|???????)	site="$1" ;;
+ *)	site=`expr "$1" : '^\(.......\).*'`
+ esac
+ 
+ ls | egrep "^C\.$site$grade....\$" | wc -l



*** util/dbzinfo.mastercopy	Sun Nov 27 20:36:44 1994
--- util/dbzinfo	Tue Oct 18 20:59:36 1994
***************
*** 19,46 ****
  NR == 2 {
  	print "lines", $1
  	printf "lines_history"
! 	n = 0
! 	for (i = 2; i <= NF; i++) {
! 		if (n%5 == 0 && n != 0)
! 			printf "\n\t"
! 		else
! 			printf " "
! 		printf "%d", $i
! 		n++
! 	}
  	printf "\n"
  }
  NR == 3 {
  	print "number_untagged", $1
  	printf "untagged_history"
! 	n = 0
! 	for (i = 2; i <= NF; i++) {
! 		if (n%5 == 0 && n != 0)
! 			printf "\n\t"
! 		else
! 			printf " "
! 		printf "%d", $i
! 		n++
! 	}
  	printf "\n"
  }' $1.dir
--- 19,32 ----
  NR == 2 {
  	print "lines", $1
  	printf "lines_history"
! 	for (i = 2; i <= NF; i++)
! 		printf " %d", $i
  	printf "\n"
  }
  NR == 3 {
  	print "number_untagged", $1
  	printf "untagged_history"
! 	for (i = 2; i <= NF; i++)
! 		printf " %d", $i
  	printf "\n"
  }' $1.dir



*** util/namecheck.awk.mastercopy	Sun Nov 27 20:36:44 1994
--- util/namecheck.awk	Mon Nov  7 17:08:00 1994
***************
*** 1,4 ****
--- 1,5 ----
  # awk program to check newsgroup names for validity, rigorously
+ # echo groupname | awk -f namecheck.awk
  # It may look like some things in per-component checking could be done
  # more efficiently by moving them into whole-name checking.  The presence
  # of encoded name components messes this up.



