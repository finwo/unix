Newsgroups: news.software.b
Subject: C News patch CR.D

This is a patch for the C News Cleanup Release.  The distribution files
on ftp.cs.toronto.edu and zoo.toronto.edu have been updated to match.
See the README.changes diff below for what's been done.

start of patch CR.D
(suggested archive name:  patchCR.D)
apply with   patch -p0 <thisfile

Prereq: CR.C.
*** README.mastercopy	Wed Nov 30 17:36:23 1994
--- README	Wed Nov 30 17:07:30 1994
***************
*** 1,4 ****
! Cleanup Release of C News, with patch CR.C.			Nov 1994
  
  The current C News distribution can be retrieved by anonymous FTP from
  ftp.cs.toronto.edu (file pub/c-news/c-news.tar.Z) or ftp.zoo.toronto.edu
--- 1,4 ----
! Cleanup Release of C News, with patch CR.D			Nov 1994
  
  The current C News distribution can be retrieved by anonymous FTP from
  ftp.cs.toronto.edu (file pub/c-news/c-news.tar.Z) or ftp.zoo.toronto.edu



*** README.changes.mastercopy	Wed Nov 30 17:36:24 1994
--- README.changes	Wed Nov 30 17:35:54 1994
***************
*** 1,3 ****
--- 1,16 ----
+ High points of patch CR.D:
+ This one's mostly a cleanup job on CR.B/CR.C.  The one really new item
+ is a queuelen appropriate to Taylor UUCP.  The master makefile has been
+ fixed to eliminate complaints from make, and to do the "patchchores"
+ checks itself so they can be done before fixing the subordinate makefiles.
+ The upact regression test has been fixed to pick up checkactive properly,
+ and to *pass* the resulting test; as a side effect, checkactive has acquired
+ a new option, -n.  A few more undesirable redeclarations of system functions
+ have been rooted out, and dostatfs.c has been beaten on a bit to make it
+ more likely to compile "straight out of the box" on Suns.  And I've started
+ what's going to be a long campaign of adding space around lines changed by
+ subst, so that they don't mess up patches.
+ 
  High points of patch CR.C:
  This is the second part of a two-part patch, the first being CR.B.  Install
  both parts, this one second.  In this part...  A nasty bug in newsgroup



*** quiz.mastercopy	Wed Nov 30 17:36:25 1994
--- quiz	Wed Nov 30 16:12:18 1994
***************
*** 394,399 ****
--- 394,400 ----
  echo 'program.  C News knows about several versions:'
  echo '	svr4	System V Release 4 uucp'
  echo '	hdb	pre-SVR4 Honey DanBer, aka Basic Networking Utilities'
+ echo '	tay	Taylor UUCP, native mode'
  echo '	sub	old uucp with subdirectories (e.g. /usr/spool/uucp/C.)'
  echo '	vo	very old uucp, no subdirectories'
  echo '	pre	prehistoric uucp, no subdirectories, no -g option on uux'
***************
*** 402,411 ****
  do
  	uucptype=`$ask 'Which one is most appropriate' ${uucptype-hdb}`
  	case "$uucptype" in
! 	sub|old)	echo 'Beware -- test "queuelen" to make sure it works.'	;;
! 	esac
! 	case "$uucptype" in
! 	svr4|hdb|sub|old|pre|null)	break	;;
  	esac
  	echo 'Sorry, no such choice is available.'
  done
--- 403,409 ----
  do
  	uucptype=`$ask 'Which one is most appropriate' ${uucptype-hdb}`
  	case "$uucptype" in
! 	svr4|hdb|tay|sub|old|pre|null)	break	;;
  	esac
  	echo 'Sorry, no such choice is available.'
  done



*** makefile.mastercopy	Wed Nov 30 17:36:25 1994
--- makefile	Wed Nov 30 16:05:16 1994
***************
*** 108,119 ****
  conf/liblist:	include/config.make
  	cd conf ; $(MAKE) liblist
  
- include/config.make:
- 	: "You must run 'quiz' before running 'make'."
- 	exit 1 ;
- 
  patchchores:
! 	cd conf ; $(MAKE) patchchores
  
  r:
  	$(INALL) "$(MAKE) r $(ROPT) && $(MAKE) rclean" $(RDIRS)
--- 108,118 ----
  conf/liblist:	include/config.make
  	cd conf ; $(MAKE) liblist
  
  patchchores:
! 	: if this fails, you have not installed all parts of a multi-part patch
! 	test " `cat conf/versionname | sed 's/^[^.]*\..//'`" = " " ;
! 	: delete files obsoleted by patches
! 	rm -f `cat conf/deadfiles`
  
  r:
  	$(INALL) "$(MAKE) r $(ROPT) && $(MAKE) rclean" $(RDIRS)



*** conf/versionname.mastercopy	Wed Nov 30 17:36:26 1994
--- conf/versionname	Wed Nov 30 17:07:21 1994
***************
*** 1 ****
! Cleanup Release, with patch CR.C
--- 1 ----
! Cleanup Release, with patch CR.D



*** conf/subst.all.mastercopy	Wed Nov 30 17:36:26 1994
--- conf/subst.all	Wed Nov 30 16:11:01 1994
***************
*** 94,99 ****
--- 94,100 ----
  util/queuelen.hdb
  util/queuelen.sub
  util/queuelen.svr4
+ util/queuelen.tay
  util/queuelen.vo
  util/report
  util/sfcproto



*** conf/makefile.mastercopy	Wed Nov 30 17:36:27 1994
--- conf/makefile	Wed Nov 30 16:05:52 1994
***************
*** 1,6 ****
--- 1,8 ----
  # =()<@<INCLUDE>@>()=
  .include "../include/config.make"
  
+ 
+ 
  A=$(NEWSARTS)
  C=$(NEWSCTL)
  NDIRS=$(A) $(A)/in.coming $(A)/in.coming/bad $(A)/out.master $(C) $(C)/bin
***************
*** 11,22 ****
  
  all:	$(ALL)
  	$(MX) report.ctl config
- 
- patchchores:
- 	: if this fails, you have not installed all parts of a multi-part patch
- 	test " `cat versionname | sed 's/^[^.]*\..//'`" = " " ;
- 	: delete files obsoleted by patches
- 	cd .. ; rm -f `cat conf/deadfiles` conf/dummy
  
  install:	$(ALL)
  
--- 13,18 ----



*** doc/problems.mastercopy	Wed Nov 30 17:36:27 1994
--- doc/problems	Wed Nov 30 16:08:45 1994
***************
*** 1102,1104 ****
--- 1102,1109 ----
  This breaks \fIupact\fR; the \fIupact\fR regression test catches this.
  Steve Robbins found this and posted a fix;
  it's too long to reproduce here.
+ See:
+ .DS
+ ftp://ftp.cim.mcgill.ca/pub/people/steve/pc/linux/join
+ .DE
+ Textutils 1.11 reportedly has fixed this.



*** expire/makefile.mastercopy	Wed Nov 30 17:36:28 1994
--- expire/makefile	Wed Nov 30 16:30:25 1994
***************
*** 1,6 ****
--- 1,8 ----
  # =()<@<INCLUDE>@>()=
  .include "../include/config.make"
  
+ 
+ 
  BINS=expire
  PROGS=$(BINS) upact doexpire expireiflow
  DEST=$(NEWSBIN)/expire
***************
*** 55,61 ****
  	rm -f $@
  	egrep -v '^unused' explist.regw >$@
  
! UTILS=../util/dbz ../util/mkpdir
  
  ../util/dbz:
  	cd ../util ; $(MAKE) dbz
--- 57,64 ----
  	rm -f $@
  	egrep -v '^unused' explist.regw >$@
  
! UTILS=../util/dbz ../util/mkpdir ../maint/checkactive ../util/gngp
! AUTILS=../util/canonsys.awk ../util/namecheck.awk
  
  ../util/dbz:
  	cd ../util ; $(MAKE) dbz
***************
*** 63,68 ****
--- 66,77 ----
  ../util/mkpdir:
  	cd ../util ; $(MAKE) mkpdir
  
+ ../util/gngp:
+ 	cd ../util ; $(MAKE) gngp
+ 
+ ../maint/checkactive:
+ 	cd ../maint ; $(MAKE) checkactive
+ 
  BARFS=active.barf active.nobarf
  AB=active.$(UPACTBARF)barf
  
***************
*** 73,104 ****
  	touch $@
  
  # setup for regression test
! rsetup:	$(PROGS) $(UTILS) explist.reg explist.regw $(BARFS)
  	$(MX) $(PROGS) dircheck
  	>history.pag
  	>history.dir
  	mkdir bin
! 	cp ../util/dbz ../util/mkpdir bin
  	echo 'exit 0' >bin/lock
  	echo 'exit 0' >bin/unlock
  	echo 'echo 10' >bin/spacefor
  	$(MX) bin/*
  	mkdir arts arts/foo arts/bar arts/bar/ugh arts/urp arch arch2
  	mkdir arts/mod arts/mod/mod arts/mod/unmod arch3 arch3/bletch
  	mkdir arts/bletch arts/lost+found arts/barf arts/barf/puke
! 	echo 'foo 00103 00000 y ' >>active
  	echo 'foo 00103 00098 y' >>active.after
! 	echo 'bar 00099 00000 m' >>active
  	echo 'bar 00099 00100 m' >>active.after
! 	echo 'bar.ugh 00099 00000 m' >>active
  	echo 'bar.ugh 00099 00099 m' >>active.after
! 	echo 'urp 00099 00000 n' >>active
  	echo 'urp 00099 00007 n' >>active.after
! 	echo 'mod.mod 00013 00000 m' >>active
  	echo 'mod.mod 00013 00013 m' >>active.after
! 	echo 'mod.unmod 00016 00000 y' >>active
  	echo 'mod.unmod 00016 00016 y' >>active.after
! 	echo 'bletch 00099 00000 y' >>active
  	echo 'bletch 00099 00100 y' >>active.after
  	cat $(AB) >>active
  	cat $(AB) >>active.after
--- 82,116 ----
  	touch $@
  
  # setup for regression test
! rsetup:	$(PROGS) $(UTILS) $(AUTILS) explist.reg explist.regw $(BARFS)
  	$(MX) $(PROGS) dircheck
  	>history.pag
  	>history.dir
  	mkdir bin
! 	cp $(UTILS) bin
  	echo 'exit 0' >bin/lock
  	echo 'exit 0' >bin/unlock
  	echo 'echo 10' >bin/spacefor
+ 	echo 'echo fred' >bin/newshostname
  	$(MX) bin/*
+ 	cp $(AUTILS) .
+ 	echo "ME:all" >sys
  	mkdir arts arts/foo arts/bar arts/bar/ugh arts/urp arch arch2
  	mkdir arts/mod arts/mod/mod arts/mod/unmod arch3 arch3/bletch
  	mkdir arts/bletch arts/lost+found arts/barf arts/barf/puke
! 	echo 'foo 00103 00001 y ' >>active
  	echo 'foo 00103 00098 y' >>active.after
! 	echo 'bar 00099 00001 m' >>active
  	echo 'bar 00099 00100 m' >>active.after
! 	echo 'bar.ugh 00099 00001 m' >>active
  	echo 'bar.ugh 00099 00099 m' >>active.after
! 	echo 'urp 00099 00001 n' >>active
  	echo 'urp 00099 00007 n' >>active.after
! 	echo 'mod.mod 00013 00001 m' >>active
  	echo 'mod.mod 00013 00013 m' >>active.after
! 	echo 'mod.unmod 00016 00001 y' >>active
  	echo 'mod.unmod 00016 00016 y' >>active.after
! 	echo 'bletch 00099 00001 y' >>active
  	echo 'bletch 00099 00100 y' >>active.after
  	cat $(AB) >>active
  	cat $(AB) >>active.after
***************
*** 222,228 ****
  	rm -f junk history history.pag history.dir history.o active active.tmp
  	rm -f history.n* *mon.out history.proto history.after test.out doit
  	rm -f active.old active.new explist.reg lint active.after test.stderr
! 	rm -f active.errs explist.regw $(BARFS)
  	rm -rf arts arch arch2 arch3 bin
  
  clean:	rclean
--- 234,240 ----
  	rm -f junk history history.pag history.dir history.o active active.tmp
  	rm -f history.n* *mon.out history.proto history.after test.out doit
  	rm -f active.old active.new explist.reg lint active.after test.stderr
! 	rm -f active.errs explist.regw $(BARFS) canonsys.awk namecheck.awk sys
  	rm -rf arts arch arch2 arch3 bin
  
  clean:	rclean



*** expire/upact.mastercopy	Wed Nov 30 17:36:29 1994
--- expire/upact	Wed Nov 30 16:45:41 1994
***************
*** 1,9 ****
--- 1,13 ----
  #! /bin/sh
  # Update 3rd field (minimum art. #) of a 4-field active file.
  
+ 
+ 
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
+ 
+ 
  PATH=$NEWSCTL/bin:$NEWSBIN/maint:$NEWSBIN:$NEWSPATH ; export PATH
  umask $NEWSUMASK
  
***************
*** 29,35 ****
  cd $NEWSCTL
  
  # check out the active file
! checkactive -q >active.eek
  if test -s active.eek
  then
  	echo "$0: problems in active file -- aborting" >&2
--- 33,39 ----
  cd $NEWSCTL
  
  # check out the active file
! checkactive -n -q >active.eek
  if test -s active.eek
  then
  	echo "$0: problems in active file -- aborting" >&2



*** libc/getreldate.c.mastercopy	Wed Nov 30 17:36:29 1994
--- libc/getreldate.c	Tue Nov 29 12:51:23 1994
***************
*** 32,37 ****
--- 32,38 ----
  #include <time.h>
  #include <sys/types.h>
  #include <sys/timeb.h>
+ #include <stdlib.h>
  
  #include "datetok.h"
  #include "dateconv.h"
***************
*** 40,46 ****
  
  #define MAXDATEFIELDS 50
  
- extern long atol();
  extern time_t time();
  extern datetkn datereltoks[];
  extern unsigned int szdatereltoks;
--- 41,46 ----



*** libdbz/fake.c.mastercopy	Wed Nov 30 17:36:30 1994
--- libdbz/fake.c	Tue Nov 29 12:52:14 1994
***************
*** 8,13 ****
--- 8,14 ----
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <string.h>
+ #include <stdlib.h>
  
  #define	MAXSTR	500		/* For sizing strings -- DON'T use BUFSIZ! */
  #define	STREQ(a, b)	(*(a) == *(b) && strcmp((a), (b)) == 0)
***************
*** 45,51 ****
  	extern char *optarg;
  	void process();
  	register long no;
- 	extern long atol();
  	char line[MAXSTR];
  
  	progname = argv[0];
--- 46,51 ----



*** libdbz/dbzmain.c.mastercopy	Wed Nov 30 17:36:31 1994
--- libdbz/dbzmain.c	Wed Nov 30 16:26:49 1994
***************
*** 8,13 ****
--- 8,14 ----
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <string.h>
+ #include <stdlib.h>
  #include <dbz.h>
  
  #ifdef FUNNYSEEKS
***************
*** 80,87 ****
  #define	rfc822ize(n)	(n)
  #endif
  
- extern char *malloc();
- 
  /*
   - main - parse arguments and handle options
   */
--- 81,86 ----
***************
*** 94,100 ****
  	extern int optind;
  	extern char *optarg;
  	int doruns = 0;
- 	extern long atol();
  
  	progname = argv[0];
  
--- 93,98 ----



*** maint/checkactive.mastercopy	Wed Nov 30 17:36:31 1994
--- maint/checkactive	Wed Nov 30 16:41:27 1994
***************
*** 2,18 ****
--- 2,24 ----
  # checkactive - check an active file for worrisome signs
  # checkactive file
  
+ 
+ 
  # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
  . ${NEWSCONFIG-/etc/news/bin/config}
  
+ 
+ 
  PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
  umask $NEWSUMASK
  
  quick=n
+ numok=n
  for dummy
  do
  	case "$1" in
  	-q)	quick=y		;;
+ 	-n)	numok=y		;;
  	--)	shift ; break	;;
  	-*)	echo "$0: unknown option \`$1'" >&2 ; exit 2	;;
  	*)	break		;;
***************
*** 36,41 ****
--- 42,53 ----
  	exit
  fi
  
+ case "$numok" in
+ y)	numcheck=	;;
+ n)	numcheck='0+$3 == 0 { print where, "field 3 is zero" ; status = 1 }
+ 		1+$2 < 0+$3 { print where, q $2 q, "<", q $3 q ; status = 1 }'
+ 	;;
+ esac
  awk 'BEGIN { status = 0 ; q = "\"" }
  { where = "line " NR ":" }
  NF != 4 { print where, "has", NF, "not 4 fields" ; status = 1 }
***************
*** 48,55 ****
  $4 !~ /^([ynmx]|=.*)$/ {
  	print where, "field 4 (" q $4 q ") invalid" ; status = 1
  }
! 0+$3 < 1 { print where, "field 3 (" q $3 q ") zero or negative" ; status = 1 }
! 1+$2 < 0+$3 { print where, q $2 q, "<", q $3 q ; status = 1 }
  END { exit status }' $file || exit
  
  awk '{print $1}' $file | sort | uniq -d >$tmp
--- 60,66 ----
  $4 !~ /^([ynmx]|=.*)$/ {
  	print where, "field 4 (" q $4 q ") invalid" ; status = 1
  }
! '"$numcheck"'
  END { exit status }' $file || exit
  
  awk '{print $1}' $file | sort | uniq -d >$tmp



*** man/checkactive.8.mastercopy	Wed Nov 30 17:36:32 1994
--- man/checkactive.8	Wed Nov 30 16:43:36 1994
***************
*** 4,10 ****
  .ds b /usr/libexec/news
  .\" =()<.ds c @<NEWSCTL>@>()=
  .ds c /etc/news
! .TH CHECKACTIVE 8CN "9 Nov 1994"
  .BY "C News"
  .SH NAME
  checkactive \- check format of news active file
--- 4,13 ----
  .ds b /usr/libexec/news
  .\" =()<.ds c @<NEWSCTL>@>()=
  .ds c /etc/news
! .\"
! .\"
! .\"
! .TH CHECKACTIVE 8CN "30 Nov 1994"
  .BY "C News"
  .SH NAME
  checkactive \- check format of news active file
***************
*** 12,17 ****
--- 15,22 ----
  .B checkactive
  [
  .B \-q
+ ] [
+ .B \-n
  ] [ file ]
  .SH DESCRIPTION
  .I Checkactive
***************
*** 25,30 ****
--- 30,42 ----
  .I sys
  file.
  Complaints, if any, are produced on standard output.
+ .PP
+ The
+ .B \-n
+ (numeric fields okay)
+ option suppresses checking of the relationship between the two middle
+ (numeric) fields,
+ for the case where they are about to be rebuilt.
  .PP
  The
  .B \-q



*** notebook/makefiles.mastercopy	Wed Nov 30 17:36:33 1994
--- notebook/makefiles	Wed Nov 30 16:15:45 1994
***************
*** 1,4 ****
! .DA "28 Aug 1994"
  .TL
  C News Makefiles
  .AU
--- 1,4 ----
! .DA "30 Nov 1994"
  .TL
  C News Makefiles
  .AU
***************
*** 82,87 ****
--- 82,92 ----
  # =(\&)<@<INCLUDE>@>()=
  include ../include/config.make
  .DE
+ To avoid problems with context during patches, these lines should
+ preferably be followed by three blank lines
+ (and likewise preceded by three blank lines,
+ if there is anything before them).
+ .PP
  Among the definitions in the makefile header
  are the standard C News pathname variables (NEWSARTS, NEWSOV, etc.).
  There are a number of others:



*** util/getabsdate.c.mastercopy	Wed Nov 30 17:36:33 1994
--- util/getabsdate.c	Tue Nov 29 12:56:12 1994
***************
*** 7,12 ****
--- 7,13 ----
  #include <time.h>
  #include <sys/types.h>
  #include <sys/timeb.h>
+ #include <stdlib.h>
  
  /* privates */
  static struct timeb ftnow;
***************
*** 15,22 ****
  char *progname;
  
  /* imports */
- extern long atol();
- extern char *malloc();
  extern struct tm *gmtime();
  extern time_t time();
  extern int optind;
--- 16,21 ----



*** util/queuelen.tay.mastercopy	Wed Nov 30 17:36:33 1994
--- util/queuelen.tay	Wed Nov 30 16:10:47 1994
***************
*** 0 ****
--- 1,10 ----
+ #! /bin/sh
+ # Find size of current queue of news outbound to $1.  Taylor version.
+ 
+ # =()<. ${NEWSCONFIG-@<NEWSCONFIG>@}>()=
+ . ${NEWSCONFIG-/etc/news/bin/config}
+ 
+ PATH=$NEWSCTL/bin:$NEWSBIN:$NEWSPATH ; export PATH
+ umask $NEWSUMASK
+ 
+ uustat -s $1 -c rnews | wc -l



*** util/dostatfs.c.mastercopy	Wed Nov 30 17:36:34 1994
--- util/dostatfs.c	Mon Nov 28 17:51:53 1994
***************
*** 22,27 ****
--- 22,28 ----
  #endif
  #ifdef sun
  #include <sys/vfs.h>
+ #define	UNIT	f_bsize
  #endif
  #ifdef _AIX
  #include <sys/statfs.h>



*** util/dowhatever.c.mastercopy	Wed Nov 30 17:36:34 1994
--- util/dowhatever.c	Tue Nov 29 12:42:31 1994
***************
*** 24,31 ****
  	extern int optind;
  	extern char *optarg;
  	long spacefor();
- 	extern long atol();
- 	extern double atof();
  	register long n;
  
  	progname = argv[0];
--- 24,29 ----



