#!/usr/bin/env bash
mkdir -p Applications Distributions Documentation Tools

rsync -avzP minnie.tuhs.org::UA_Root             .
rsync -avzP minnie.tuhs.org::UA_Applications     Applications
rsync -avzP minnie.tuhs.org::UA_Distributions    Distributions
rsync -avzP minnie.tuhs.org::UA_Documentation    Documentation
rsync -avzP minnie.tuhs.org::UA_Tools            Tools
