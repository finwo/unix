SYSGEN -- an imitation of the CONFIG command of VMS SYSGEN.

$Id: README,v 1.4 2015/12/30 12:43:08 tih Exp $

Donated into the public domain and submitted to the archives of
"PUPS", the PDP-11 UNIX Preservation Society -- for info, see
<URL:http://minnie.tuhs.org/PUPS/>.

The program accepts a list of devices on standard input, each device
name optionally followed by a comma and a number.  The number, if
given, indicates the number of occurrences of the named device.

The output is a table of the same devices, with CSR and vector
assignments according to DEC standards.  Floating assignments are
flagged with asterisks, as in original SYSGEN output.  VMS device
names and support info are, however, not printed.

Sample run:

% cat devlist
tsv05
delqa
dhv11
tk50,2
rqdx3,2
kda50
% sysgen < devlist
Table of standard DEC assignments for configuration:

  DEVICE     CSR   VECTOR
-------------------------
   TSV05  772520   0224 
   DELQA  774440   0120 
   DHV11  760500*  0320*
    TK50  774500   0260 
    TK50  760444*  0310*
   RQDX3  772150   0154 
   RQDX3  760334*  0300*
   KDA50  760340*  0304*

CSRs and vectors marked '*' are in floating space.
%

The data contained in the ranking table is taken from documentation
for VMS 5.5, with a few corrections made where the manuals disagreed
with the actual output of the VMS SYSGEN command.

Corrections, improvements, comments and especially additions to the
alias table that translates newer device names into the originals in
the ranking table are very welcome!

Tom Ivar Helbekkmo <tih@hamartun.priv.no>
