This area contains the documents that we have uncovered that related to
Unix before 1st Edition, including some code from PDP-7 Unix.

[ Also see ../PDP7 for a project to reconstruct a working PDP-7 Unix system. ]

Draft of the Unix Timesharing System
====================================

At the end of 2015, Doug McIlroy sent this e-mail in to the TUHS
mailing list:

  Among the papers of the late Bob Morris I have found a
  Unix manual that I don't remember at all--a draft by
  Dennis Ritchie, in the style of (but not designated as)
  a technical report with numbered sections and subsections.
  It does not resemble the familiar layout of the numbered
  editions. Besides the usual overview of kernel and shell,
  it describes system calls and some commands, in a layout
  unrelated to the familiar man-page style. Detailed
  reference/tutorial manuals for as, roff, db and ed
  are included as appendices.
   
  The famous and well-justified claim that "UNIX contains a number
  of features very seldom offered even by larger systems"
  appears on page 1.

  The document is evidently ancestral to both the recognized manuals and
  the SIGOPS/CACM paper. It apparently dates from mid-1971 when Unix had
  been running for a "few months" on the PDP-11. At that time there were
  only 21 system calls, a number that had increased to 34 by November
  when the v1 manual was produced.

A PDF scan of this draft manual is available here as the file
UnixEditionZero.pdf. Nelson Beebe has created an OCR'd version
of the file which is UnixEditionZero-OCR.pdf. Paul McJones has
contributed an even smaller version of the file
UnixEditionZero-Threshold_OCR.pdf.

A text version of the document is available in UnixEditionZero.txt.

PDP-7 Unix Source Code
======================

At the beginning of 2016, Norman Wilson scanned in several documents
pertaining to PDP-7 Unix. These are the files:

    01-s1.pdf 02-hw.pdf 03-scope.pdf 04-cas.pdf 05-1-4.pdf
    06-5-12.pdf 07-13-19.pdf 08-rest.pdf

He writes:
    My notebook of PDP-7 stuff is photocopies of the
    contents of one or two loose-leaf notebooks that
    were at one point in the latter 1980s sitting around
    in the UNIX Room.  The original notebooks were mostly
    trimmed and punched line-printer listings, with hand-
    marked blank pages as dividers (the pages marked with
    numbers, or S1 or S3 or whatnot).  The PDP-7 Programming
    Card was a photocopy; I don't remember whether the modem
    and graphics-scope memos looked like originals.
    
    There were some complicated backs-and-forths with the
    photocopies.  In particular, I think Dennis lost the
    original notebooks at some point after I moved to Toronto
    (taking copies with me), and I sent him copies of my copies.

    What is there should be a scan of every page I have, in
    order.  Division into files is quasi-arbitrary, but the
    two-digit numbers at the front should sort it all in order.
    It is a long time since I've looked at this stuff so I have
    only a weak memory of what's where; I think the first part
    (sections labelled S1-S9 in the original) are the operating
    system, then there are some bits of info about specific
    hardware (including a PDP-7 instruction-set card, codes
    to drive the graphics scope, and a program that I am guessing
    without really knowing is about the scope, plus as a FREE
    BONUS a hand-drawn diagram of the scope's coordinates); then
    various commands in alphabetical order; then a bit of other
    stuff, including two short unlabelled B programs and ed.

The structure of the kernel source code is very similar to that of the
1st Edition Unix scan at
http://www.tuhs.org/Archive/PDP-11/Distributions/research/Dennis_v1/PreliminaryUnixImplementationDocument_Jun72.pdf

Here is what I can glean from the files:

01-s1.pdf	contains the kernel source divided into sections S1 to S9
02-hw.pdf	has hardware details of the PDP-7
03-scope.pdf	has information about the PDP-7 scope
04-cas.pdf	seems to be a user-mode program that uses the PDP-7 scope
05-1-4.pdf	user-mode programs: adm, ald, apr, as
06-5-12.pdf	user-mode programs: bl, bc, bi, cat, check, chown, chmod, cp, chrm
07-13-19.pdf	user-mode programs: db, dmabs, ds, dsksav, dskres, dskio, dsw, init
08-rest.pdf	user-mode programs: ed. Also at the beginning some B code (?)
