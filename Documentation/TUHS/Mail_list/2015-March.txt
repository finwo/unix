From ron at ronnatalie.com  Sat Mar 14 02:54:06 2015
From: ron at ronnatalie.com (Ronald Natalie)
Date: Fri, 13 Mar 2015 12:54:06 -0400
Subject: [TUHS] Old kernel hacker chuckle
Message-ID: <4C84DA71-80B9-4FA2-8EF9-56B001CCA148@ronnatalie.com>

I have an appointment Monday with my oral surgeon, Dr. Naimi.   I still can’t resist pronouncing it Dr. Namei.



From clemc at ccc.com  Sat Mar 14 03:18:22 2015
From: clemc at ccc.com (Clem Cole)
Date: Fri, 13 Mar 2015 13:18:22 -0400
Subject: [TUHS] Old kernel hacker chuckle
In-Reply-To: <4C84DA71-80B9-4FA2-8EF9-56B001CCA148@ronnatalie.com>
References: <4C84DA71-80B9-4FA2-8EF9-56B001CCA148@ronnatalie.com>
Message-ID: <CAC20D2NbPf53s5cvK9a=Ld8odV+OEbMveOeEz6A7e0oSqk65uA@mail.gmail.com>

but is the path going to bring you to his files?

On Fri, Mar 13, 2015 at 12:54 PM, Ronald Natalie <ron at ronnatalie.com> wrote:

> I have an appointment Monday with my oral surgeon, Dr. Naimi.   I still
> can’t resist pronouncing it Dr. Namei.
>
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs
>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20150313/c575efbd/attachment.html>

From beebe at math.utah.edu  Sun Mar 22 01:10:06 2015
From: beebe at math.utah.edu (Nelson H. F. Beebe)
Date: Sat, 21 Mar 2015 09:10:06 -0600 (MDT)
Subject: [TUHS]  microfiche scanning news
Message-ID: <CMM.0.95.0.1426950605.beebe@psi.math.utah.edu>

There was a posting on the SIMH list today from Joerg Hoppe
<j_hoppe at t-online.de> about a project to build a microfiche scanner
that has now successfully converted 53,545 document pages to
electronic form, and the files are being uploaded to the PDP-11
section of bitsavers.org. The scanner is described here:

        http://retrocmp.com/projects/scanning-micro-fiches
        
There are links on that page to the rest of the story.  It is an
amazing piece of work for a single person.


-------------------------------------------------------------------------------
- Nelson H. F. Beebe                    Tel: +1 801 581 5254                  -
- University of Utah                    FAX: +1 801 581 4148                  -
- Department of Mathematics, 110 LCB    Internet e-mail: beebe at math.utah.edu  -
- 155 S 1400 E RM 233                       beebe at acm.org  beebe at computer.org -
- Salt Lake City, UT 84112-0090, USA    URL: http://www.math.utah.edu/~beebe/ -
-------------------------------------------------------------------------------


From gregg.drwho8 at gmail.com  Sun Mar 22 02:13:26 2015
From: gregg.drwho8 at gmail.com (Gregg Levine)
Date: Sat, 21 Mar 2015 12:13:26 -0400
Subject: [TUHS] microfiche scanning news
In-Reply-To: <CMM.0.95.0.1426950605.beebe@psi.math.utah.edu>
References: <CMM.0.95.0.1426950605.beebe@psi.math.utah.edu>
Message-ID: <CAC5iaNGNhPoeEG17CQWVWexra4FXm2m60Q0MFfE2-SjcU+9M2w@mail.gmail.com>

Hello!
He wrote back regarding that concept of sending the completed scans to
the PDP-11 location on Bitsavers. Basically he sent  a note to Al
regarding all of them. He's not directly sure when the process will be
complete.

Soon I expect.
-----
Gregg C Levine gregg.drwho8 at gmail.com
"This signature fought the Time Wars, time and again."


On Sat, Mar 21, 2015 at 11:10 AM, Nelson H. F. Beebe
<beebe at math.utah.edu> wrote:
> There was a posting on the SIMH list today from Joerg Hoppe
> <j_hoppe at t-online.de> about a project to build a microfiche scanner
> that has now successfully converted 53,545 document pages to
> electronic form, and the files are being uploaded to the PDP-11
> section of bitsavers.org. The scanner is described here:
>
>         http://retrocmp.com/projects/scanning-micro-fiches
>
> There are links on that page to the rest of the story.  It is an
> amazing piece of work for a single person.
>
>
> -------------------------------------------------------------------------------
> - Nelson H. F. Beebe                    Tel: +1 801 581 5254                  -
> - University of Utah                    FAX: +1 801 581 4148                  -
> - Department of Mathematics, 110 LCB    Internet e-mail: beebe at math.utah.edu  -
> - 155 S 1400 E RM 233                       beebe at acm.org  beebe at computer.org -
> - Salt Lake City, UT 84112-0090, USA    URL: http://www.math.utah.edu/~beebe/ -
> -------------------------------------------------------------------------------
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs


From cubexyz at gmail.com  Wed Mar 25 23:16:26 2015
From: cubexyz at gmail.com (Mark Longridge)
Date: Wed, 25 Mar 2015 09:16:26 -0400
Subject: [TUHS] Three Questions
Message-ID: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>

Ok, the first question is:

Has anyone got Unix sysv running on PDP-11 via simh?
I downloaded some files from archive.org which included the file
'sys_V_tape' but so far I haven't got anywhere with it. Looks
interesting though.

Second question is:

What is the deal with Unix version 8? Except for the manuals v8 seems
to have disappeared into the twilight zone. Wikipedia doesn't say
much, only "Used internally, and only licensed for educational use".
So can we look at the source code? Was it sold in binary form only?

Ok, now the big question:

Does anything at all exist of PDP-7 Unics? All I know about is that
there was a B language interpreter. Maybe a printout of the manual has
survived?

Mark


From clemc at ccc.com  Wed Mar 25 23:30:59 2015
From: clemc at ccc.com (Clem Cole)
Date: Wed, 25 Mar 2015 09:30:59 -0400
Subject: [TUHS] Three Questions
In-Reply-To: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>
References: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>
Message-ID: <CAC20D2Oi=d+AyJ8J656fvc5ptwVxtoW9ChcKfbj171eUGp-j9w@mail.gmail.com>

On Wed, Mar 25, 2015 at 9:16 AM, Mark Longridge <cubexyz at gmail.com> wrote:

> Does anything at all exist of PDP-7 Unics? All I know about is that
> there was a B language interpreter. Maybe a printout of the manual has
> survived?
>

The
​ ​
restoration of 1st Edition UNIX can be found at
​ ​
https://code.google.com/p/unix-jun72/
​ although since Google Code is going away, I hope Warren can get it moved
to his own archives and/or bitsavers.

Clem​
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20150325/e0ccab24/attachment.html>

From crossd at gmail.com  Wed Mar 25 23:39:06 2015
From: crossd at gmail.com (Dan Cross)
Date: Wed, 25 Mar 2015 09:39:06 -0400
Subject: [TUHS] Three Questions
In-Reply-To: <CAC20D2Oi=d+AyJ8J656fvc5ptwVxtoW9ChcKfbj171eUGp-j9w@mail.gmail.com>
References: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>
 <CAC20D2Oi=d+AyJ8J656fvc5ptwVxtoW9ChcKfbj171eUGp-j9w@mail.gmail.com>
Message-ID: <CAEoi9W76ANwHD0odRGMBmXvkvwDwagew55eqF==M2QKjAme3tw@mail.gmail.com>

On Wed, Mar 25, 2015 at 9:30 AM, Clem Cole <clemc at ccc.com> wrote:

> On Wed, Mar 25, 2015 at 9:16 AM, Mark Longridge <cubexyz at gmail.com> wrote:
>
>> Does anything at all exist of PDP-7 Unics? All I know about is that
>> there was a B language interpreter. Maybe a printout of the manual has
>> survived?
>>
>
> The
> ​ ​
> restoration of 1st Edition UNIX can be found at
> ​ ​
> https://code.google.com/p/unix-jun72/
> ​ although since Google Code is going away, I hope Warren can get it moved
> to his own archives and/or bitsavers.
>

First edition is still PDP-11 Unix, I believe.

I remember Dennis Ritchie showing me the source to PDP-7 Unix in his office
once.  It was in a bound book of sorts; the pages were copy from a line
printer.  I don't know what happened to that...does anyone have a copy?

        - Dan C.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20150325/cca83033/attachment.html>

From norman at oclsc.org  Thu Mar 26 00:00:04 2015
From: norman at oclsc.org (Norman Wilson)
Date: Wed, 25 Mar 2015 10:00:04 -0400 (EDT)
Subject: [TUHS]  Three Questions
Message-ID: <20150325140004.9D7A31DE416@lignose.oclsc.org>

Mark Longridge:

  What is the deal with Unix version 8? Except for the manuals v8 seems
  to have disappeared into the twilight zone. Wikipedia doesn't say
  much, only "Used internally, and only licensed for educational use".
  So can we look at the source code? Was it sold in binary form only?

=======

The Eighth Edition system was never released in any general way,
only to a few educational institutions (I forget the number but
it was no more than a dozen) under specific letter agreements that
forbade redistribution.  It was never sold, in source or binary or
any other form; the tape included a bootstrap image and full source
code.

I was involved in all this--in fact one of the first nontrivial
things I did after arriving at Bell Labs was to help Dennis assemble
the tape--but that was more than 30 years ago and the details have
faded.  The system as distributed ran only on the VAX-11/750 and
11/780.  The bootstrap image on the tape was probably more restrictive
than that; if one of the licensees needed something different to
get started we would have tried to make it, but I don't remember
whether that ever happened.

Later systems (loosely corresponding to the Ninth and Tenth editions
of the manual) ran on a somewhat wider set of VAXes, in particular
the MicroVAX II and III and the VAX 8700 and 8550 (but not the dual-
processor 8800).  There was never a real distribution of either of
those systems, though a few sites made special requests and got
hand-crafted snapshots under the same restrictive letter agreement.

So far as I know, no Research UNIX system after 7/e has ever been made
available under anything but a special letter agreement.  There was
at one point some discussion amongst several interested parties
(including me and The Esteemed Warren Toomey) about strategies to
open up the later source code, but that was quashed by the IBM vs
The SCO Group lawsuit.  It would likely be very hard to make happen
now, because I doubt there's anyone left inside Bell Labs with both
the influence and the interest, though I'd be quite happy to be
proven wrong on that.

I know of one place in the world where (a descendant of) that
system is still running, but I am not at the moment in a position
to say where that is.  I do know, however, of at least two places
where there are safe copies of the source code, so it is unlikely
to disappear from the historic record even if that record cannot
be made open for a long time.

Norman Wilson
Toronto ON
(Computing Science Research Centre, Bell Labs, 1984-1990)


From wkt at tuhs.org  Thu Mar 26 06:51:17 2015
From: wkt at tuhs.org (Warren Toomey)
Date: Thu, 26 Mar 2015 07:51:17 +1100
Subject: [TUHS] Three Questions
In-Reply-To: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>
References: <CADxT5N6C9v5ctN7E_n6SDumESwLu9ZMSz1ssfQSXGpisJhR3Mg@mail.gmail.com>
Message-ID: <20150325205117.GA22992@www.oztivo.net>

On Wed, Mar 25, 2015 at 09:16:26AM -0400, Mark Longridge wrote:
> Does anything at all exist of PDP-7 Unics? All I know about is that
> there was a B language interpreter. Maybe a printout of the manual has
> survived?

I asked Dennis Ritchie this question a few times. He did say that he had
fragmentary printouts of some source code. Then in 2009 he wrote:

  In other news, I have found the book that has the listings
  that I knew I had, that of (some) of the user-level commands.
  I wonder what's the best way to get it scanned?

That's as far as we got. Now that he has passed on, I imagine that the
task of obtaining the book is nigh on impossible. I suppose that raises
the question, did Dennis' computing effects get "saved" in any way?

Cheers, Warren

P.S. I must do the Google code to github migration, thanks Clem.


From doug at cs.dartmouth.edu  Thu Mar 26 11:36:41 2015
From: doug at cs.dartmouth.edu (Doug McIlroy)
Date: Wed, 25 Mar 2015 21:36:41 -0400
Subject: [TUHS] Three Questions
Message-ID: <201503260136.t2Q1afNX031968@coolidge.cs.dartmouth.edu>

> Does anything at all exist of PDP-7 Unics? All I know about is that
> there was a B language interpreter. Maybe a printout of the manual has
> survived?

There was no manual. 

doug


