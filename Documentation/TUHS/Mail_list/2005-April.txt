From cfmd at bredband.net  Mon Apr 25 06:55:11 2005
From: cfmd at bredband.net (Magnus Danielson)
Date: Sun, 24 Apr 2005 22:55:11 +0200 (CEST)
Subject: [pups] Seeking info on SBC11/21+ Falcon Plus
Message-ID: <20050424.225511.125377820.cfmd@bredband.net>

Dear all,

I'm sitting here with a M7676 SBC11/21+ Falcon Plus card. It came with a
BA11-VA chassi and a custom A/D card (controlled over the parallel bus).

I have been scanning the Internet a number of times, but have had no luck in
finding much material related. It has the T11 (DC310) chip, a pair of DC 319
serial chips and some unknown chip called DC331 "FALCON". I've found a few
related hints about it in the Micronotes (about the 82S100 PLA chip among
other things). I've also done some reverse-engineering, so part of the
schematic is know to me. But, since I have no plans of reverse-engineering the
DC331 chip getting the hands on some hard documents would be much apprechiated.

I also have a DEQNA card that I got from a friend. I am missing out on the AUI
port, so any information (schematic would be great) beyond the user guide is
appreachiated.

Cheers,
Magnus

From michael_davidson at pacbell.net  Mon Apr 25 10:12:36 2005
From: michael_davidson at pacbell.net (Michael Davidson)
Date: Sun, 24 Apr 2005 17:12:36 -0700
Subject: [pups] Seeking info on SBC11/21+ Falcon Plus
In-Reply-To: <20050424.225511.125377820.cfmd@bredband.net>
References: <20050424.225511.125377820.cfmd@bredband.net>
Message-ID: <426C35F4.6050905@pacbell.net>

Magnus Danielson wrote:

>Dear all,
>
>I'm sitting here with a M7676 SBC11/21+ Falcon Plus card. It came with a
>BA11-VA chassi and a custom A/D card (controlled over the parallel bus).
>
>I have been scanning the Internet a number of times, but have had no luck in
>finding much material related. It has the T11 (DC310) chip, a pair of DC 319
>serial chips and some unknown chip called DC331 "FALCON". I've found a few
>related hints about it in the Micronotes (about the 82S100 PLA chip among
>other things). I've also done some reverse-engineering, so part of the
>schematic is know to me. But, since I have no plans of reverse-engineering the
>DC331 chip getting the hands on some hard documents would be much apprechiated
>
While it isn't identical, you might find the user guide for the original 
M8063 Falcon SBC 11/21
to be at least somewhat useful. It can be found at:

http://www.bitsavers.org/pdf/dec/pdp11/1121/EK-KXT11-UG-PR1.pdf

As far as I can remember the Falcon and Falcon+ are functionally very 
similar with the Falcon+
supporting larger memory devices and having a more flexible set of 
options for the memory map.

Chapter 6 of the 1985 edition of the Microcomputer Products Handbook 
lists all of the configuration
options for the Falcon+ and also shows the DC331 at E20 as being a "gate 
array". I have a copy
of this manual and could send you a copy of the relevant pages if that 
would be helpful.

Michael Davidson


From wes.parish at paradise.net.nz  Thu Apr  7 23:46:55 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Fri, 08 Apr 2005 01:46:55 +1200
Subject: [TUHS] franz-lisp
Message-ID: <200504080146.55579.wes.parish@paradise.net.nz>

I downloaded this a short while ago and am interested in trying it out on my 
Linux box and also on my Windows machine, through the MinGW compiler suite.

But lispconf is demanding cvt.awk and it's not on my system.

Can anyone help me out?

Thanks

Wesley Parish
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From mparson at bl.org  Fri Apr  8 00:42:40 2005
From: mparson at bl.org (Mike Parson)
Date: Thu, 7 Apr 2005 09:42:40 -0500
Subject: [TUHS] franz-lisp
In-Reply-To: <200504080146.55579.wes.parish@paradise.net.nz>
References: <200504080146.55579.wes.parish@paradise.net.nz>
Message-ID: <20050407144240.GA25503@bl.org>

On Fri, Apr 08, 2005 at 01:46:55AM +1200, Wesley Parish wrote:
> I downloaded this a short while ago and am interested in trying it out on my 
> Linux box and also on my Windows machine, through the MinGW compiler suite.
> 
> But lispconf is demanding cvt.awk and it's not on my system.
> 
> Can anyone help me out?

It seems to be in this archive:

http://www-2.cs.cmu.edu/afs/cs/project/ai-repository/ai/lang/others/franzlsp/op38_ucd/ucfranz.tgz

-- 
Michael Parson
mparson at bl.org

From wes.parish at paradise.net.nz  Sun Apr 10 20:35:45 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Sun, 10 Apr 2005 22:35:45 +1200
Subject: [TUHS] I came across this
Message-ID: <200504102235.45964.wes.parish@paradise.net.nz>

here:
http://www.finseth.com/~fin/emacs.html

Anyone got any idea where it might be hiding out?

Thanks

Wesley Parish
//
Emacs [toc] 

      name: Emacs
      last changed/verified: 1994-12-20
      original distribution: 1975
      version: 165
      base language: MIDAS (PDP10/DEC-20 assembly language)
      implementation language: TECO
      extension language: TECO
      scope of implementation: extensible
      hardware/software requirements: PDP10/ITS or DEC-20/TOPS-20
      organization/author:
              Richard M. Stallman
              MIT AI Lab/MIT Lab. for Comp. Sci.
              545 Technology Square
              Cambridge MA 02139
              USA

 

Note: this is the original free, anonymous FTP from ? 
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From menace3society at gnu-darwin.org  Mon Apr 11 01:47:59 2005
From: menace3society at gnu-darwin.org (menace3society at gnu-darwin.org)
Date: Sun, 10 Apr 2005 08:47:59 -0700 (PDT)
Subject: [TUHS] I came across this
In-Reply-To: <200504102235.45964.wes.parish@paradise.net.nz>
References: <200504102235.45964.wes.parish@paradise.net.nz>
Message-ID: <58676.159.28.161.74.1113148079.squirrel@webmail.gnu-darwin.org>

> here:
> http://www.finseth.com/~fin/emacs.html
>
> Anyone got any idea where it might be hiding out?
>
> Thanks
>
> Wesley Parish
Don't know about version 165, but pdp-10.trailing-edge.com has version 170
+ source for download/browse.

Evan

From lars at nocrew.org  Fri Apr 15 18:28:41 2005
From: lars at nocrew.org (Lars Brinkhoff)
Date: Fri, 15 Apr 2005 10:28:41 +0200
Subject: [TUHS] I came across this
In-Reply-To: <58676.159.28.161.74.1113148079.squirrel@webmail.gnu-darwin.org>
	(menace3society@gnu-darwin.org's
	message of "Sun, 10 Apr 2005 08:47:59 -0700 (PDT)")
References: <200504102235.45964.wes.parish@paradise.net.nz>
	<58676.159.28.161.74.1113148079.squirrel@webmail.gnu-darwin.org>
Message-ID: <85d5sw4f5y.fsf@junk.nocrew.org>

menace3society at gnu-darwin.org writes:
>> here:
>> http://www.finseth.com/~fin/emacs.html
>> Anyone got any idea where it might be hiding out?
> Don't know about version 165, but pdp-10.trailing-edge.com has version 170
> + source for download/browse.

Rich Alderson is the current maintainer.

Here are my notes for installing on TOPS-20 with the KLH10 emulator:
  http://pdp10.nocrew.org/software/emacs-install.txt

I used this tape image; may be identical to the Trailing-Edge one:
  http://pdp10.nocrew.org/software/emacs.tap.bz2

From martin.hardie at gmail.com  Wed Apr 20 18:55:39 2005
From: martin.hardie at gmail.com (martin hardie)
Date: Wed, 20 Apr 2005 10:55:39 +0200
Subject: [TUHS] unix non disclosure clauses
Message-ID: <7ff9538b050420015572373a6d@mail.gmail.com>

Hi i joined this list as I found some intersting stuff in its archives
and I am working on my Phd in law concerning the logic and rhetoric of
FOSS and i thought maybe the list would be a good source of
knowledgable information.

I am currently  proofing a draft thesis chapter I have put together on
the early history of Unix and have a question or two arising from text
of the early  licences.

The 1974 licence to the Catholic University in Holland (I guess this
was to Andy Tanenbaum) has a confidentiality clause in it. I presume
this was a standard clause.

That is interesting from lots of perspectives - the myth of a unix
commons, which we both know  is a myth in the GNUish sense although
people like Lessig still say it in their tomes; and from the
perspective that copyright or patents where not used to cover the code
but confidential inofrmation - this resonates with my work with
Aboriginal  artists in Australia and their communal system of
knowledge production and with the notion of trust and equity which I
am building towards in this research.

But right now what interests me is a bit more in the context of
contemporary "licence fetishism" or the way licences and IP were
viewed back then. I am sort of trying to deal with the way that many
commentators (like Lessig, Wayner and even Raymond) credit changes in
unix and linux to legal command. I just don't buy that but position
them more in the context of the globalisation of production.

Anyway, the question -  the licences prohibited dissemination of Unix
to third parties - eg in the case of universities the system could
only  be given/shown to students and employees.

How then was the question of bugs, fixes and updates dealt with? Did
everything come back to Bell and then get dealt with from there.  IE
the question of who controllled "R&D"? Did universities talk directly
to each other? And if so when did this become a problem for AT&T? If
at all? If they did was there any conception that they  were breaking
the licence conditions?

I am also intrigued about Raymond's comment that Ken quietly shipped
out copies of the program with a note "love Ken". Is this based in
fact? was it a covert operation? And is it tied into the matter of
turning a blind eye to licence conditions eg the unis talking to each
other directly?

Is that clear? If the uni's were talking to each other and Ken was
sending out gift wrapped parcels ......... maybe there was a commons
but not one annointed by law.....


Thanks

Martin

From wb at freebie.xs4all.nl  Wed Apr 20 21:05:58 2005
From: wb at freebie.xs4all.nl (Wilko Bulte)
Date: Wed, 20 Apr 2005 13:05:58 +0200
Subject: [TUHS] unix non disclosure clauses
In-Reply-To: <7ff9538b050420015572373a6d@mail.gmail.com>
References: <7ff9538b050420015572373a6d@mail.gmail.com>
Message-ID: <20050420110558.GA47232@freebie.xs4all.nl>

On Wed, Apr 20, 2005 at 10:55:39AM +0200, martin hardie wrote..
> Hi i joined this list as I found some intersting stuff in its archives
> and I am working on my Phd in law concerning the logic and rhetoric of
> FOSS and i thought maybe the list would be a good source of
> knowledgable information.
> 
> I am currently  proofing a draft thesis chapter I have put together on
> the early history of Unix and have a question or two arising from text
> of the early  licences.
> 
> The 1974 licence to the Catholic University in Holland (I guess this

That must be the KUN.  Katholieke Universiteit Nijmegen.

> was to Andy Tanenbaum) has a confidentiality clause in it. I presume
> this was a standard clause.

You might want to ask hjt at atcomputing.nl, as he was one of the folks
that got it into the KUN.

> That is interesting from lots of perspectives - the myth of a unix
> commons, which we both know  is a myth in the GNUish sense although
> people like Lessig still say it in their tomes; and from the
> perspective that copyright or patents where not used to cover the code
> but confidential inofrmation - this resonates with my work with
> Aboriginal  artists in Australia and their communal system of
> knowledge production and with the notion of trust and equity which I
> am building towards in this research.
> 
> But right now what interests me is a bit more in the context of
> contemporary "licence fetishism" or the way licences and IP were
> viewed back then. I am sort of trying to deal with the way that many
> commentators (like Lessig, Wayner and even Raymond) credit changes in
> unix and linux to legal command. I just don't buy that but position
> them more in the context of the globalisation of production.
> 
> Anyway, the question -  the licences prohibited dissemination of Unix
> to third parties - eg in the case of universities the system could
> only  be given/shown to students and employees.
> 
> How then was the question of bugs, fixes and updates dealt with? Did
> everything come back to Bell and then get dealt with from there.  IE
> the question of who controllled "R&D"? Did universities talk directly
> to each other? And if so when did this become a problem for AT&T? If
> at all? If they did was there any conception that they  were breaking
> the licence conditions?
> 
> I am also intrigued about Raymond's comment that Ken quietly shipped
> out copies of the program with a note "love Ken". Is this based in
> fact? was it a covert operation? And is it tied into the matter of
> turning a blind eye to licence conditions eg the unis talking to each
> other directly?
> 
> Is that clear? If the uni's were talking to each other and Ken was
> sending out gift wrapped parcels ......... maybe there was a commons
> but not one annointed by law.....
> 
> 
> Thanks
> 
> Martin
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> http://minnie.tuhs.org/mailman/listinfo/tuhs
> 
--- end of quoted text ---

-- 
Wilko

From kwall at kurtwerks.com  Wed Apr 20 22:28:20 2005
From: kwall at kurtwerks.com (Kurt Wall)
Date: Wed, 20 Apr 2005 08:28:20 -0400
Subject: [TUHS] unix non disclosure clauses
In-Reply-To: <7ff9538b050420015572373a6d@mail.gmail.com>
References: <7ff9538b050420015572373a6d@mail.gmail.com>
Message-ID: <200504200828.20698.kwall@kurtwerks.com>

On Wednesday 20 April 2005 04:55, martin hardie enlightened us thusly:
> Hi i joined this list as I found some intersting stuff in its
> archives and I am working on my Phd in law concerning the logic and
> rhetoric of FOSS and i thought maybe the list would be a good source
> of
> knowledgable information.
>
> I am currently  proofing a draft thesis chapter I have put together
> on the early history of Unix and have a question or two arising from
> text of the early  licences.
>
> The 1974 licence to the Catholic University in Holland (I guess this
> was to Andy Tanenbaum) has a confidentiality clause in it. I presume
> this was a standard clause.
>
> That is interesting from lots of perspectives - the myth of a unix
> commons, which we both know  is a myth in the GNUish sense although
> people like Lessig still say it in their tomes; and from the
> perspective that copyright or patents where not used to cover the
> code but confidential inofrmation - this resonates with my work with
> Aboriginal  artists in Australia and their communal system of
> knowledge production and with the notion of trust and equity which I
> am building towards in this research.
>
> But right now what interests me is a bit more in the context of
> contemporary "licence fetishism" or the way licences and IP were
> viewed back then. I am sort of trying to deal with the way that many
> commentators (like Lessig, Wayner and even Raymond) credit changes in
> unix and linux to legal command. I just don't buy that but position
> them more in the context of the globalisation of production.
>
> Anyway, the question -  the licences prohibited dissemination of Unix
> to third parties - eg in the case of universities the system could
> only  be given/shown to students and employees.

The 1956 consent decree required AT&T to provide licenses
to patented technology when asked. It isn't hard to imagine that the
seven years of anti-trust litigation, culminating in the consent
decree, cast a long shadow when it came to how AT&T wrote and
enforced the UNIX licenses.

> How then was the question of bugs, fixes and updates dealt with? Did
> everything come back to Bell and then get dealt with from there.  IE
> the question of who controllled "R&D"? Did universities talk directly
> to each other? And if so when did this become a problem for AT&T? If
> at all? If they did was there any conception that they  were breaking
> the licence conditions?
>
> I am also intrigued about Raymond's comment that Ken quietly shipped
> out copies of the program with a note "love Ken". Is this based in
> fact? was it a covert operation? And is it tied into the matter of
> turning a blind eye to licence conditions eg the unis talking to each
> other directly?

Judge for yourself whether it is fact or not: 
http://www.groklaw.net/article.php?story=20050414215646742
See particularly Chapter 3, which establishes pretty clearly that
almost all of the users of the UNIX system were talking to each
other.

> Is that clear? If the uni's were talking to each other and Ken was
> sending out gift wrapped parcels ......... maybe there was a commons
> but not one annointed by law.....

Kurt

From asbesto at freaknet.org  Wed Apr 20 22:38:00 2005
From: asbesto at freaknet.org (asbesto)
Date: Wed, 20 Apr 2005 12:38:00 +0000
Subject: [TUHS] Help about a DATA GENERAL ECLIPSE MV-7800 with DG-UX
Message-ID: <20050420123800.GD5119@freaknet.org>


Hi all,

we recently received this fantastic machine for our 
computer museum. 

We need some help because we can't find any documentation 
about the "console cable", to connect a tty terminal 
to it, and make it boot (or test the cpu, or whatever)

we think we have only a part of the manuals, so we can't 
make progress booting it and we don't want to make
casual testing ...

can someone help?

some documentation images can be found here:

http://dyne.org/~asbesto/missionecompiuta1/
http://dyne.org/~asbesto/missionecompiuta2/
http://dyne.org/~asbesto/missionecompiuta3/

sorry for some crazy and funny images - we are really
crazy people :D

-- 
[ asbesto : IW9HGS : freaknet medialab : radiocybernet : poetry ]
[ http://freaknet.org/asbesto http://papuasia.org/radiocybernet ]
[ http://www.emergelab.org :: NON SCRIVERMI USANDO LE ACCENTATE ]
[ *I DELETE* EMAIL > 100K, ATTACHMENTS, HTML, M$-WORD DOC, SPAM ]

-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050420/3714e0e4/attachment.sig>

From jkunz at unixag-kl.fh-kl.de  Thu Apr 21 01:41:09 2005
From: jkunz at unixag-kl.fh-kl.de (Jochen Kunz)
Date: Wed, 20 Apr 2005 17:41:09 +0200
Subject: [TUHS] Help about a DATA GENERAL ECLIPSE MV-7800 with DG-UX
In-Reply-To: <20050420123800.GD5119@freaknet.org>
References: <20050420123800.GD5119@freaknet.org>
Message-ID: <20050420174109.5772c755.jkunz@unixag-kl.fh-kl.de>

On Wed, 20 Apr 2005 12:38:00 +0000
asbesto <asbesto at freaknet.org> wrote:

> We need some help because we can't find any documentation 
> about the "console cable", to connect a tty terminal 
> to it, and make it boot (or test the cpu, or whatever)
I sugest to ask on the Classic Computer Mailing List:
http://www.classiccmp.org/lists.html
There are several owners of DG equipment on that list.

BTW: Nice machine! :-)
-- 


tschüß,
       Jochen

Homepage: http://www.unixag-kl.fh-kl.de/~jkunz/


From wes.parish at paradise.net.nz  Thu Apr 21 22:29:56 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Fri, 22 Apr 2005 00:29:56 +1200
Subject: [TUHS] (resend) Not strictly TUHS, but
Message-ID: <200504220029.56730.wes.parish@paradise.net.nz>

(Not receiving a reply back then, I'm going to ask again.  My apologies for 
any inconvenience.)  wotthehell, I'm going to ask anyway.

Soemtime during the late 1980s, Clarkson U., came out with a GPLed MS-DOS word 
processor package called Galahad, released under the Galahad Public License, 
which is a rebadged GNU Emacs Public License.  I've sent them the CS 
Professor an email requesting the source.

I was wondering if anyone on this list might have the sources, because as yet 
I've had no reply.

Thanks

Wesley Parish
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From newsham at lava.net  Sun Apr 24 10:10:22 2005
From: newsham at lava.net (Tim Newsham)
Date: Sat, 23 Apr 2005 14:10:22 -1000 (HST)
Subject: [TUHS] simh 6th ed?
Message-ID: <Pine.BSI.4.61.0504231405080.26630@malasada.lava.net>

Hi,
   I'm playing with simh and the 6th ed software pack (uv6swre
http://simh.trailing-edge.com/software.html).
It turns out that it didn't have /usr/sys, so I grabbed
sources from http://miffy.tom-yam.or.jp/2238/rl/ (they had
an RL image and a kernel patched with rl support).
Also strangely the kernel doesnt print the normal (c) when
booting.  Is the unix kernel that comes with the software patch
hacked up?

Anyway, I'm now trying to build a kernel and having no success.
In /usr/sys running "sh run" works properly and it makes a
bunch of /*unix files.  When I try to boot them though it
just hangs.  I get no output.  I've tried building rkunix with
and without the m45.s bits commented out.

Has anyone had luck with building the kernel?  Any pointers?

Tim Newsham
http://www.lava.net/~newsham/

From jpeek at jpeek.com  Fri Apr 29 02:50:21 2005
From: jpeek at jpeek.com (Jerry Peek)
Date: Thu, 28 Apr 2005 09:50:21 -0700
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
Message-ID: <8449.1114707021@jpeek.com>

Hi everyone.  I'm a short-time UNIX user (I started in 1981 :)
and also a columnist for Linux Magazine (in the US: not the UK
flavour).  I just came across TUHS while I was searching for a
V7 cp(1) manpage.  (I found it, BTW, via Warren Toomey's page
http://mirror.cc.vt.edu/pub/projects/Ancient_Unix/Documentation/PUPS/manpages.html.)

I'm writing a series of columns on "What's GNU in Old Utilities".
It describes new features of GNU utilities like cat(1) and
contrasts them to "how we used to do it."  I'd like to mention
TUHS in the third column, which should be out in August.  It
seems that TUHS is alive and well.  If any of you have comments
or complaints about that idea, though, would you please let me
know before May 1 -- which is when the column is due?  Thanks.

Jerry
-- 
Jerry Peek, jpeek at jpeek.com, http://www.jpeek.com/

From newsham at lava.net  Fri Apr 29 11:55:32 2005
From: newsham at lava.net (Tim Newsham)
Date: Thu, 28 Apr 2005 15:55:32 -1000 (HST)
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <8449.1114707021@jpeek.com>
References: <8449.1114707021@jpeek.com>
Message-ID: <Pine.BSI.4.61.0504281554360.11177@malasada.lava.net>

> I'm writing a series of columns on "What's GNU in Old Utilities".
> It describes new features of GNU utilities like cat(1) and
> contrasts them to "how we used to do it."

How about mentioning:

   http://plan9.bell-labs.com/cm/cs/doc/84/kp.ps.gz

It describes "new" features of BSD utilities like cat(1)
and contrasts them with how things should be done.

> Jerry
> Jerry Peek, jpeek at jpeek.com, http://www.jpeek.com/

Tim Newsham
http://www.lava.net/~newsham/

From dmr at plan9.bell-labs.com  Fri Apr 29 13:55:39 2005
From: dmr at plan9.bell-labs.com (dmr at plan9.bell-labs.com)
Date: Thu, 28 Apr 2005 23:55:39 -0400
Subject: [TUHS] re: Mention TUHS in Linux Magazine (US)?
Message-ID: <fe0ce340145d44c8043ce280c28cefb3@plan9.bell-labs.com>

Jerry Peek wrote:

  > I'm writing a series of columns on "What's GNU in Old Utilities".
  > It describes new features of GNU utilities like cat(1) and
  > contrasts them to "how we used to do it."

The most famous rant on this topic (actually BSD, not
GNU) was by Rob Pike, "UNIX Style: cat -v considered harmful"

I couldn't find the thing itself (it's from a Usenix conference
in 1983) but there's a .ps version of a contemporary paper
with most of the content under

   http://gaul.org/files/cat_-v_considered_harmful.html

	Dennis


From grog at lemis.com  Fri Apr 29 17:04:48 2005
From: grog at lemis.com (Greg 'groggy' Lehey)
Date: Fri, 29 Apr 2005 16:34:48 +0930
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <8449.1114707021@jpeek.com>
References: <8449.1114707021@jpeek.com>
Message-ID: <20050429070448.GO70593@wantadilla.lemis.com>

On Thursday, 28 April 2005 at  9:50:21 -0700, Jerry Peek wrote:
> Hi everyone.  I'm a short-time UNIX user (I started in 1981 :)
> and also a columnist for Linux Magazine (in the US: not the UK
> flavour).  I just came across TUHS while I was searching for a
> V7 cp(1) manpage.  (I found it, BTW, via Warren Toomey's page
> http://mirror.cc.vt.edu/pub/projects/Ancient_Unix/Documentation/PUPS/manpages.html.)
>
> I'm writing a series of columns on "What's GNU in Old Utilities".

Heh.  This sounds more like "What's Old in GNU Utilities".

> It describes new features of GNU utilities like cat(1) and contrasts
> them to "how we used to do it."  I'd like to mention TUHS in the
> third column, which should be out in August.  It seems that TUHS is
> alive and well.  If any of you have comments or complaints about
> that idea, though, would you please let me know before May 1 --
> which is when the column is due?

Heh.  Thanks for the long warning :-)

I can't speak for Warren, but I'd be surprised if he were not
enthusiastically in favour.  I certainly am.  Unfortunately, if you
haven't heard from him yet, you probably won't by the submission
deadline.

Greg
--
Finger grog at lemis.com for PGP public key.
See complete headers for address and phone numbers.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 187 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050429/9cc4d935/attachment.sig>

From wkt at tuhs.org  Fri Apr 29 19:02:58 2005
From: wkt at tuhs.org (Warren Toomey)
Date: Fri, 29 Apr 2005 19:02:58 +1000
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <20050429070448.GO70593@wantadilla.lemis.com>
References: <8449.1114707021@jpeek.com>
	<20050429070448.GO70593@wantadilla.lemis.com>
Message-ID: <20050429090258.GA21780@minnie.tuhs.org>

On Fri, Apr 29, 2005 at 04:34:48PM +0930, Greg 'groggy' Lehey wrote:
> > It describes new features of GNU utilities like cat(1) and contrasts
> > them to "how we used to do it."  I'd like to mention TUHS in the
> > third column, which should be out in August.  It seems that TUHS is
> > alive and well.  If any of you have comments or complaints about
> > that idea, though, would you please let me know before May 1 --
> > which is when the column is due?
> 
> I can't speak for Warren, but I'd be surprised if he were not
> enthusiastically in favour.  I certainly am.  Unfortunately, if you
> haven't heard from him yet, you probably won't by the submission
> deadline.

I mailed him back yesterday, unfortunately my reply went to Tim and not
the list. I did mention cat -v considered harmful, and Gunnar Ritter's
Heirloom Toolchest.

Cheers,
	Warren

From wkt at tuhs.org  Fri Apr 29 20:05:13 2005
From: wkt at tuhs.org (Warren Toomey)
Date: Fri, 29 Apr 2005 20:05:13 +1000
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <200504290913.j3T9DdGA016881@skeeve.com>
References: <200504290913.j3T9DdGA016881@skeeve.com>
Message-ID: <20050429100513.GA22908@minnie.tuhs.org>

On Fri, Apr 29, 2005 at 12:13:39PM +0300, Aharon Robbins wrote:
> Where can we get "cat -v considered harmful" and do you want to start
> archiving such papers on the TUHS site too?  There's lots by Henry Spencer
> and Geoff Collyer on C and Unix system from the 80s that would be worth
> having in an accessable place.
> Arnold

That's a good idea.
	Warren

From jpeek at jpeek.com  Sat Apr 30 02:20:16 2005
From: jpeek at jpeek.com (Jerry Peek)
Date: Fri, 29 Apr 2005 09:20:16 -0700
Subject: [TUHS] Mention TUHS in Linux Magazine (US)? 
In-Reply-To: <20050429090258.GA21780@minnie.tuhs.org> 
References: <8449.1114707021@jpeek.com>
	<20050429070448.GO70593@wantadilla.lemis.com>
	<20050429090258.GA21780@minnie.tuhs.org>
Message-ID: <18766.1114791616@beowulf.gw.com>

Thanks, everyone, for the comments and the helpful pointers!

One of you asked if the articles will be available online. 
The first one will be in the June 2005 print issue; it should
be on the Web by August or so at http://www.linux-mag.com/.

Jerry

From cdl at mpl.ucsd.edu  Sat Apr 30 03:59:21 2005
From: cdl at mpl.ucsd.edu (Carl Lowenstein)
Date: Fri, 29 Apr 2005 10:59:21 -0700 (PDT)
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
Message-ID: <200504291759.j3THxL110157@opihi.ucsd.edu>

> From: Jerry Peek <jpeek at jpeek.com>
> To: tuhs at minnie.tuhs.org
> Date: Thu, 28 Apr 2005 09:50:21 -0700
> Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
> Hi everyone.  I'm a short-time UNIX user (I started in 1981 :)
> and also a columnist for Linux Magazine (in the US: not the UK
> flavour).  I just came across TUHS while I was searching for a
> V7 cp(1) manpage.  (I found it, BTW, via Warren Toomey's page
> http://mirror.cc.vt.edu/pub/projects/Ancient_Unix/Documentation/PUPS/manpages.html.)
> 
> I'm writing a series of columns on "What's GNU in Old Utilities".
> It describes new features of GNU utilities like cat(1) and
> contrasts them to "how we used to do it."  I'd like to mention
> TUHS in the third column, which should be out in August.  It
> seems that TUHS is alive and well.  If any of you have comments
> or complaints about that idea, though, would you please let me
> know before May 1 -- which is when the column is due?  Thanks.

More power to you.  Just keep a sharp eye out for things that
are touted as "new improved GNU features" that have been around
since the days of 6th Edition or 7th Edition Unix.

    carl
-- 
    carl lowenstein         marine physical lab     u.c. san diego
                                                 clowenst at ucsd.edu

From jpeek at jpeek.com  Sat Apr 30 05:10:23 2005
From: jpeek at jpeek.com (Jerry Peek)
Date: Fri, 29 Apr 2005 12:10:23 -0700
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <200504291759.j3THxL110157@opihi.ucsd.edu>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
Message-ID: <11918.1114801823@jpeek.com>

On Fri, 29 Apr 2005 10:59:21 -0700, Carl Lowenstein wrote:
> More power to you.  Just keep a sharp eye out for things that
> are touted as "new improved GNU features" that have been around
> since the days of 6th Edition or 7th Edition Unix.

Thanks for that wise advice, Carl.  I actually started on a VAX
running 4.1 BSD, so I'm not always clear about what's real. ;-)
I've been checking manpages online and trying to keep it straight.

While I'm at it, I wanted to say thanks to Tim Newsham for
reminding me of the paper "Program design in the UNIX environment"
(at http://plan9.bell-labs.com/cm/cs/doc/84/kp.ps.gz) and to Dennis 
for his home page (http://plan9.bell-labs.com/who/dmr/index.html --
which I found, BTW, on Tim's "Links" page).  Both of them have lots 
of fascinating and useful info that took me way back from my GUI
GNU-ey ;-) environment.  I've sat here for a couple of hours,
reading and thinking.  I'll work this into my August column --
set off in a sidebar or some way to call attention to it.
Today's Linux users need to read and understand this stuff.
It was a great reminder for me.  Thanks again, everyone.

Jerry

From kwall at kurtwerks.com  Sat Apr 30 11:19:39 2005
From: kwall at kurtwerks.com (Kurt Wall)
Date: Fri, 29 Apr 2005 21:19:39 -0400
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <11918.1114801823@jpeek.com>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
	<11918.1114801823@jpeek.com>
Message-ID: <200504292119.40082.kwall@kurtwerks.com>

On Friday 29 April 2005 15:10, Jerry Peek enlightened us thusly:
> On Fri, 29 Apr 2005 10:59:21 -0700, Carl Lowenstein wrote:
> > More power to you.  Just keep a sharp eye out for things that
> > are touted as "new improved GNU features" that have been around
> > since the days of 6th Edition or 7th Edition Unix.
>
> Thanks for that wise advice, Carl.  I actually started on a VAX
> running 4.1 BSD, so I'm not always clear about what's real. ;-)
> I've been checking manpages online and trying to keep it straight.
>
> While I'm at it, I wanted to say thanks to Tim Newsham for
> reminding me of the paper "Program design in the UNIX environment"
> (at http://plan9.bell-labs.com/cm/cs/doc/84/kp.ps.gz) and to Dennis
> for his home page (http://plan9.bell-labs.com/who/dmr/index.html --
> which I found, BTW, on Tim's "Links" page).  Both of them have lots
> of fascinating and useful info that took me way back from my GUI
> GNU-ey ;-) environment.  I've sat here for a couple of hours,
> reading and thinking.  I'll work this into my August column --
> set off in a sidebar or some way to call attention to it.
> Today's Linux users need to read and understand this stuff.
> It was a great reminder for me.  Thanks again, everyone.

I quite agree. I started with some SVR3 UNIX but wound up using
Linux because it was free and worked with the hardware I had at
the time. It's a constant source of pleasure to work with the old 
UNIX hands around my shop and learn what my roots are. I even
have some books around here with Jerry Peek's name on them... :-)

Kurt

From wes.parish at paradise.net.nz  Sat Apr 30 17:31:52 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Sat, 30 Apr 2005 19:31:52 +1200
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <200504291759.j3THxL110157@opihi.ucsd.edu>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
Message-ID: <200504301931.53182.wes.parish@paradise.net.nz>

And, FWIW, in one of the few GNUs Bulletins I actually have received, courtesy 
of the FSF, RMS (I think) was advising that with the dropping price in 
memory, GNU hackers could do without worrying about memory size, when it came 
to replicating Unix utilities..

So perhaps that is a point to be taken into consideration?  I know in some 
respects FreeBSD - the only *BSD I've ever gotten around to installing - 
comes across as smaller and faster than Linux.  I hate to think of SysVRx - 
everything I have read about that series of OSes said it was bloated, and the 
SCO SysVR3 I learnt Unix on felt somewhat clumsier than Linux when I first 
got it.  But that's the result of Bash being nicer than sh to a 4DOS user.

Hope this is useful.

Wesley Parish

On Sat, 30 Apr 2005 05:59, Carl Lowenstein wrote:
> > From: Jerry Peek <jpeek at jpeek.com>
> > To: tuhs at minnie.tuhs.org
> > Date: Thu, 28 Apr 2005 09:50:21 -0700
> > Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
> > Hi everyone.  I'm a short-time UNIX user (I started in 1981 :)
> > and also a columnist for Linux Magazine (in the US: not the UK
> > flavour).  I just came across TUHS while I was searching for a
> > V7 cp(1) manpage.  (I found it, BTW, via Warren Toomey's page
> > http://mirror.cc.vt.edu/pub/projects/Ancient_Unix/Documentation/PUPS/manp
> >ages.html.)
> >
> > I'm writing a series of columns on "What's GNU in Old Utilities".
> > It describes new features of GNU utilities like cat(1) and
> > contrasts them to "how we used to do it."  I'd like to mention
> > TUHS in the third column, which should be out in August.  It
> > seems that TUHS is alive and well.  If any of you have comments
> > or complaints about that idea, though, would you please let me
> > know before May 1 -- which is when the column is due?  Thanks.
>
> More power to you.  Just keep a sharp eye out for things that
> are touted as "new improved GNU features" that have been around
> since the days of 6th Edition or 7th Edition Unix.
>
>     carl

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From shoppa at trailing-edge.com  Sat Apr 30 21:35:40 2005
From: shoppa at trailing-edge.com (Tim Shoppa)
Date: Sat, 30 Apr 2005 07:35:40 -0400
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <200504301931.53182.wes.parish@paradise.net.nz>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
 <200504301931.53182.wes.parish@paradise.net.nz>
Message-ID: <42736D8C.nail7GX1WKZCL@mini-me.trailing-edge.com>

Wes Parish wrote:
> was advising that with the dropping price in memory, GNU hackers could
> do without worrying about memory size, when it came to replicating
> Unix utilities..

I sometimes make fun of the strings of #ifdefs and huge size in source
of the GNU utilities, but I've never made (to take a utility that is
simple and takes few if any options) GNU pwd dump core.  I have succesfully
made many commercial Unix implementations of "pwd" dump core!

Tim.

