From wkt at tuhs.org  Wed Jul 24 11:34:10 2013
From: wkt at tuhs.org (Warren Toomey)
Date: Wed, 24 Jul 2013 11:34:10 +1000
Subject: [TUHS] Fwd: A cameo by UNIX in BSTJ
Message-ID: <20130724013409.GB19629@neddie.local.net>

All, I got this interesting e-mail from Poul-Henning a few days ago.
	Warren

----- Forwarded message from Poul-Henning Kamp <phk at phk.freebsd.dk> -----

   Date: Sat, 20 Jul 2013 13:27:27 +0000
   From: Poul-Henning Kamp <phk at phk.freebsd.dk>
   To: wkt at tuhs.org
   Subject: A cameo by UNIX in BSTJ

Some months ago I faced a flight from Denmark to NZ and back again,
so I bought a Kobo eBook reader and reformatted the entire BSTJ to fit
the screen.

That gave me about 100k "pages" to read, plenty for my NZ-flights
and a large number of otherwise wasted moments since then.  Highly
recommeded.

Recently I came over what I belive is the first mention of UNIX in BSTJ.

We all know about the v57i6, July-August 1978 "UNIX Time-Sharing System"
issue, but it transpires that UNIX made a cameo two years earlier
in an article about compression-schemes for TeleFax:

www3.alcatel-lucent.com/bstj/vol55-1976/articles/bstj55-10-1539.pdf

Enjoy,

Poul-Henning

-- 
Poul-Henning Kamp       | UNIX since Zilog Zeus 3.20
phk at FreeBSD.ORG         | TCP/IP since RFC 956
FreeBSD committer       | BSD since 4.3-tahoe
Never attribute to malice what can adequately be explained by incompetence.

----- End forwarded message -----


From lm at bitmover.com  Wed Jul 24 12:47:20 2013
From: lm at bitmover.com (Larry McVoy)
Date: Tue, 23 Jul 2013 19:47:20 -0700
Subject: [TUHS] Fwd: A cameo by UNIX in BSTJ
In-Reply-To: <20130724013409.GB19629@neddie.local.net>
References: <20130724013409.GB19629@neddie.local.net>
Message-ID: <20130724024720.GH28626@bitmover.com>

I'd like to know where I can get the entire BSTJ.  For the record,
Bell Labs was the *the* place when I was growing up.  Reading their
journals was right up there with having an account on slovax at
uwisc and reading the bell labs/BSD source.

On Wed, Jul 24, 2013 at 11:34:10AM +1000, Warren Toomey wrote:
> All, I got this interesting e-mail from Poul-Henning a few days ago.
> 	Warren
> 
> ----- Forwarded message from Poul-Henning Kamp <phk at phk.freebsd.dk> -----
> 
>    Date: Sat, 20 Jul 2013 13:27:27 +0000
>    From: Poul-Henning Kamp <phk at phk.freebsd.dk>
>    To: wkt at tuhs.org
>    Subject: A cameo by UNIX in BSTJ
> 
> Some months ago I faced a flight from Denmark to NZ and back again,
> so I bought a Kobo eBook reader and reformatted the entire BSTJ to fit
> the screen.
> 
> That gave me about 100k "pages" to read, plenty for my NZ-flights
> and a large number of otherwise wasted moments since then.  Highly
> recommeded.
> 
> Recently I came over what I belive is the first mention of UNIX in BSTJ.
> 
> We all know about the v57i6, July-August 1978 "UNIX Time-Sharing System"
> issue, but it transpires that UNIX made a cameo two years earlier
> in an article about compression-schemes for TeleFax:
> 
> www3.alcatel-lucent.com/bstj/vol55-1976/articles/bstj55-10-1539.pdf
> 
> Enjoy,
> 
> Poul-Henning
> 
> -- 
> Poul-Henning Kamp       | UNIX since Zilog Zeus 3.20
> phk at FreeBSD.ORG         | TCP/IP since RFC 956
> FreeBSD committer       | BSD since 4.3-tahoe
> Never attribute to malice what can adequately be explained by incompetence.
> 
> ----- End forwarded message -----
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs

-- 
---
Larry McVoy                lm at bitmover.com           http://www.bitkeeper.com


From ecashin at noserose.net  Thu Jul 25 05:30:41 2013
From: ecashin at noserose.net (Ed Cashin)
Date: Wed, 24 Jul 2013 15:30:41 -0400
Subject: [TUHS] Fwd: A cameo by UNIX in BSTJ
In-Reply-To: <20130724024720.GH28626@bitmover.com>
References: <20130724013409.GB19629@neddie.local.net>
 <20130724024720.GH28626@bitmover.com>
Message-ID: <CADvA-dnqem77BvFN1LGrEy7bOw=tWDteZS-weCCnSfORvpLacw@mail.gmail.com>

What parts are missing from the archive mentioned by Poul-Henning Kamp?

  http://www3.alcatel-lucent.com/bstj/

It has 1922 to 1983.  I was assuming that missing issues like 1942 issues 2
through 4 were not ever published.

On Tue, Jul 23, 2013 at 10:47 PM, Larry McVoy <lm at bitmover.com> wrote:

> I'd like to know where I can get the entire BSTJ.  For the record,
> Bell Labs was the *the* place when I was growing up.  Reading their
> journals was right up there with having an account on slovax at
> uwisc and reading the bell labs/BSD source.
>
> On Wed, Jul 24, 2013 at 11:34:10AM +1000, Warren Toomey wrote:
> > All, I got this interesting e-mail from Poul-Henning a few days ago.
> >       Warren
> >
> > ----- Forwarded message from Poul-Henning Kamp <phk at phk.freebsd.dk>
> -----
> >
> >    Date: Sat, 20 Jul 2013 13:27:27 +0000
> >    From: Poul-Henning Kamp <phk at phk.freebsd.dk>
> >    To: wkt at tuhs.org
> >    Subject: A cameo by UNIX in BSTJ
> >
> > Some months ago I faced a flight from Denmark to NZ and back again,
> > so I bought a Kobo eBook reader and reformatted the entire BSTJ to fit
> > the screen.
> >
> > That gave me about 100k "pages" to read, plenty for my NZ-flights
> > and a large number of otherwise wasted moments since then.  Highly
> > recommeded.
> >
> > Recently I came over what I belive is the first mention of UNIX in BSTJ.
> >
> > We all know about the v57i6, July-August 1978 "UNIX Time-Sharing System"
> > issue, but it transpires that UNIX made a cameo two years earlier
> > in an article about compression-schemes for TeleFax:
> >
> > www3.alcatel-lucent.com/bstj/vol55-1976/articles/bstj55-10-1539.pdf
> >
> > Enjoy,
> >
> > Poul-Henning
> >
> > --
> > Poul-Henning Kamp       | UNIX since Zilog Zeus 3.20
> > phk at FreeBSD.ORG         | TCP/IP since RFC 956
> > FreeBSD committer       | BSD since 4.3-tahoe
> > Never attribute to malice what can adequately be explained by
> incompetence.
> >
> > ----- End forwarded message -----
> > _______________________________________________
> > TUHS mailing list
> > TUHS at minnie.tuhs.org
> > https://minnie.tuhs.org/mailman/listinfo/tuhs
>
> --
> ---
> Larry McVoy                lm at bitmover.com
> http://www.bitkeeper.com
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs
>



-- 
  Ed Cashin <ecashin at noserose.net>
  http://noserose.net/e/
  http://www.coraid.com/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20130724/96e45fea/attachment.html>

From doug at cs.dartmouth.edu  Fri Jul 26 08:10:16 2013
From: doug at cs.dartmouth.edu (Doug McIlroy)
Date: Thu, 25 Jul 2013 18:10:16 -0400
Subject: [TUHS] Fwd: A cameo by UNIX in BSTJ
Message-ID: <201307252210.r6PMAGPA011216@stowe.cs.dartmouth.edu>

Ken Thompson has famously said that the only thing he'd do
differently if he were to do Unix afresh would be to spell 
"create" with a final e. The BSTJ cameo (or product placement?)
reveals another example: he'd spell Unix as an ordinary proper
name. Once the marketers had glommed onto "UNIX" as a  trademark,
we were regularly badgered when when we tried to naturalize
the name. References to "Unix" in internal documents were 
scrubbed to "UNIX" for external consumption. I'd like to think
that by exhibiting "Unix" in an image Netravali et al
intentionally cocked a snook at corporate orthodoxy.

Incidentally, the online BSTJ is complete. A new publication,
the AT&T Technical Journal took its place after 1983, with
a new format and quite different content. The replacement 
publication was a house organ, not a research journal.

Doug


From david at kdbarto.org  Fri Jul 26 09:56:28 2013
From: david at kdbarto.org (David Barto)
Date: Thu, 25 Jul 2013 16:56:28 -0700
Subject: [TUHS] TUHS Digest, Vol 105, Issue 2
In-Reply-To: <mailman.1.1374717601.19800.tuhs@minnie.tuhs.org>
References: <mailman.1.1374717601.19800.tuhs@minnie.tuhs.org>
Message-ID: <4C10577F-5352-49E9-8A7E-72FFB6ABB226@kdbarto.org>

On Jul 24, 2013, at 7:00 PM, tuhs-request at minnie.tuhs.org wrote:

> What parts are missing from the archive mentioned by Poul-Henning Kamp?
> 
>  http://www3.alcatel-lucent.com/bstj/
> 
> It has 1922 to 1983.  I was assuming that missing issues like 1942 issues 2
> through 4 were not ever published.

I've got Vol 68, No. 8 October 1984, the second 'Unix System' edition of the BLTJ.

Additional articles on all things Unix:

Evolution of the UNIX Time-sharing system
Program Design in the UNIX System Environment
The Blit: A Multiplexed Graphics Terminal
Debugging C programs with the Blit
UNIX Operation System Security
File Security and the UNIX System Crypt Command
The Evolution of C - Past and Future
Data Abstration in C by B. Stroustrup - The first C++ mention that I know of in the Journal
Multiprocessor UNIX Systems
A UNIX System Implementation for System/370
UNIX Operating System Porting Experiences
The evolution of UNIX System Performance
Cheap Dynamic Instruction Counting
Theory and Practice in the Construction of a Working Sort Routine
The Fair Share Scheduler
The Virtual Protocol Machine
A Network of Computers Running the UNIX System
A Stream Input-Output System (D M Richie) - SYS V streams implementation description

	David




