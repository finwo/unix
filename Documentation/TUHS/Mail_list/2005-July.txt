From james at peacemax.org  Sun Jul 10 01:41:36 2005
From: james at peacemax.org (James Falknor)
Date: Sat, 09 Jul 2005 09:41:36 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition 
Message-ID: <42CFF030.4080609@peacemax.org>

To all you programmers,

    I'm sure you're scoffing at me, but I just obtained Borland's C++ 
BuilderX, Person Edition. It only cost $10.00 U.S. dollars direct from 
Borland.

    I plan to teach myself how to view, modify, replace, and 
write/re-write Unix Operating System code.

    May I rely on help from TUHS expert programmers? I know I'm going to 
need the help.

    My ultimate goal is to bring Unix Version7 into the 21st century. 
Since 4.3BSD contributed code into Unix Version8, I feel I will probably 
use portions of the latest BSD sources that are legally available.

    Again, if I may use those of you that programmers help, I would 
surely appreciate it alot.

Thank you,
James Falknor
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/x-pkcs7-signature
Size: 4602 bytes
Desc: S/MIME Cryptographic Signature
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050709/0bf63d43/attachment.bin>

From vasco at icpnet.pl  Mon Jul 11 06:21:54 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Sun, 10 Jul 2005 22:21:54 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42CFF030.4080609@peacemax.org>
References: <42CFF030.4080609@peacemax.org>
Message-ID: <42D18362.5050102@icpnet.pl>

Uz.ytkownik James Falknor napisa?:

> To all you programmers,
> My ultimate goal is to bring Unix Version7 into the 21st century. 

And which hardware do You plan to support ? Unix Version 7 was as I
understand designed for PDP-11 etc. Do You have running PDP machine ?

Andrzej



From wb at freebie.xs4all.nl  Mon Jul 11 06:25:15 2005
From: wb at freebie.xs4all.nl (Wilko Bulte)
Date: Sun, 10 Jul 2005 22:25:15 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18362.5050102@icpnet.pl>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
Message-ID: <20050710202515.GA4495@freebie.xs4all.nl>

On Sun, Jul 10, 2005 at 10:21:54PM +0200, Andrzej Popielewicz wrote..
> Uz.ytkownik James Falknor napisa?:
> 
> > To all you programmers,
> > My ultimate goal is to bring Unix Version7 into the 21st century. 
> 
> And which hardware do You plan to support ? Unix Version 7 was as I
> understand designed for PDP-11 etc. Do You have running PDP machine ?

You can also run on a PDP emulator.

-- 
Wilko


From james at peacemax.org  Mon Jul 11 07:06:05 2005
From: james at peacemax.org (James Falknor)
Date: Sun, 10 Jul 2005 15:06:05 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18362.5050102@icpnet.pl>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
Message-ID: <42D18DBD.1010906@peacemax.org>



Andrzej Popielewicz wrote:

>Uz.ytkownik James Falknor napisa?:
>
>  
>
>>To all you programmers,
>>My ultimate goal is to bring Unix Version7 into the 21st century. 
>>    
>>
>
>And which hardware do You plan to support ? Unix Version 7 was as I
>understand designed for PDP-11 etc. Do You have running PDP machine ?
>
>Andrzej
>  
>
Andrzej,

My first goal is to achieve modern x86 system support. After all,  
somebody was able to get Unix Version6 to run on a 286 system. 
Eventually, achieve modern 64bit system support.

James Falknor
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050710/3a61fe5a/attachment.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/x-pkcs7-signature
Size: 4602 bytes
Desc: S/MIME Cryptographic Signature
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050710/3a61fe5a/attachment.bin>

From james at peacemax.org  Mon Jul 11 07:09:45 2005
From: james at peacemax.org (James Falknor)
Date: Sun, 10 Jul 2005 15:09:45 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18362.5050102@icpnet.pl>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
Message-ID: <42D18E99.3040904@peacemax.org>

Andrzej,

My first goal is to achieve modern x86 system support. After all,  
somebody was able to get Unix Version6 to run on a 286 system. 
Eventually, achieve modern 64bit system support.

James Falknor

Andrzej Popielewicz wrote:

>Uz.ytkownik James Falknor napisa?:
>
>  
>
>>To all you programmers,
>>My ultimate goal is to bring Unix Version7 into the 21st century. 
>>    
>>
>
>And which hardware do You plan to support ? Unix Version 7 was as I
>understand designed for PDP-11 etc. Do You have running PDP machine ?
>
>Andrzej
>  
>

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050710/79a76542/attachment.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/x-pkcs7-signature
Size: 4602 bytes
Desc: S/MIME Cryptographic Signature
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050710/79a76542/attachment.bin>

From james at peacemax.org  Mon Jul 11 07:11:34 2005
From: james at peacemax.org (James Falknor)
Date: Sun, 10 Jul 2005 15:11:34 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18362.5050102@icpnet.pl>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
Message-ID: <42D18F06.3010104@peacemax.org>

Andrzej,

My first goal is to achieve modern x86 system support. After all,  
somebody was able to get Unix Version6 to run on a 286 system. 
Eventually, achieve modern 64bit system support.

James Falknor

Andrzej Popielewicz wrote:

>Uz.ytkownik James Falknor napisa?:
>
>  
>
>>To all you programmers,
>>My ultimate goal is to bring Unix Version7 into the 21st century. 
>>    
>>
>
>And which hardware do You plan to support ? Unix Version 7 was as I
>understand designed for PDP-11 etc. Do You have running PDP machine ?
>
>Andrzej
>  
>

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050710/c029cb17/attachment.html>

From imp at bsdimp.com  Mon Jul 11 09:28:10 2005
From: imp at bsdimp.com (M. Warner Losh)
Date: Sun, 10 Jul 2005 17:28:10 -0600 (MDT)
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18F06.3010104@peacemax.org>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
	<42D18F06.3010104@peacemax.org>
Message-ID: <20050710.172810.99473416.imp@bsdimp.com>

In message: <42D18F06.3010104 at peacemax.org>
            James Falknor <james at peacemax.org> writes:
: somebody was able to get Unix Version6 to run on a 286 system. 

Venix 86 ran on my old Rainbow.  That's a 8086.  It was v6 based,
iirc.

Warner


From chronaut at juno.com  Mon Jul 11 10:28:30 2005
From: chronaut at juno.com (chronaut at juno.com)
Date: Mon, 11 Jul 2005 00:28:30 GMT
Subject: [TUHS] Mark Williams Company Unix
Message-ID: <20050710.172904.16679.31732@webmail39.nyc.untd.com>


Hello everyone,

   I noticed that there were other flavors of Unix (on CD)for sale on the main website. I wondered who I needed to talk with, or send the $10.00 donation to, so I could get the Coherent stuff burned to CD ? Here's the directory I need.

http://www.tuhs.org/Archive/Other/Coherent/

My floppies with MWC Unix\X Xindows was stolen along with my automobile when I went to Jackson, MS to replace a retiring systems engineer. Anyway, help from anyone to get this done, would be very much appreciated. I have the original manuals, but no MWC Unix to go with it :) Thanks for any help !


Will

___________________________________________________________________
Try Juno Platinum for Free! Then, only $9.95/month!
Unlimited Internet Access with 250MB of Email Storage.
Visit http://www.juno.com/value to sign up today!



From james at peacemax.org  Mon Jul 11 13:43:09 2005
From: james at peacemax.org (James Falknor)
Date: Sun, 10 Jul 2005 21:43:09 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <20050710.172810.99473416.imp@bsdimp.com>
References: <42CFF030.4080609@peacemax.org>	<42D18362.5050102@icpnet.pl>	<42D18F06.3010104@peacemax.org>
	<20050710.172810.99473416.imp@bsdimp.com>
Message-ID: <42D1EACD.8000701@peacemax.org>

Warner,
    Where might I find the source code to Venix 86. I looked and could 
not find it here on THUS.

Thank you,
James Falknor

M. Warner Losh wrote:

>In message: <42D18F06.3010104 at peacemax.org>
>            James Falknor <james at peacemax.org> writes:
>: somebody was able to get Unix Version6 to run on a 286 system. 
>
>Venix 86 ran on my old Rainbow.  That's a 8086.  It was v6 based,
>iirc.
>
>Warner
>
>  
>




From imp at bsdimp.com  Mon Jul 11 14:11:41 2005
From: imp at bsdimp.com (M. Warner Losh)
Date: Sun, 10 Jul 2005 22:11:41 -0600 (MDT)
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D1EACD.8000701@peacemax.org>
References: <42D18F06.3010104@peacemax.org>
	<20050710.172810.99473416.imp@bsdimp.com>
	<42D1EACD.8000701@peacemax.org>
Message-ID: <20050710.221141.91191873.imp@bsdimp.com>

In message: <42D1EACD.8000701 at peacemax.org>
            James Falknor <james at peacemax.org> writes:
:     Where might I find the source code to Venix 86. I looked and could 
: not find it here on THUS.

I'd love to know myself.  All a google search has turned up is the DEC
PRO version.

Warner



From vasco at icpnet.pl  Mon Jul 11 18:38:47 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Mon, 11 Jul 2005 10:38:47 +0200
Subject: [TUHS] Mark Williams Company Unix
In-Reply-To: <20050710.172904.16679.31732@webmail39.nyc.untd.com>
References: <20050710.172904.16679.31732@webmail39.nyc.untd.com>
Message-ID: <42D23017.1080102@icpnet.pl>

Użytkownik chronaut at juno.com napisał:

>Hello everyone,
>
>   I noticed that there were other flavors of Unix (on CD)for sale on the main website. I wondered who I needed to talk with, or send the $10.00 donation to, so I could get the Coherent stuff burned to CD ? Here's the directory I need.
>
>http://www.tuhs.org/Archive/Other/Coherent/
>
>My floppies with MWC Unix\X Xindows was stolen along with my automobile when I went to 
>
You need 4 floppies anyway to install Coherent.



From chronaut at juno.com  Tue Jul 12 08:29:55 2005
From: chronaut at juno.com (chronaut at juno.com)
Date: Mon, 11 Jul 2005 22:29:55 GMT
Subject: [TUHS] Mark Williams Company Unix
Message-ID: <20050711.153048.11993.44557@webmail35.nyc.untd.com>


Yes I know, my originals were stolen when my car was stolen. I just don't have the time to download a mountain of software right now !


Will




U�ytkownik chronaut at juno.com napisa�:

Andrzej Popielewicz <vasco at icpnet.pl>

You need 4 floppies anyway to install Coherent.


___________________________________________________________________
Try Juno Platinum for Free! Then, only $9.95/month!
Unlimited Internet Access with 250MB of Email Storage.
Visit http://www.juno.com/value to sign up today!



From vasco at icpnet.pl  Wed Jul 20 08:13:17 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Wed, 20 Jul 2005 00:13:17 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42D18DBD.1010906@peacemax.org>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
	<42D18DBD.1010906@peacemax.org>
Message-ID: <42DD7AFD.5090307@icpnet.pl>

Uz.ytkownik James Falknor napisa?:

> My first goal is to achieve modern x86 system support. After all,
> somebody was able to get Unix Version6 to run on a 286 system.
> Eventually, achieve modern 64bit system support.

Well , it seems to be a giant effort You are planning to accomplish.
Because pdp and x86 have different architectures, a lot of new low level
stuff will have to be added.
The same concerns multitasking, multithreading , shared libraries etc.
You will take these from BSD probably.
So I am asking what are You planning to preserve in the modified system.
Will it be still be a Unix Version 7 ?

BTW I have ported apout emulator to Coherent. What I miss is "more" and
"vi". Editing with "cat" is possible but not very useful. I am not going
to learn "ed". Did You try to port "vi" to Unix Version 7 system ? I can
build everything for V7 under Coherent, so I thought about vi. One would
have to port curses . What about termcap and terminfo for V7 ?

Andrzej



From newsham at lava.net  Thu Jul 21 03:21:04 2005
From: newsham at lava.net (Tim Newsham)
Date: Wed, 20 Jul 2005 07:21:04 -1000 (HST)
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42DD7AFD.5090307@icpnet.pl>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
	<42D18DBD.1010906@peacemax.org> <42DD7AFD.5090307@icpnet.pl>
Message-ID: <Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>

> The same concerns multitasking, multithreading , shared libraries etc.

V7 already multitasks.
Why would you want shared libraries!?

> BTW I have ported apout emulator to Coherent. What I miss is "more" and

I have a small paging utility at
   http://lava.net/~newsham/x/machine/more_v6.c
   http://lava.net/~newsham/x/machine/more_v7.c

> "vi". Editing with "cat" is possible but not very useful. I am not going
> to learn "ed".

Why?

> Did You try to port "vi" to Unix Version 7 system ? I can

Why!?

> Andrzej

If one wanted an architecturally clean and modern V7-like system
that ran on the PC they could always install Plan9.  Of course if
you're porting V7 for the educational experience, more power to you.

Tim Newsham
http://www.lava.net/~newsham/


From vasco at icpnet.pl  Thu Jul 21 05:50:53 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Wed, 20 Jul 2005 21:50:53 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>
References: <42CFF030.4080609@peacemax.org> <42D18362.5050102@icpnet.pl>
	<42D18DBD.1010906@peacemax.org> <42DD7AFD.5090307@icpnet.pl>
	<Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>
Message-ID: <42DEAB1D.1020403@icpnet.pl>

Uz.ytkownik Tim Newsham napisa?:

>> The same concerns multitasking, multithreading , shared libraries etc.
>
>
> V7 already multitasks.

OK, so problem is solved.But I suspect it multitasks differently on
different hardware, unless You use existing V7 x86 implementation. I do
not know pdp architecture, I suspect it does differ from x86, I mean
TSS,GDT, TR etc.

> Why would you want shared libraries!?

Not me, but others, and possibly James himself, he wanted to make V7 top
modern.

> http://lava.net/~newsham/x/machine/more_v6.c
> http://lava.net/~newsham/x/machine/more_v7.c 

Fine, I will try it out.I let You know if it works on Coherent.
I have already began to port more, but to finish I need to port termcap
first.

>> "vi". Editing with "cat" is possible but not very useful. I am not going
>> to learn "ed".
>
> Why?

Simply because. Because I do not like ed.
I want to have useful and user friendly system. To use ed, only because
it is the oldest editor, does not make any sense for me.
I appreciate ed, because of sed, because sed has some similarity to ed
and is extremely useful as a tool.
Unix is not about ed, Unix is about unlimited possibilities of adding
new software , new applications or new editors, it makes Unix beautiful
that it can develop and not editor ed.If ed were all Unix has, it would
not survive.
I hope You accept that someone else can have different favourite
editors. I prefer vi, or even more vim, which is perfect editor.Of
course in the case of emulator missing user friendly editor is not a
problem, because I can edit under Coherent and then build under
emulator.It is good to have a choice, and Unix offers it.

>> Did You try to port "vi" to Unix Version 7 system ? I can
>
> Why!? 

As I said, because I like vi more than ed. Do You suggest , that James
should invest giant amount of time into porting V7 to x86/x64 and than
after use only one editor , namely ed ? As if it was prohibited to
develop another editor ?

> If one wanted an architecturally clean and modern V7-like system
> that ran on the PC they could always install Plan9. Of course if
> you're porting V7 for the educational experience, more power to you. 

I do not know Plan9, but according to descriptions I have read, it looks
very interesting. Well it was developed by Bell Lab...(?)-AT/T, which
does not require recommendation.They offer also another interesting OS,
namely Inferno.

BTW, I am not porting it(V7), It is James , who wants to do it .So more
power to him.
I have enough fun with Coherent, it is according to wikipedia based on
Unix version 7 and is x86.

Andrzej



From brantley at coraid.com  Thu Jul 21 07:17:57 2005
From: brantley at coraid.com (Brantley Coile)
Date: Wed, 20 Jul 2005 17:17:57 -0400
Subject: [TUHS] (no subject)
Message-ID: <1b49272b3bbc9700453cf18bc201181a@coraid.com>

>>> "vi". Editing with "cat" is possible but not very useful. I am not going
>>> to learn "ed".
>>
>> Why?
> 
> Simply because. Because I do not like ed.
> I want to have useful and user friendly system. To use ed, only because
> it is the oldest editor, does not make any sense for me.
> I appreciate ed, because of sed, because sed has some similarity to ed
> and is extremely useful as a tool.
> Unix is not about ed, Unix is about unlimited possibilities of adding
> new software , new applications or new editors, it makes Unix beautiful
> that it can develop and not editor ed.If ed were all Unix has, it would
> not survive.
> I hope You accept that someone else can have different favourite
> editors. I prefer vi, or even more vim, which is perfect editor.Of
> course in the case of emulator missing user friendly editor is not a
> problem, because I can edit under Coherent and then build under
> emulator.It is good to have a choice, and Unix offers it.

In 1983 I was using vi.  I allowed a friend to use our system to typeset
his companies UNIX manuals, and quickly found that I was having to
share the machine with a dozen troff jobs.  Vi, being a program that
ran in raw mode, didn't respond very well on that 68010 10Mhz system.
I was forced to switch to ed.  Suddenly I discovered that I had hidden
real UNIX behind all those vi commands.  I now had plenty of
mental capacity to use the rest of the tools available.

To really say you understand the spirit of the software tools approach,
you must spend a couple of months just using ed.  Today I use acme
mostly, but still find myself using ed for some edits.

I would really encourage you to give it a try.  Spend two months
just using ed.  You cerntainly should use the editor you feel most
confortable with, but the growing experience will be well worth your while.

  Brantley


From brantley at coraid.com  Thu Jul 21 07:18:24 2005
From: brantley at coraid.com (Brantley Coile)
Date: Wed, 20 Jul 2005 17:18:24 -0400
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
Message-ID: <bfb8fe6f29ee2865079c5a2704399ebe@coraid.com>

> I do not know Plan9, but according to descriptions I have read, it looks
> very interesting. Well it was developed by Bell Lab...(?)-AT/T, which
> does not require recommendation.They offer also another interesting OS,
> namely Inferno.

This is my last email on this subject.  Promise.

I suggest using the Plan 9 compilers and start with the code for the
32V system.  That's the code that first ran on the VAX.  It'll be
easier to move than the PDP version.  It's just Seventh Edition moved
to the VAX.

I'm using Plan 9 to type this.  It's the os I have used as my primary
os for the last 10 years.  I wrote the Cisco PIX Firewall and the
LocalDirector using it.  I first used Plan 9 15 years ago at Bell Labs.
In a very real sense, it is the true decendent of a very noble line of
timesharing systems, going all the way back to MIT's CTSS.

You should try Plan 9 for free by downloading it  from Bell Labs.  It's
all open source.  Expect to learn a lot.  It's UNIX like a Ford
Mustang is a T-Model.  Lot of the ideas of V7-10 are further developed
in Plan 9.  It's certainly the os perfered by a good number of UNIX
purests.  It was the result of a number of poeple, including Ken
Thompson, who thought that a fresh code start would allow them to
better exploit new technology like networking, hetergenious
processors, and symmetrical multiple processors.
http://plan9.bell-labs.com

I really hope James does the port.  I wish I had the time to do it
myself.  A native V7 port would be really useful in some situations,
but more importantly it would help educate new generations of
programmers.  It would demonstrate the true power and synergy of the
software tools approach that UNIX blessed us with.  It doesn't need
shared libraries, threads, gui's or even vi.  The Seventh Edition is
amazing technology in a form that can be understood, internalized, and
the resulting education used to produce much better modern software.
There should be at least a version in it's native form.  There's just
something special about running it native.

  Brantley Coile



From brantley at coraid.com  Thu Jul 21 07:17:18 2005
From: brantley at coraid.com (Brantley Coile)
Date: Wed, 20 Jul 2005 17:17:18 -0400
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
Message-ID: <d9f3b1c76ec1bd7d3a55404d4c7d88a9@coraid.com>

> OK, so problem is solved.But I suspect it multitasks differently on
> different hardware, unless You use existing V7 x86 implementation. I do
> not know pdp architecture, I suspect it does differ  from x86, I mean
> TSS,GDT, TR etc.

These details are hidden under the kernel.  The idea of a process with
address space and other contexts are what V7 provides.  It's pretty
easy to implement what V7 expects using Intel's paging.

  Brantley


From vasco at icpnet.pl  Thu Jul 21 09:05:18 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 01:05:18 +0200
Subject: [Fwd: Re: [TUHS] Borland's C++ BuilderX, Personal Edition]
Message-ID: <42DED8AE.5080201@icpnet.pl>


-------------- next part --------------
An embedded message was scrubbed...
From: Andrzej Popielewicz <vasco at icpnet.pl>
Subject: Re: [TUHS] Borland's C++ BuilderX, Personal Edition
Date: Thu, 21 Jul 2005 01:04:13 +0200
Size: 1827
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050721/1a4fc7c0/attachment.mht>

From vasco at icpnet.pl  Thu Jul 21 09:32:49 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 01:32:49 +0200
Subject: [Fwd: Re: [TUHS] Borland's C++ BuilderX, Personal Edition]
Message-ID: <42DEDF21.9090208@icpnet.pl>


-------------- next part --------------
An embedded message was scrubbed...
From: Andrzej Popielewicz <vasco at icpnet.pl>
Subject: Re: [TUHS] Borland's C++ BuilderX, Personal Edition
Date: Thu, 21 Jul 2005 01:19:04 +0200
Size: 2004
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050721/aa3d741f/attachment.mht>

From vasco at icpnet.pl  Thu Jul 21 09:33:06 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 01:33:06 +0200
Subject: [Fwd: Re: [TUHS] Borland's C++ BuilderX, Personal Edition]
Message-ID: <42DEDF32.5070207@icpnet.pl>


-------------- next part --------------
An embedded message was scrubbed...
From: Andrzej Popielewicz <vasco at icpnet.pl>
Subject: Re: [TUHS] Borland's C++ BuilderX, Personal Edition
Date: Thu, 21 Jul 2005 01:32:14 +0200
Size: 1962
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050721/5d31d771/attachment.mht>

From james at peacemax.org  Thu Jul 21 10:35:45 2005
From: james at peacemax.org (James Falknor)
Date: Wed, 20 Jul 2005 18:35:45 -0600
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
Message-ID: <42DEEDE1.6070609@peacemax.org>

Thanks to all,
   
    This project is, indeed, for my own educational purposes. I have 
always been "all idea, no action" type of person. It's time for me to 
act on my ideas.

    I have started by comparing the differences between Unix V7, 32V, 
Coherent, and NetBSD code base.

    I wish to preserve as much of Unix V7 as possible. Unix V7 was not 
without code contributed by others, namely Universities around the 
world. I don't know the legal logistics, but I don't see any reason to 
change the name from Unix V7 to anything else.

    As I begin to make run time progress on this project, I will keep 
everybody notified. I will also be posting questions as they arise.

Thank you,
James Falknor

(I know I'm not alone with a World of Programmers on this mailing list)






From wes.parish at paradise.net.nz  Thu Jul 21 17:41:54 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Thu, 21 Jul 2005 19:41:54 +1200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42DEAB1D.1020403@icpnet.pl>
References: <42CFF030.4080609@peacemax.org>
	<Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>
	<42DEAB1D.1020403@icpnet.pl>
Message-ID: <200507211941.54334.wes.parish@paradise.net.nz>

On Thu, 21 Jul 2005 07:50, Andrzej Popielewicz wrote:
> Uz.ytkownik Tim Newsham napisa?:
<snip>
> >> "vi". Editing with "cat" is possible but not very useful. I am not going
> >> to learn "ed".
> >
> > Why?
>
> Simply because. Because I do not like ed.
> I want to have useful and user friendly system. To use ed, only because
> it is the oldest editor, does not make any sense for me.
> I appreciate ed, because of sed, because sed has some similarity to ed
> and is extremely useful as a tool.

If you have a  look at the more modern *BSD, you'll see that vi contains ed 
and ex.  It should be possible to backport that to V7 and likewise to 
Coherent.
> Unix is not about ed, Unix is about unlimited possibilities of adding
> new software , new applications or new editors, it makes Unix beautiful
> that it can develop and not editor ed.If ed were all Unix has, it would
> not survive.
> I hope You accept that someone else can have different favourite
> editors. I prefer vi, or even more vim, which is perfect editor.Of
> course in the case of emulator missing user friendly editor is not a
> problem, because I can edit under Coherent and then build under
> emulator.It is good to have a choice, and Unix offers it.
>
> >> Did You try to port "vi" to Unix Version 7 system ? I can
> >
> > Why!?
>
> As I said, because I like vi more than ed. Do You suggest , that James
> should invest giant amount of time into porting V7 to x86/x64 and than
> after use only one editor , namely ed ? As if it was prohibited to
> develop another editor ?
>
<snip>
>
> Andrzej
>
<snip>

Wesley Parish
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.


From vasco at icpnet.pl  Thu Jul 21 18:35:05 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 10:35:05 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <200507211941.54334.wes.parish@paradise.net.nz>
References: <42CFF030.4080609@peacemax.org>	<Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>	<42DEAB1D.1020403@icpnet.pl>
	<200507211941.54334.wes.parish@paradise.net.nz>
Message-ID: <42DF5E39.3080101@icpnet.pl>

Uz.ytkownik Wesley Parish napisa?:

>If you have a  look at the more modern *BSD, you'll see that vi contains ed 
>and ex.  It should be possible to backport that to V7 and likewise to 
>Coherent.
>  
>
What for ?
Vim 6.1(my port) under Coherent works perfectly. And of course Coherent
has its own very good vi and also ed and microEmacs and as far as I
rememeber (I have seen on Tuhs) Emacs too.
I have ported also mp( ala DOS edit) , asedit,xed(better version of
xedit) .Unfortunately I was not lucky with gvim and nedit.I have added
also about 1990 my own editor cedit(with hex support ala vim), but it
was for Coherent 4.0 and I did not port it to Coherent 4.2.10 yet.

OK, but if You see it does make sense than do it.




From vasco at icpnet.pl  Thu Jul 21 19:27:15 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 11:27:15 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <200507211941.54334.wes.parish@paradise.net.nz>
References: <42CFF030.4080609@peacemax.org>	<Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>	<42DEAB1D.1020403@icpnet.pl>
	<200507211941.54334.wes.parish@paradise.net.nz>
Message-ID: <42DF6A73.6000400@icpnet.pl>

Uz.ytkownik Wesley Parish napisa?:

>If you have a  look at the more modern *BSD, you'll see that vi contains ed 
>and ex.  It should be possible to backport that to V7 and likewise to 
>Coherent.
>  
>
According to Coherent Manual :
*****
Coherent vi is a link to Berkeley screen editor elvis by Kirkendall from
1990.
elvis is a clone of Unix vi and ex editors.
elvis command structure ressembles command structure of line editor ed.
*****





From vasco at icpnet.pl  Thu Jul 21 19:28:37 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 11:28:37 +0200
Subject: [Fwd: Re: [TUHS] Borland's C++ BuilderX, Personal Edition]
Message-ID: <42DF6AC5.2010103@icpnet.pl>


-------------- next part --------------
An embedded message was scrubbed...
From: Andrzej Popielewicz <vasco at icpnet.pl>
Subject: Re: [TUHS] Borland's C++ BuilderX, Personal Edition
Date: Thu, 21 Jul 2005 11:27:15 +0200
Size: 1177
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050721/e958bc7a/attachment.mht>

From wes.parish at paradise.net.nz  Thu Jul 21 21:44:12 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Thu, 21 Jul 2005 23:44:12 +1200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42DF6A73.6000400@icpnet.pl>
References: <42CFF030.4080609@peacemax.org>
	<200507211941.54334.wes.parish@paradise.net.nz>
	<42DF6A73.6000400@icpnet.pl>
Message-ID: <200507212344.12405.wes.parish@paradise.net.nz>

On Thu, 21 Jul 2005 21:27, Andrzej Popielewicz wrote:
> Uz.ytkownik Wesley Parish napisa?:
> >If you have a  look at the more modern *BSD, you'll see that vi contains
> > ed and ex.  It should be possible to backport that to V7 and likewise to
> > Coherent.
>
> According to Coherent Manual :
> *****
> Coherent vi is a link to Berkeley screen editor elvis by Kirkendall from
> 1990.
> elvis is a clone of Unix vi and ex editors.
> elvis command structure ressembles command structure of line editor ed.
> *****

Makes sense.  My bad.

Wesley Parish

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.


From gunnarr at acm.org  Fri Jul 22 03:15:48 2005
From: gunnarr at acm.org (Gunnar Ritter)
Date: Thu, 21 Jul 2005 19:15:48 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <200507211941.54334.wes.parish@paradise.net.nz>
References: <42CFF030.4080609@peacemax.org>
	<Pine.BSI.4.61.0507200717410.21772@malasada.lava.net>
	<42DEAB1D.1020403@icpnet.pl>
	<200507211941.54334.wes.parish@paradise.net.nz>
Message-ID: <42dfd844.gGVrLFVHAobYRgcn%gunnarr@acm.org>

Wesley Parish <wes.parish at paradise.net.nz> wrote:

> If you have a  look at the more modern *BSD, you'll see that vi contains ed 
> and ex.

Which BSD implementation of vi contains ed?

> It should be possible to backport that to V7 and likewise to 
> Coherent.

For Unix 7th edition, you can just use vi version 2 from one of
the early 2BSD releases.

	Gunnar


From vasco at icpnet.pl  Fri Jul 22 05:45:24 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Thu, 21 Jul 2005 21:45:24 +0200
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <20050721164429.60aa8811.jrvalverde@cnb.uam.es>
References: <42CFF030.4080609@peacemax.org>	<42D18362.5050102@icpnet.pl>	<42D18DBD.1010906@peacemax.org>	<42DD7AFD.5090307@icpnet.pl>
	<20050721164429.60aa8811.jrvalverde@cnb.uam.es>
Message-ID: <42DFFB54.80205@icpnet.pl>

Użytkownik José R. Valverde napisał:

>Looks like overkill to me. It made lots of sense way back then, but as you
>are speaking of an X86 port and you can assume an ANSI terminal to be the
>default and available, you may as well (at least as a start) do without
>termcap and terminfo (BTW I'd bet you don't need support for almost none
>of those ancient terminals).
>
I do not need curses or termcap or terminfo to work under simulated pdp
environment  via apout in Coherent. As I said I can build
everything(using V7 make,cc ans as), it means I can work, and I do not
need ANSI terminal (if You have meant true VT100 terminal, and not
TERM=vt100 on PC, BTW apout for version 7 assumes as default TERM=vt100
). I was meaning building in pdp environment running in Coherent, and
not building in Coherent via for example a crosscompiler or so.

 One needs  termcap or terminfo if one wants to port more sophisticated
 tools like vi etc.

>
>What you can get is then a simpler screen-oriented text editor which can
>easily be ported and then used as a bootstrap to port more advanced tools.
>
>Namely, S from 'A Software Tools Sampler' by Webb Miller. I ported and used 
>it on both V6 and V7, and still use it on V7 on SIMH. Neat, small, easy to 
>port, usage alike vi, but much simpler... And comes with some other 
>interesting tools (actually my first involvement with that code was to have a 
>unix-like toolkit on eraly VMS long, long ago).
>
>The code is available on the Net, but I'm including it here as an attachment
>as it is not that big (52K).
>
>				j
>  
>
Great, I will try it out. I have already tested succesfully more_v7 ,
which I obtained from Tim.
Thanks. I will let You know how it works.
 

Andrzej



From PeterJeremy at optushome.com.au  Fri Jul 22 17:37:04 2005
From: PeterJeremy at optushome.com.au (Peter Jeremy)
Date: Fri, 22 Jul 2005 17:37:04 +1000
Subject: [TUHS] Borland's C++ BuilderX, Personal Edition
In-Reply-To: <42CFF030.4080609@peacemax.org>
References: <42CFF030.4080609@peacemax.org>
Message-ID: <20050722073703.GA5352@cirb503493.alcatel.com.au>

On Sat, 2005-Jul-09 09:41:36 -0600, James Falknor wrote:
>   My ultimate goal is to bring Unix Version7 into the 21st century. 

That's already been done: Look at 2.11BSD.

Note that getting V7 (or 2BSD) running on a 286 is non-trivial.
Whilst both the PDP-11 and 286 are both 16-bit machines supporting VM,
the 286 MMU is extremely primitive compared to the PDP-11.  Your
biggest problem will be that the 286 does not support holes in the
address space - which means your data segment needs to cover both the
real data as well as the stack area and you lose brk limit checking.

-- 
Peter Jeremy


From james at peacemax.org  Thu Jul 28 10:20:08 2005
From: james at peacemax.org (James Falknor)
Date: Wed, 27 Jul 2005 18:20:08 -0600
Subject: [TUHS] IBM Xenix 1.00
Message-ID: <42E824B8.9060605@peacemax.org>

To all concerned,
    I recently came into possesion of IBM's Xenix 1.00 on 5.25 floppy 
disks for free. Problem is that it is missing disk 3 of 3 or the 4th 
disk in the set. I have the Installation disk, Disk 1 of 3, and Disk 2 of 3.
    Does anybody, by any chance, have the 4th disk, Disk 3 of 3 that 
they would be willing to share? Or, is that not allowed to be asked here?

Thank you,
James




