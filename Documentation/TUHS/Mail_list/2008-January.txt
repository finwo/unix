From jrvalverde at cnb.uam.es  Thu Jan 10 21:37:05 2008
From: jrvalverde at cnb.uam.es (Jose R. Valverde)
Date: Thu, 10 Jan 2008 12:37:05 +0100
Subject: [TUHS] tropix
Message-ID: <20080110123705.12791898@veda.cnb.uam.es>

Looking at the UNIX tree chart as I do from time to time to re-search for old
UNIX, I came this time over TROPIX, which is a UNIX-like system developed from
scratch on Brazil in their "IT cold war" times.

The system is now open source and can be found at

	http://allegro.nce.ufrj.br/tropix/index.html

the pages are in Portuguese (sic) but one may get install media and the full
source code (Código Fonte).

I wonder it if would be sensible to store a copy of it on TUHS as well. In any
case, I've added it to the collection at 

	ftp://ftp.es.embnet.org/pub/misc/os/UNIX/tropix

I've run it on QEMU and it's got a somewhat distinctive look and feel (and
command set). Curious to say the least. The major drawback is that it is all
in Portuguese.

					j
-- 
	These opinions are mine and only mine. Hey man, I saw them first!

			    José R. Valverde

	De nada sirve la Inteligencia Artificial cuando falta la Natural
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20080110/1e34ba71/attachment.sig>

From grog at FreeBSD.org  Sun Jan 13 13:41:17 2008
From: grog at FreeBSD.org (Greg 'groggy' Lehey)
Date: Sun, 13 Jan 2008 14:41:17 +1100
Subject: [TUHS] TUHS Digest, Vol 44, Issue 5
In-Reply-To: <20071227101441.GA28854@freebie.xs4all.nl>
References: <mailman.3.1198720801.60061.tuhs@minnie.tuhs.org>
	<20071227020800.GH6651@bitmover.com>
	<20071227031755.GA9570@anteil.com>
	<20071227033921.GA7053@bitmover.com>
	<20071227101441.GA28854@freebie.xs4all.nl>
Message-ID: <20080113034117.GN60332@dereel.lemis.com>

On Thursday, 27 December 2007 at 11:14:42 +0100, Wilko Bulte wrote:
> Quoting Larry McVoy, who wrote on Wed, Dec 26, 2007 at 07:39:22PM -0800 ..
>> It's not me, it's all of you.  OSL has a gigabit link to the net if I
>> recall correctly.  But I don't think TUHS needs that.  There are only
>> so many of us old farts.  But fast is nice.
>>
>> I do think that there is a _huge_ amount of value in the TUHS archive.
>> I think that anyone I hire who has not wandered through there, should.
>>
>> Anyhoo, I'm pinging the OSL guys, I suspect that they will be cool about
>> hosting a mirror, they have been way cool in the past.  For that matter,
>> they'd host www.tuhs.org but I suspect Warren may not be cool with that.
>> Dunno.  I can arrange introductions, they are a cool group, this is a
>> cool group, who knows?
>
> Hm..  if all else fails we could check if ftp.freebsd.org would be available
> to mirror.  Grog, Warner, what do you think?

(sorry for the slow response) I seem to have lost some messages, so
I'm not longer sure what Larry's suggesting to mirror.  Also it's not
as if I have much to say for the FreeBSD project any more, though I'm
surprised imp didn't say anything.  Isn't this a matter for the
FreeBSD core team and whoever is running ftp.freebsd.org at the
moment?  I certainly don't see any obvious reason why not.

Greg
--
See complete headers for address and phone numbers.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 187 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20080113/7d681839/attachment.sig>

From imp at bsdimp.com  Sun Jan 13 16:25:12 2008
From: imp at bsdimp.com (M. Warner Losh)
Date: Sat, 12 Jan 2008 23:25:12 -0700 (MST)
Subject: [TUHS] TUHS Digest, Vol 44, Issue 5
In-Reply-To: <20080113034117.GN60332@dereel.lemis.com>
References: <20071227033921.GA7053@bitmover.com>
	<20071227101441.GA28854@freebie.xs4all.nl>
	<20080113034117.GN60332@dereel.lemis.com>
Message-ID: <20080112.232512.-399282796.imp@bsdimp.com>

In message: <20080113034117.GN60332 at dereel.lemis.com>
            "Greg 'groggy' Lehey" <grog at FreeBSD.org> writes:
: On Thursday, 27 December 2007 at 11:14:42 +0100, Wilko Bulte wrote:
: > Quoting Larry McVoy, who wrote on Wed, Dec 26, 2007 at 07:39:22PM -0800 ..
: >> It's not me, it's all of you.  OSL has a gigabit link to the net if I
: >> recall correctly.  But I don't think TUHS needs that.  There are only
: >> so many of us old farts.  But fast is nice.
: >>
: >> I do think that there is a _huge_ amount of value in the TUHS archive.
: >> I think that anyone I hire who has not wandered through there, should.
: >>
: >> Anyhoo, I'm pinging the OSL guys, I suspect that they will be cool about
: >> hosting a mirror, they have been way cool in the past.  For that matter,
: >> they'd host www.tuhs.org but I suspect Warren may not be cool with that.
: >> Dunno.  I can arrange introductions, they are a cool group, this is a
: >> cool group, who knows?
: >
: > Hm..  if all else fails we could check if ftp.freebsd.org would be available
: > to mirror.  Grog, Warner, what do you think?
: 
: (sorry for the slow response) I seem to have lost some messages, so
: I'm not longer sure what Larry's suggesting to mirror.  Also it's not
: as if I have much to say for the FreeBSD project any more, though I'm
: surprised imp didn't say anything.  Isn't this a matter for the
: FreeBSD core team and whoever is running ftp.freebsd.org at the
: moment?  I certainly don't see any obvious reason why not.

Before I could say something, it sounded like Larry had the mirrors he
needed.

Warner


From wes.parish at paradise.net.nz  Thu Jan 31 18:35:51 2008
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Thu, 31 Jan 2008 21:35:51 +1300
Subject: [TUHS] Banyan Vines?  Banyan/ePresence dissolves self
Message-ID: <200801312135.52821.wes.parish@paradise.net.nz>

I was reading Graklaw for more-of-the-same - boneheaded companies taking on 
productive people with intent to reduce dangerous productivity in favour of 
monopolizing transaction tokens ie, money - and I came across the article on 
some_bright_spark suing some other company for daring to try protecting 
networks from email-borne spam:
http://www.groklaw.net/article.php?story=20080125135544713

Which got me thinking - Banyan Vines was a player back then, and the comments 
mentioned only Novell.  Surely there's something about Banyan's Vines?

I did a google search and found this:

http://www.bizjournals.com/masshightech/stories/2007/12/24/daily7.html?ana=from_rss
Wednesday, December 26, 2007
Liquidating ePresence distributes cash
Mass High Tech: The Journal of New England Technology
<snip>
 
 Framingham's ePresence Inc. reports a plan to distribute cash to its 
shareholders as part of its dissolution plan. 
 The distribution of $3.6 million, or 14 cents per common share, is expected 
to be paid this week to those who were ePresence shareholders as of June 23, 
2004. The distribution, combined with the previous distributions totaling 
$4.15 per share, would return a total $4.29 per share to ePresence 
shareholders, company officials said. 
 EPresence was launched in 1983 as Banyan Systems, selling a network operating 
system and directory. But competitors such as Novell Inc. and Microsoft Corp. 
subsequently moved into that market and Banyan switched focus in 1997. 
 In 2003, ePresence sold it services business to Unisys Corp. for $11.5 
million. In 2004, the company sold its online telephone directory division 
Switchboard Inc. to Bellevue, Wash.-based InfoSpace Inc. for $160 million.
--------------------------------------------------------------------------------------------------------------

Who would one need to get in touch with, to ask about the possibility of 
getting the various obsolete Banyan Vines bits and pieces donated to TUHS?  
(It was based on a Unix kernel, so I would say it - one of the first NOSes to 
have a directory - should be part of the TUHS repository.)

Thanks

Wesley Parish

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Gaul is quartered into three halves.  Things which are 
impossible are equal to each other.  Guerrilla 
warfare means up to their monkey tricks. 
Extracts from "Schoolboy Howlers" - the collective wisdom 
of the foolish.
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.


From andreww at datanet.ab.ca  Thu Jan 31 18:56:26 2008
From: andreww at datanet.ab.ca (Andrew Warkentin)
Date: Thu, 31 Jan 2008 01:56:26 -0700
Subject: [TUHS] Banyan Vines?  Banyan/ePresence dissolves self
In-Reply-To: <200801312135.52821.wes.parish@paradise.net.nz>
References: <200801312135.52821.wes.parish@paradise.net.nz>
Message-ID: <47A18D3A.5090401@datanet.ab.ca>

Wesley Parish wrote:

>Who would one need to get in touch with, to ask about the possibility of 
>getting the various obsolete Banyan Vines bits and pieces donated to TUHS?  
>(It was based on a Unix kernel, so I would say it - one of the first NOSes to 
>have a directory - should be part of the TUHS repository.)
>  
>
Wasn't it based on System V? Wouldn't that prevent it from being 
released? (unless they made a similar deal with AT&T to the one Sun 
made, which is very unlikely)


