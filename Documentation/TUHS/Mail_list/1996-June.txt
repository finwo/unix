From wkt at dolphin.cs.adfa.oz.au  Sat Jun 22 16:44:19 1996
From: wkt at dolphin.cs.adfa.oz.au (Warren Toomey)
Date: Sat, 22 Jun 1996 16:44:19 +1000 (EST)
Subject: v6 tapes
In-Reply-To: <Pine.SOL.3.91.960621142754.6541A-100000@sparcy.geneva.edu> from "Jacob Ritorto" at Jun 21, 96 02:40:53 pm
Message-ID: <9606220644.AA06253@dolphin>

In atricle by Jacob Ritorto:
> Warren,
> 	I have three 600' 9-track 800bpi tapes marked (in pen) UNIX V6 
> 4000 blocks.  One Source, one Object, one Documentation.  I've tried 
> using ROLLIN to restore them, but it expects a filename, which I can't 
> find.  I did do an RT11 dump of the first few blocks of the tapes, which 
> revealed an ascii 'd' as the first byte, then a series of decending 
> bytes.  It didn't look like a file structure or a filename.  All the 
> tapes had the same first block.  I tried to boot the tapes directly on an 
> 11/34.  No luck.  I tried to DIR them from rt11, but, of course, rt 
> couldn't find any directory info.  There's definately unix stuff on the 
> tapes--I saw it in the RT11 device dump.  Dates in some of the source 
> files are around 1974 or 1976, if I recall correctly.  
> 	What do I need to do to get these tapes back onto disk and 
> running?  I'm assuming they restore to RK05 disks because the labels say 
> 4000 blocks.  I have 4 rk05 drives and lots of packs.  BTW, the tape 
> drive I'm using is a TU10 with standard address and vector.
> 	Your help would be greatly appreciated.
> Jacob Ritorto

Jacob, I'm passing this onto a bunch of PDP Unix people, as I don't have
the hardware & RT11 experience to tell you how to install v6 from the
tapes. Yes, the 3 tapes are RK05 pack images, I have on-line copies here
if that can be of any help to you. Can you raw dump the tapes to RK05s
using RT11?

Can anybody help Jacob out here? We also have v7, 2.9BSD and 2.11BSD here.
What hardware do you have?

Best of luck,

	Warren Toomey


From wkt at csadfa.cs.adfa.oz.au  Thu Jun 27 12:06:47 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Thu, 27 Jun 1996 12:06:47 +1000 (EST)
Subject: Good News re PDP Unix Licensing
Message-ID: <9606270206.AA02788@dolphin>

All,
	I just received a very pleasing letter from Dion L. Johnson II, the
Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
his email and my response below. If I can get a legally authorative statement
on paper from SCO, I'll pass it on to you all, especially Steven Schultz.

Cheers,
	Warren

In atricle by Dion:
> 
> SCO owns the licensing rights all versions of the UNIX system, or
> so our legal folks tell me.  Now, of course there are many
> derivative, licensed versions, and some of the holders of those
> licenses have rights to sublicense.  In the case of BSD
> enhancements, the Berkeley additions are owned by the Regents of
> the University of California, and I believe the UCB license terms
> are well known.
> 
> As for your friends who have rescued ancient PDP machines...  I
> am confident that SCO would cheerfully encourage them to run UNIX
> on these antiques without any payment to us.  I cant quite
> officially give that permission myself, but I can speculate that
> SCO certainly would not mind.
> 
> So go for it.  Does this help?
> -Dion
>Dion L. Johnson II  - The Santa Cruz Operation, Inc.              dionj at sco.com
>SCO Product Manager - Development Systems and Various Other Stuff
>400 Encinal St.  Santa Cruz, CA 95061    FAX: 408-427-5417  Voice: 408-427-7565

Dion, thanks very much for your email, in fact I'm ecstatic! I know this
could be a tricky legal minefield, so if possible could SCO draft a letter
(and run it past their lawyers) which sets out exactly what you said above.

In particular, you said that ``SCO would cheerfully encourage them to run UNIX
on these antiques without any payment to us''. Does this mean I can legally
distribute the source code to the PDP versions of UNIX, and to anybody? or
just to people who own PDP-11s. There are PDP-11 emulators available, so
it is conceivable that people who don't even have a real PDP-11 might like
to try UNIX out on these emulators. If to anybody, then I assume this means
the source is legally owned by SCO but freely distributable?

I really appreciate your offer of making these old versions of UNIX
available, but given the legal status of the code to this point, I would
like to cover myself with an officially blessed and signed document from SCO.
Let me know what you can do, and many many thanks again for this!

Cheers,
	Warren


From Johnny.Billquist at emw.ericsson.se  Thu Jun 27 19:02:02 1996
From: Johnny.Billquist at emw.ericsson.se (Johnny Billquist konsult)
Date: Thu, 27 Jun 96 11:02:02 +0200
Subject: Good News re PDP Unix Licensing
Message-ID: <9606270902.AA03902@genesis.mo.emw.ericsson.se>

> All,
> 	I just received a very pleasing letter from Dion L. Johnson II, the
> Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
> his email and my response below. If I can get a legally authorative statement
> on paper from SCO, I'll pass it on to you all, especially Steven Schultz.

Not that I wan't to sound pessimistic, but there are several
miles between "would not mind", and "legally allowed".

>From what I read into his letter, he's saying that he don't think
SCO would take legal actions against us, but at the same time they
won't probably make it officially legal.

And your reply, hoping that they'll say that "Unix is legally owned by
SCO, but freely distributable", is really reaching for the sky... :-)

Anyway, keep trying, it would be very nice if they really did write
such a paper.

	Johnny


From wkt at csadfa.cs.adfa.oz.au  Fri Jun 28 09:07:07 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Fri, 28 Jun 1996 09:07:07 +1000 (EST)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <9606270902.AA03902@genesis.mo.emw.ericsson.se> from "Johnny Billquist konsult" at Jun 27, 96 11:02:02 am
Message-ID: <9606272307.AA06842@dolphin>

In atricle by Johnny Billquist konsult:
> 
> > 	I just received a very pleasing letter from Dion L. Johnson II, the
> > Product Manager at SCO, about the legal status of the PDP UNIXs.
> 
> Not that I wan't to sound pessimistic, but there are several
> miles between "would not mind", and "legally allowed".
> From what I read into his letter, he's saying that he don't think
> SCO would take legal actions against us, but at the same time they
> won't probably make it officially legal.
> And your reply, hoping that they'll say that "Unix is legally owned by
> SCO, but freely distributable", is really reaching for the sky... :-)

I know, but I really don't want to try to work under a `head in the sand'
approach from SCO. At the very least I'd like official blessing to pass
on old UNIXes to people who need/want them. I know getting SCO to allow
them to be freely redistributable is a pipe dream, but perhaps there's
some middle ground they would be willing to move to.

I'll keep you all informed of any progress with SCO.

Cheers,
	Warren


From sms at wlv.iipo.gtegsc.com  Fri Jun 28 02:05:17 1996
From: sms at wlv.iipo.gtegsc.com (Steven M. Schultz)
Date: Thu, 27 Jun 1996 09:05:17 -0700 (PDT)
Subject: Good News re PDP Unix Licensing
Message-ID: <199606271605.JAA10306@wlv.iipo.gtegsc.com>

Warren, et al -

> 	I just received a very pleasing letter from Dion L. Johnson II, the
> Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
> his email and my response below. If I can get a legally authorative statement
> on paper from SCO, I'll pass it on to you all, especially Steven Schultz.
> 	Warren

	Wow.  I never even dreamed that such a thing would happen - how times
	(or people) have changed over time.  Very welcome news indeed!

> In particular, you said that ``SCO would cheerfully encourage them to run UNIX
> on these antiques without any payment to us''. Does this mean I can legally
> distribute the source code to the PDP versions of UNIX, and to anybody? or
> just to people who own PDP-11s. There are PDP-11 emulators available, so
> it is conceivable that people who don't even have a real PDP-11 might like
> to try UNIX out on these emulators. If to anybody, then I assume this means
> the source is legally owned by SCO but freely distributable?

	I submit that running an emulator does not demonstrate the proper
	degree of obsession or investment (financial as well as emotional) with 
	``antiques''.  No investment in actual old hardware is involved when 
	running an emulator.  That is, I believe, what SCO is referring
	to when mention is made of ``run UNIX on these antiques''.  Not a new
	fangled Pentium Pro running an emulator but an honest to DEC PDP-11 
	and all the dealing with 9-track (or TK-25, etc) peripherals and
	front panel or console ODT banging which that entails.

	Basically I think it would be a 'good thing' to honor the "spirit" of 
	Dion's mail item as well as the "letter" of it.

	Ummm - not freely redistributable but distributable to those who can
	at least make some sort of case that they do own a PDP-11 capable of
	running UNIX.  Granted, "proving" this over the net might be difficult
	but the effort should at least, I think, be made in an attempt to
	reciprocate SCO's spirit.

	Steven Schultz
	sms at wlv.iipo.gtegsc.com, sms at moe.2bsd.com



From tih at Hamartun.Priv.NO  Sat Jun 29 21:48:08 1996
From: tih at Hamartun.Priv.NO (Tom I Helbekkmo)
Date: Sat, 29 Jun 1996 11:48:08 +0000 (GMT)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <9606272307.AA06842@dolphin>
Message-ID: <960629113832.6626A@barsoom.Hamartun.Priv.NO>

On Fri, 28 Jun 1996, Warren Toomey wrote:

> I know, but I really don't want to try to work under a `head in the sand'
> approach from SCO. At the very least I'd like official blessing to pass
> on old UNIXes to people who need/want them. I know getting SCO to allow
> them to be freely redistributable is a pipe dream, but perhaps there's
> some middle ground they would be willing to move to.

Is there really any good reason for them to object to the distribution
of UNIXes prior to SVR1?  Could there possibly be anything at all in
V7 and earlier that could in any way be damaging to SCO (or anyone
else who might buy UNIX from SCO) if it were freely distributed?  If
I'm right in assuming that it couldn't possibly make a difference to
their bottom line, perhaps SCO could be convinced to formally release
these oldest versions of UNIX?

Does anyone know, by the way, what's happening with the Lions
commentaries?  They're at the top of my "stuff I want to read" list,
and have been for quite some time now!

-tih
-- 
Tom Ivar Helbekkmo
tih at Hamartun.Priv.NO



From wkt at csadfa.cs.adfa.oz.au  Sun Jun 30 09:14:49 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Mon, 1 Jul 1996 09:14:49 +3400 (EST)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <960629113832.6626A@barsoom.Hamartun.Priv.NO> from "Tom I Helbekkmo" at Jun 29, 96 11:48:08 am
Message-ID: <9606302314.AA19468@dolphin>

In atricle by Tom I Helbekkmo:
> On Fri, 28 Jun 1996, Warren Toomey wrote:
> 
> Is there really any good reason for them to object to the distribution
> of UNIXes prior to SVR1?  Could there possibly be anything at all in
> V7 and earlier that could in any way be damaging to SCO (or anyone
> else who might buy UNIX from SCO) if it were freely distributed?  If
> I'm right in assuming that it couldn't possibly make a difference to
> their bottom line, perhaps SCO could be convinced to formally release
> these oldest versions of UNIX?

I suggested to Dion that SCO would get kudos from the Unix community if
they did. Haven't heard back from him yet (still Sunday there).

> Does anyone know, by the way, what's happening with the Lions
> commentaries?  They're at the top of my "stuff I want to read" list,
> and have been for quite some time now!

I have a copy of the PostScript version which floated around the 'net
a few years back. I'd be prepared to give it out on the solemn promise
that people buy Lions' commentaries when they are published.

I'll let you all know how I go with SCO.

Cheers,
	Warren


From wkt at dolphin.cs.adfa.oz.au  Sat Jun 22 16:44:19 1996
From: wkt at dolphin.cs.adfa.oz.au (Warren Toomey)
Date: Sat, 22 Jun 1996 16:44:19 +1000 (EST)
Subject: v6 tapes
In-Reply-To: <Pine.SOL.3.91.960621142754.6541A-100000@sparcy.geneva.edu> from "Jacob Ritorto" at Jun 21, 96 02:40:53 pm
Message-ID: <9606220644.AA06253@dolphin>

In atricle by Jacob Ritorto:
> Warren,
> 	I have three 600' 9-track 800bpi tapes marked (in pen) UNIX V6 
> 4000 blocks.  One Source, one Object, one Documentation.  I've tried 
> using ROLLIN to restore them, but it expects a filename, which I can't 
> find.  I did do an RT11 dump of the first few blocks of the tapes, which 
> revealed an ascii 'd' as the first byte, then a series of decending 
> bytes.  It didn't look like a file structure or a filename.  All the 
> tapes had the same first block.  I tried to boot the tapes directly on an 
> 11/34.  No luck.  I tried to DIR them from rt11, but, of course, rt 
> couldn't find any directory info.  There's definately unix stuff on the 
> tapes--I saw it in the RT11 device dump.  Dates in some of the source 
> files are around 1974 or 1976, if I recall correctly.  
> 	What do I need to do to get these tapes back onto disk and 
> running?  I'm assuming they restore to RK05 disks because the labels say 
> 4000 blocks.  I have 4 rk05 drives and lots of packs.  BTW, the tape 
> drive I'm using is a TU10 with standard address and vector.
> 	Your help would be greatly appreciated.
> Jacob Ritorto

Jacob, I'm passing this onto a bunch of PDP Unix people, as I don't have
the hardware & RT11 experience to tell you how to install v6 from the
tapes. Yes, the 3 tapes are RK05 pack images, I have on-line copies here
if that can be of any help to you. Can you raw dump the tapes to RK05s
using RT11?

Can anybody help Jacob out here? We also have v7, 2.9BSD and 2.11BSD here.
What hardware do you have?

Best of luck,

	Warren Toomey


From wkt at csadfa.cs.adfa.oz.au  Thu Jun 27 12:06:47 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Thu, 27 Jun 1996 12:06:47 +1000 (EST)
Subject: Good News re PDP Unix Licensing
Message-ID: <9606270206.AA02788@dolphin>

All,
	I just received a very pleasing letter from Dion L. Johnson II, the
Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
his email and my response below. If I can get a legally authorative statement
on paper from SCO, I'll pass it on to you all, especially Steven Schultz.

Cheers,
	Warren

In atricle by Dion:
> 
> SCO owns the licensing rights all versions of the UNIX system, or
> so our legal folks tell me.  Now, of course there are many
> derivative, licensed versions, and some of the holders of those
> licenses have rights to sublicense.  In the case of BSD
> enhancements, the Berkeley additions are owned by the Regents of
> the University of California, and I believe the UCB license terms
> are well known.
> 
> As for your friends who have rescued ancient PDP machines...  I
> am confident that SCO would cheerfully encourage them to run UNIX
> on these antiques without any payment to us.  I cant quite
> officially give that permission myself, but I can speculate that
> SCO certainly would not mind.
> 
> So go for it.  Does this help?
> -Dion
>Dion L. Johnson II  - The Santa Cruz Operation, Inc.              dionj at sco.com
>SCO Product Manager - Development Systems and Various Other Stuff
>400 Encinal St.  Santa Cruz, CA 95061    FAX: 408-427-5417  Voice: 408-427-7565

Dion, thanks very much for your email, in fact I'm ecstatic! I know this
could be a tricky legal minefield, so if possible could SCO draft a letter
(and run it past their lawyers) which sets out exactly what you said above.

In particular, you said that ``SCO would cheerfully encourage them to run UNIX
on these antiques without any payment to us''. Does this mean I can legally
distribute the source code to the PDP versions of UNIX, and to anybody? or
just to people who own PDP-11s. There are PDP-11 emulators available, so
it is conceivable that people who don't even have a real PDP-11 might like
to try UNIX out on these emulators. If to anybody, then I assume this means
the source is legally owned by SCO but freely distributable?

I really appreciate your offer of making these old versions of UNIX
available, but given the legal status of the code to this point, I would
like to cover myself with an officially blessed and signed document from SCO.
Let me know what you can do, and many many thanks again for this!

Cheers,
	Warren


From Johnny.Billquist at emw.ericsson.se  Thu Jun 27 19:02:02 1996
From: Johnny.Billquist at emw.ericsson.se (Johnny Billquist konsult)
Date: Thu, 27 Jun 96 11:02:02 +0200
Subject: Good News re PDP Unix Licensing
Message-ID: <9606270902.AA03902@genesis.mo.emw.ericsson.se>

> All,
> 	I just received a very pleasing letter from Dion L. Johnson II, the
> Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
> his email and my response below. If I can get a legally authorative statement
> on paper from SCO, I'll pass it on to you all, especially Steven Schultz.

Not that I wan't to sound pessimistic, but there are several
miles between "would not mind", and "legally allowed".

>From what I read into his letter, he's saying that he don't think
SCO would take legal actions against us, but at the same time they
won't probably make it officially legal.

And your reply, hoping that they'll say that "Unix is legally owned by
SCO, but freely distributable", is really reaching for the sky... :-)

Anyway, keep trying, it would be very nice if they really did write
such a paper.

	Johnny


From wkt at csadfa.cs.adfa.oz.au  Fri Jun 28 09:07:07 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Fri, 28 Jun 1996 09:07:07 +1000 (EST)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <9606270902.AA03902@genesis.mo.emw.ericsson.se> from "Johnny Billquist konsult" at Jun 27, 96 11:02:02 am
Message-ID: <9606272307.AA06842@dolphin>

In atricle by Johnny Billquist konsult:
> 
> > 	I just received a very pleasing letter from Dion L. Johnson II, the
> > Product Manager at SCO, about the legal status of the PDP UNIXs.
> 
> Not that I wan't to sound pessimistic, but there are several
> miles between "would not mind", and "legally allowed".
> From what I read into his letter, he's saying that he don't think
> SCO would take legal actions against us, but at the same time they
> won't probably make it officially legal.
> And your reply, hoping that they'll say that "Unix is legally owned by
> SCO, but freely distributable", is really reaching for the sky... :-)

I know, but I really don't want to try to work under a `head in the sand'
approach from SCO. At the very least I'd like official blessing to pass
on old UNIXes to people who need/want them. I know getting SCO to allow
them to be freely redistributable is a pipe dream, but perhaps there's
some middle ground they would be willing to move to.

I'll keep you all informed of any progress with SCO.

Cheers,
	Warren


From sms at wlv.iipo.gtegsc.com  Fri Jun 28 02:05:17 1996
From: sms at wlv.iipo.gtegsc.com (Steven M. Schultz)
Date: Thu, 27 Jun 1996 09:05:17 -0700 (PDT)
Subject: Good News re PDP Unix Licensing
Message-ID: <199606271605.JAA10306@wlv.iipo.gtegsc.com>

Warren, et al -

> 	I just received a very pleasing letter from Dion L. Johnson II, the
> Product Manager at SCO, about the legal status of the PDP UNIXs. I've included
> his email and my response below. If I can get a legally authorative statement
> on paper from SCO, I'll pass it on to you all, especially Steven Schultz.
> 	Warren

	Wow.  I never even dreamed that such a thing would happen - how times
	(or people) have changed over time.  Very welcome news indeed!

> In particular, you said that ``SCO would cheerfully encourage them to run UNIX
> on these antiques without any payment to us''. Does this mean I can legally
> distribute the source code to the PDP versions of UNIX, and to anybody? or
> just to people who own PDP-11s. There are PDP-11 emulators available, so
> it is conceivable that people who don't even have a real PDP-11 might like
> to try UNIX out on these emulators. If to anybody, then I assume this means
> the source is legally owned by SCO but freely distributable?

	I submit that running an emulator does not demonstrate the proper
	degree of obsession or investment (financial as well as emotional) with 
	``antiques''.  No investment in actual old hardware is involved when 
	running an emulator.  That is, I believe, what SCO is referring
	to when mention is made of ``run UNIX on these antiques''.  Not a new
	fangled Pentium Pro running an emulator but an honest to DEC PDP-11 
	and all the dealing with 9-track (or TK-25, etc) peripherals and
	front panel or console ODT banging which that entails.

	Basically I think it would be a 'good thing' to honor the "spirit" of 
	Dion's mail item as well as the "letter" of it.

	Ummm - not freely redistributable but distributable to those who can
	at least make some sort of case that they do own a PDP-11 capable of
	running UNIX.  Granted, "proving" this over the net might be difficult
	but the effort should at least, I think, be made in an attempt to
	reciprocate SCO's spirit.

	Steven Schultz
	sms at wlv.iipo.gtegsc.com, sms at moe.2bsd.com



From tih at Hamartun.Priv.NO  Sat Jun 29 21:48:08 1996
From: tih at Hamartun.Priv.NO (Tom I Helbekkmo)
Date: Sat, 29 Jun 1996 11:48:08 +0000 (GMT)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <9606272307.AA06842@dolphin>
Message-ID: <960629113832.6626A@barsoom.Hamartun.Priv.NO>

On Fri, 28 Jun 1996, Warren Toomey wrote:

> I know, but I really don't want to try to work under a `head in the sand'
> approach from SCO. At the very least I'd like official blessing to pass
> on old UNIXes to people who need/want them. I know getting SCO to allow
> them to be freely redistributable is a pipe dream, but perhaps there's
> some middle ground they would be willing to move to.

Is there really any good reason for them to object to the distribution
of UNIXes prior to SVR1?  Could there possibly be anything at all in
V7 and earlier that could in any way be damaging to SCO (or anyone
else who might buy UNIX from SCO) if it were freely distributed?  If
I'm right in assuming that it couldn't possibly make a difference to
their bottom line, perhaps SCO could be convinced to formally release
these oldest versions of UNIX?

Does anyone know, by the way, what's happening with the Lions
commentaries?  They're at the top of my "stuff I want to read" list,
and have been for quite some time now!

-tih
-- 
Tom Ivar Helbekkmo
tih at Hamartun.Priv.NO



From wkt at csadfa.cs.adfa.oz.au  Sun Jun 30 09:14:49 1996
From: wkt at csadfa.cs.adfa.oz.au (Warren Toomey)
Date: Mon, 1 Jul 1996 09:14:49 +3400 (EST)
Subject: Good News re PDP Unix Licensing
In-Reply-To: <960629113832.6626A@barsoom.Hamartun.Priv.NO> from "Tom I Helbekkmo" at Jun 29, 96 11:48:08 am
Message-ID: <9606302314.AA19468@dolphin>

In atricle by Tom I Helbekkmo:
> On Fri, 28 Jun 1996, Warren Toomey wrote:
> 
> Is there really any good reason for them to object to the distribution
> of UNIXes prior to SVR1?  Could there possibly be anything at all in
> V7 and earlier that could in any way be damaging to SCO (or anyone
> else who might buy UNIX from SCO) if it were freely distributed?  If
> I'm right in assuming that it couldn't possibly make a difference to
> their bottom line, perhaps SCO could be convinced to formally release
> these oldest versions of UNIX?

I suggested to Dion that SCO would get kudos from the Unix community if
they did. Haven't heard back from him yet (still Sunday there).

> Does anyone know, by the way, what's happening with the Lions
> commentaries?  They're at the top of my "stuff I want to read" list,
> and have been for quite some time now!

I have a copy of the PostScript version which floated around the 'net
a few years back. I'd be prepared to give it out on the solemn promise
that people buy Lions' commentaries when they are published.

I'll let you all know how I go with SCO.

Cheers,
	Warren


