From wkt at cs.adfa.oz.au  Tue Feb 11 15:38:53 1997
From: wkt at cs.adfa.oz.au (Warren Toomey)
Date: Tue, 11 Feb 1997 16:38:53 +1100 (EST)
Subject: PDP-11
Message-ID: <199702110538.QAA08050@csadfa.cs.adfa.oz.au>

	[Mike is taking over a sysadmin job for a UNIX PDP-11.
	 He hasn't seen one before. Can someone help!]

> Thanks for the response. I am told that it's running unix. I start the
> job the end of this week. The person I am replacing will be fired the
> day I start. So, I have never seen their computer room, and will not
> have any passwords or other info. It is critical that I gain control of
> the system and prevent that person from getting into the system. Any
> suggestions would be GREATLY appreciated.
> 
> Thanks again,
> 
> Mike Offenberg
> moffen at ix.netcom.com

Ok, I'd try to chat with the person before they leave! At least to get the
root password.

I am cc'ing this email to a group of PDP-11 Unix users who may be able to
help you out. I'd guess it's a 6th or 7th Edition UNIX, or a 2BSD system.
What you really need to know is:

	+ how to get a root login on the system
	+ how to change the root password
	+ how to reboot the system properly

If the person leaves root logged in on the console, at least you can
type passwd and change the root password. If they haven't, then there
is no easy way of getting a root login to change the password, except
for rebooting the system.

Rebooting depends on what PDP-11 you've got there. If you can find out,
someone here should be able to explain how to boot into single-user mode,
where you have a root prompt and can change the root password and then
go to multi-user mode.

I'll try to get the basic maintenance manuals for these old UNIX systems
put into ASCII and email them to you.

Cheers,
	Warren


From wkt at cs.adfa.oz.au  Tue Feb 11 15:38:53 1997
From: wkt at cs.adfa.oz.au (Warren Toomey)
Date: Tue, 11 Feb 1997 16:38:53 +1100 (EST)
Subject: PDP-11
Message-ID: <199702110538.QAA08050@csadfa.cs.adfa.oz.au>

	[Mike is taking over a sysadmin job for a UNIX PDP-11.
	 He hasn't seen one before. Can someone help!]

> Thanks for the response. I am told that it's running unix. I start the
> job the end of this week. The person I am replacing will be fired the
> day I start. So, I have never seen their computer room, and will not
> have any passwords or other info. It is critical that I gain control of
> the system and prevent that person from getting into the system. Any
> suggestions would be GREATLY appreciated.
> 
> Thanks again,
> 
> Mike Offenberg
> moffen at ix.netcom.com

Ok, I'd try to chat with the person before they leave! At least to get the
root password.

I am cc'ing this email to a group of PDP-11 Unix users who may be able to
help you out. I'd guess it's a 6th or 7th Edition UNIX, or a 2BSD system.
What you really need to know is:

	+ how to get a root login on the system
	+ how to change the root password
	+ how to reboot the system properly

If the person leaves root logged in on the console, at least you can
type passwd and change the root password. If they haven't, then there
is no easy way of getting a root login to change the password, except
for rebooting the system.

Rebooting depends on what PDP-11 you've got there. If you can find out,
someone here should be able to explain how to boot into single-user mode,
where you have a root prompt and can change the root password and then
go to multi-user mode.

I'll try to get the basic maintenance manuals for these old UNIX systems
put into ASCII and email them to you.

Cheers,
	Warren


