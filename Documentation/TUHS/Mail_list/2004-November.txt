From steve at quintile.net  Mon Nov  8 16:44:52 2004
From: steve at quintile.net (Steve Simon)
Date: Mon, 8 Nov 2004 06:44:52 0000
Subject: [TUHS] wwb
Message-ID: <6fbf350c4c842c0fab3d1f892b696f5f@quintile.net>

Hi,

Anyone know the status of the writers workbench (WWB)
which was a seperate package even in System III days 
I think.

I know about style and diction which was shipped with BSD4.1
which (again wooly memory) was an early subset of the
whole wwb package.

I was hoping to compile it up and use it to help me
improve my written English!

-Steve

From arnold at skeeve.com  Mon Nov  8 22:47:45 2004
From: arnold at skeeve.com (Aharon Robbins)
Date: Mon, 8 Nov 2004 14:47:45 +0200
Subject: [TUHS] wwb
Message-ID: <200411081247.iA8CljYI006465@skeeve.com>

I once asked Brian Kernighan about style and diction. His
response was rather uncomplimentary; it's net meaning was
"don't bother with them".

As I recall, wwb was style, diction, maybe one or two other
related programs, and the ditroff suite: troff, tbl, eqn, pic,
and various macro packages.  For the troff stuff, you're
better off with groff, anyway.

Arnold

> From: "Steve Simon" <steve at quintile.net>
> Date: Mon, 8 Nov 2004 06:44:52 0000
> To: tuhs at minnie.tuhs.org
>
> Hi,
>
> Anyone know the status of the writers workbench (WWB)
> which was a seperate package even in System III days 
> I think.
>
> I know about style and diction which was shipped with BSD4.1
> which (again wooly memory) was an early subset of the
> whole wwb package.
>
> I was hoping to compile it up and use it to help me
> improve my written English!
>
> -Steve

From txomsy at yahoo.es  Wed Nov 17 01:53:32 2004
From: txomsy at yahoo.es (Jose R. Valverde)
Date: Tue, 16 Nov 2004 16:53:32 +0100
Subject: [TUHS] Ultrix from CD on SIMH
Message-ID: <20041116165332.5ae15a0b.txomsy@yahoo.es>

Has anybody succeeded installing Ultrix v4.4/4.5 for the VAX on SIMH from 
DEC CD-ROM distribution?

I've been trying but when it goes to device detection it always turns 
up with an empty list. I mean, the install kernel boots, detects the
virtual hard disk and the CD-ROM, the install program starts and reaches
the installation menu (options for BASIC or ADVANCED) and it's then,
when choosing any option that it does not detect any suitable install
device.

It's been about 10 years since last I installed Ultrix on a VAX and to
be true, I can hardly remember all the details involved. It this doesn't
work, I'll try to go back to legacy tapes (if I can still find any 
around).

					j

-- 
	These opinions are mine and only mine. Hey man, I saw them first!

			    José R. Valverde

	De nada sirve la Inteligencia Artificial cuando falta la Natural
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20041116/73fc6ee2/attachment.sig>

From jfoust at threedee.com  Wed Nov 17 02:20:26 2004
From: jfoust at threedee.com (John Foust)
Date: Tue, 16 Nov 2004 10:20:26 -0600
Subject: [TUHS] Old USENET Source Code available
In-Reply-To: <200409261009.i8QA9Ajt020336@skeeve.com>
References: <200409261009.i8QA9Ajt020336@skeeve.com>
Message-ID: <6.2.0.14.2.20041116100841.05442bc8@pc>

At 04:09 AM 9/26/2004, Aharon Robbins wrote:
>A few weeks ago, in a fit of nostalgia, I decided to gather together
>a personal copy of the various Usenet source groups as still available
>at places like gatekeeper.dec.com and ftp.uu.net.  The result is
>a collection of six newsgroups, net.sources, and then
>comp.sources.{games,misc,x,unix,reviewed}.

Consider giving a copy to Google.  Back in August 2000 they were
looking for old Usenet news archive CDs such as those from Sterling
Software, pre-dating 1995.  At the time, I pointed a few leads to and
exchanged a few messages with Michael Schmitt <michael at google.com>.

This applies to anyone with old backup tapes of Usenet news, of course.

- John


From tih at hamartun.priv.no  Mon Nov 22 06:05:04 2004
From: tih at hamartun.priv.no (Tom Ivar Helbekkmo)
Date: Sun, 21 Nov 2004 21:05:04 +0100
Subject: [TUHS] Ultrix from CD on SIMH
In-Reply-To: <20041116165332.5ae15a0b.txomsy@yahoo.es> (Jose R. Valverde's
 message of "Tue, 16 Nov 2004 16:53:32 +0100")
References: <20041116165332.5ae15a0b.txomsy@yahoo.es>
Message-ID: <m2fz33c6j3.fsf@dejah.hamartun.priv.no>

"Jose R. Valverde" <txomsy at yahoo.es> writes:

> I've been trying but when it goes to device detection it always turns 
> up with an empty list. I mean, the install kernel boots, detects the
> virtual hard disk and the CD-ROM, the install program starts and reaches
> the installation menu (options for BASIC or ADVANCED) and it's then,
> when choosing any option that it does not detect any suitable install
> device.

I know that I figured out, a number of years back, how to install
Ultrix on non-supported media.  It involved aborting the install,
editing the install script to "recognize" the target media, and then
restarting the script.  You might want to experiment with this...

-tih
-- 
Popularity is the hallmark of mediocrity.  --Niles Crane, "Frasier"

