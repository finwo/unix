From wkt at henry.cs.adfa.oz.au  Wed Jul  2 10:21:15 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 10:21:15 +1000 (EST)
Subject: PDP11
In-Reply-To: <199707020013.KAA08108@iccu6.ipswich.gil.com.au> from "crr@gil.com.au" at "Jul 2, 97 10:13:49 am"
Message-ID: <199707020021.KAA16859@henry.cs.adfa.oz.au>

In atricle by crr at gil.com.au:
> Hello Warren,
> 
> Just a short note to introduce myself, Colin Riddel is my name,
> I have just
> saved from the scrap heap a PDP11 / 34 with 2 RK5 drives 
> and about 20 disks.. unfortunately the former owners of the system
> wiped all contents from the disks. I would like to be able to 
> get a copy of UNIX for the PDP11, then fully restore the machime
> I would apreciate any information that you can give me on
> obtaining and setting up PDP unix on the beast
> 
> I am a member of HUMBUG (home Unix machine Brisbane user group )
> and one of the system administrators at Global Info-Links (an ISP in
> Ipswich Qld) 
> Colin Riddel

Hi Colin, I saw you sign the petition, thanks. The problem is how to load the
software onto the box! There are RK05 disk images of UNIX (v6 and v7) at
ftp://minnie.adfa.oz.au/pub/PDP-11-sims/Supnik_2.2d/

But how do we get them into the box? Do you have any boot disks for any OS
(RSX, RT-11)? What about tape drives?

I'll pass this onto our mailing list, maybe some more knowledgeable readers
will be able to answer you. Alternatively, posting this to vmsnet.pdp-11
should get you an answers.

Ciao,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul  2 12:00:05 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 12:00:05 +1000 (EST)
Subject: Petition SCO for PDP-11 UNIX Source Licenses
Message-ID: <199707020200.MAA17575@henry.cs.adfa.oz.au>

Hello,
	You may have received email from me last week asking for ideas about
lobbying SCO to make source licenses for PDP-11 UNIXes available. The feedback
from the email was in favour of creating a petition which we would present to
SCO. I mailed a draft petition to many of you and again got a favourable
response.

Therefore, I've set up a WWW petition for PDP-11 UNIX source licenses at
http://minnie.cs.adfa.oz.au/PUPS/petition.html.

PLEASE SIGN THE PETITION!!! if you agree with the petition.

PLEASE GET OTHER PEOPLE TO SIGN TOO!!! We need as many signatures as we can
get. Pass the word around to other interested people you know of.

If you filled in the PDP-11 UNIX source code survey on the same WWW server,
your name has been automatically put on the petition, unless you disagreed
with the petition's contents. Please email me if you want your name removed.
You can re-sign the petition to add your Full Name; I'll tidy it up later.

I'd like to get well over 100 names on the petition before I pass it to SCO.
If you have contacts at SCO who we could send the petition, please email me!
If you'd like to be put on a mailing list to be kept informed of the petition's
progress, also email me.

Hopefully, we will get a result from SCO.

Thanks again,

	Warren Toomey	wkt at cs.adfa.oz.au


From wkt at henry.cs.adfa.oz.au  Wed Jul  2 12:05:23 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 12:05:23 +1000 (EST)
Subject: PDP UNIX Src License Petition
Message-ID: <199707020205.MAA17606@henry.cs.adfa.oz.au>

Apologies if you've already got word of this; I've lost track of who I have
& haven't emailed. There is a petition to sign to convince SCO to make
Unix source licenses available at

	http://minnie.cs.adfa.oz.au/PUPS/petition.html

160 signatures so far, about 120 culled from the survey I was running and
40 in the past 48 hours. Please sign if you want SCO to make licenses
available!

Informal feedback from SCO is good; Dion Johnson there thinks it looks
reasonable & he will be our advocate at SCO.

Thanks,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul 16 09:38:59 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 16 Jul 1997 09:38:59 +1000 (EST)
Subject: Another Old PDP-11 UNIX Tape
Message-ID: <199707152339.JAA01410@henry.cs.adfa.oz.au>

Hi all,
	I've just tracked down another old tape with PDP-11 Unix stuff on it.
It's in the UK.  Anybody there care to chat with George & extract the bits
from the tape so we can put it in the archive?

----- Forwarded message from George Coulouris -----

	To: Warren Toomey <wkt at henry.cs.adfa.oz.au>
	From: George Coulouris <George.Coulouris at dcs.qmw.ac.uk>
	Subject: Re: PDP-11 UNIX

Warren,

I've had a 9-track tape reel in my bottom drawer for about 15 years. I no
longer remember what is on it! It may contain the source of 'em' and a few
other utilities that I wrote in the '70s. Unfortunately I have no
convenient way to read it. If you are interested, I could try to find
someone with a 9-track reel drive, or if you have one I could just
send the tape to you. I'd be happy to liase with anyone who is willing to
have a go at reading the tape.

George

----- End of forwarded message from George Coulouris -----

Also, the source code petition has now got 290 signatures. I haven't heard
anything from SCO since I passed it to them formally, except that they had
received it and it was being passed to `the right people'. I'll keep updating
the status page, hyperlinked to the petition itself, as things happen.

The petition's at http://minnie.cs.adfa.oz.au/PUPS/petition.html. Quite a
number of well-known people have signed. Both Andy Tan(n)enbaums, Steven
Schultz, Richard Tobin, Bill Joy, Henry Spencer, Neil Groundwater, Dave
Presotto, Andrew Hume, Peter Collinson, Greg Rose, Brian Redman, Peter
Honeyman, Megan Gentry, Jim McKie, Margo Seltzer, John Mashey, Peter Salus,
Ozalp Babaoglu, Keith Bostic, George Coulouris etc. Plus all the other
individuals who have shown support.

Cheers,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul 23 13:36:12 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 23 Jul 1997 13:36:12 +1000 (EST)
Subject: New PDP-11 UNIX Archive Additions
Message-ID: <199707230336.NAA01232@henry.cs.adfa.oz.au>

All,
	I've just received a couple of _huge_ tarballs from Keith Bostic
with the following PDP-11 UNIX stuff:

        1BSD
        2.10BSD
        2.79BSD
        2.8BSD
        2.9BSD
        2BSD		First 2BSD tape.
        3BSD		First 3BSD tape.
        pascal.2.0      Pascal distribution
        pascal.2.10     Pascal distribution

        32V
        Documentors WorkBench.
        pdp.archive     ???
        pwb		3 slightly different & incomplete versions
        v6              6th Edition
        v6.compat       ???
        v7              7th Edition
        v7add           The "40 changes" tape.
        v7m             Follow-on release to V7

Due to space considerations (& a lack of time), I haven't been able to
extract anything into the licensed UNIX archive that I run on henry. I'd
also like to try and remove duplicates where possible.

If you're interested in a particular thing from the tarballs, please email
me and I'll extract the relevant sections.

Also: 300+ signatures on the PDP-11 UNIX src license petition. I just sent
a reminder email to SCO to see what they're doing with it.

Cheers,
	Warren


From rjm at swift.eng.ox.ac.uk  Wed Jul 23 19:46:40 1997
From: rjm at swift.eng.ox.ac.uk (Bob Manners)
Date: Wed, 23 Jul 1997 10:46:40 +0100 (BST)
Subject: PDP-11/73 UNIX
Message-ID: <m0wqy0H-0003dNC@europa.ox.ac.uk>


I have a micro11/73 with RX50 and RD53 drives. Is there a BSD variant
which I can run on the thing?

Regards,
Bob

-- 
------------------------------------------------------------------------------
Bob Manners                       (My REAL address is: rjm at swift.eng.ox.ac.uk)
BOB'S COMPUTER MUSEUM:               http://swift.eng.ox.ac.uk/rjm/museum.html
------------------------------------------------------------------------------


From wkt at henry.cs.adfa.oz.au  Wed Jul  2 10:21:15 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 10:21:15 +1000 (EST)
Subject: PDP11
In-Reply-To: <199707020013.KAA08108@iccu6.ipswich.gil.com.au> from "crr@gil.com.au" at "Jul 2, 97 10:13:49 am"
Message-ID: <199707020021.KAA16859@henry.cs.adfa.oz.au>

In atricle by crr at gil.com.au:
> Hello Warren,
> 
> Just a short note to introduce myself, Colin Riddel is my name,
> I have just
> saved from the scrap heap a PDP11 / 34 with 2 RK5 drives 
> and about 20 disks.. unfortunately the former owners of the system
> wiped all contents from the disks. I would like to be able to 
> get a copy of UNIX for the PDP11, then fully restore the machime
> I would apreciate any information that you can give me on
> obtaining and setting up PDP unix on the beast
> 
> I am a member of HUMBUG (home Unix machine Brisbane user group )
> and one of the system administrators at Global Info-Links (an ISP in
> Ipswich Qld) 
> Colin Riddel

Hi Colin, I saw you sign the petition, thanks. The problem is how to load the
software onto the box! There are RK05 disk images of UNIX (v6 and v7) at
ftp://minnie.adfa.oz.au/pub/PDP-11-sims/Supnik_2.2d/

But how do we get them into the box? Do you have any boot disks for any OS
(RSX, RT-11)? What about tape drives?

I'll pass this onto our mailing list, maybe some more knowledgeable readers
will be able to answer you. Alternatively, posting this to vmsnet.pdp-11
should get you an answers.

Ciao,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul  2 12:00:05 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 12:00:05 +1000 (EST)
Subject: Petition SCO for PDP-11 UNIX Source Licenses
Message-ID: <199707020200.MAA17575@henry.cs.adfa.oz.au>

Hello,
	You may have received email from me last week asking for ideas about
lobbying SCO to make source licenses for PDP-11 UNIXes available. The feedback
from the email was in favour of creating a petition which we would present to
SCO. I mailed a draft petition to many of you and again got a favourable
response.

Therefore, I've set up a WWW petition for PDP-11 UNIX source licenses at
http://minnie.cs.adfa.oz.au/PUPS/petition.html.

PLEASE SIGN THE PETITION!!! if you agree with the petition.

PLEASE GET OTHER PEOPLE TO SIGN TOO!!! We need as many signatures as we can
get. Pass the word around to other interested people you know of.

If you filled in the PDP-11 UNIX source code survey on the same WWW server,
your name has been automatically put on the petition, unless you disagreed
with the petition's contents. Please email me if you want your name removed.
You can re-sign the petition to add your Full Name; I'll tidy it up later.

I'd like to get well over 100 names on the petition before I pass it to SCO.
If you have contacts at SCO who we could send the petition, please email me!
If you'd like to be put on a mailing list to be kept informed of the petition's
progress, also email me.

Hopefully, we will get a result from SCO.

Thanks again,

	Warren Toomey	wkt at cs.adfa.oz.au


From wkt at henry.cs.adfa.oz.au  Wed Jul  2 12:05:23 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 2 Jul 1997 12:05:23 +1000 (EST)
Subject: PDP UNIX Src License Petition
Message-ID: <199707020205.MAA17606@henry.cs.adfa.oz.au>

Apologies if you've already got word of this; I've lost track of who I have
& haven't emailed. There is a petition to sign to convince SCO to make
Unix source licenses available at

	http://minnie.cs.adfa.oz.au/PUPS/petition.html

160 signatures so far, about 120 culled from the survey I was running and
40 in the past 48 hours. Please sign if you want SCO to make licenses
available!

Informal feedback from SCO is good; Dion Johnson there thinks it looks
reasonable & he will be our advocate at SCO.

Thanks,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul 16 09:38:59 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 16 Jul 1997 09:38:59 +1000 (EST)
Subject: Another Old PDP-11 UNIX Tape
Message-ID: <199707152339.JAA01410@henry.cs.adfa.oz.au>

Hi all,
	I've just tracked down another old tape with PDP-11 Unix stuff on it.
It's in the UK.  Anybody there care to chat with George & extract the bits
from the tape so we can put it in the archive?

----- Forwarded message from George Coulouris -----

	To: Warren Toomey <wkt at henry.cs.adfa.oz.au>
	From: George Coulouris <George.Coulouris at dcs.qmw.ac.uk>
	Subject: Re: PDP-11 UNIX

Warren,

I've had a 9-track tape reel in my bottom drawer for about 15 years. I no
longer remember what is on it! It may contain the source of 'em' and a few
other utilities that I wrote in the '70s. Unfortunately I have no
convenient way to read it. If you are interested, I could try to find
someone with a 9-track reel drive, or if you have one I could just
send the tape to you. I'd be happy to liase with anyone who is willing to
have a go at reading the tape.

George

----- End of forwarded message from George Coulouris -----

Also, the source code petition has now got 290 signatures. I haven't heard
anything from SCO since I passed it to them formally, except that they had
received it and it was being passed to `the right people'. I'll keep updating
the status page, hyperlinked to the petition itself, as things happen.

The petition's at http://minnie.cs.adfa.oz.au/PUPS/petition.html. Quite a
number of well-known people have signed. Both Andy Tan(n)enbaums, Steven
Schultz, Richard Tobin, Bill Joy, Henry Spencer, Neil Groundwater, Dave
Presotto, Andrew Hume, Peter Collinson, Greg Rose, Brian Redman, Peter
Honeyman, Megan Gentry, Jim McKie, Margo Seltzer, John Mashey, Peter Salus,
Ozalp Babaoglu, Keith Bostic, George Coulouris etc. Plus all the other
individuals who have shown support.

Cheers,
	Warren


From wkt at henry.cs.adfa.oz.au  Wed Jul 23 13:36:12 1997
From: wkt at henry.cs.adfa.oz.au (Warren Toomey)
Date: Wed, 23 Jul 1997 13:36:12 +1000 (EST)
Subject: New PDP-11 UNIX Archive Additions
Message-ID: <199707230336.NAA01232@henry.cs.adfa.oz.au>

All,
	I've just received a couple of _huge_ tarballs from Keith Bostic
with the following PDP-11 UNIX stuff:

        1BSD
        2.10BSD
        2.79BSD
        2.8BSD
        2.9BSD
        2BSD		First 2BSD tape.
        3BSD		First 3BSD tape.
        pascal.2.0      Pascal distribution
        pascal.2.10     Pascal distribution

        32V
        Documentors WorkBench.
        pdp.archive     ???
        pwb		3 slightly different & incomplete versions
        v6              6th Edition
        v6.compat       ???
        v7              7th Edition
        v7add           The "40 changes" tape.
        v7m             Follow-on release to V7

Due to space considerations (& a lack of time), I haven't been able to
extract anything into the licensed UNIX archive that I run on henry. I'd
also like to try and remove duplicates where possible.

If you're interested in a particular thing from the tarballs, please email
me and I'll extract the relevant sections.

Also: 300+ signatures on the PDP-11 UNIX src license petition. I just sent
a reminder email to SCO to see what they're doing with it.

Cheers,
	Warren


From rjm at swift.eng.ox.ac.uk  Wed Jul 23 19:46:40 1997
From: rjm at swift.eng.ox.ac.uk (Bob Manners)
Date: Wed, 23 Jul 1997 10:46:40 +0100 (BST)
Subject: PDP-11/73 UNIX
Message-ID: <m0wqy0H-0003dNC@europa.ox.ac.uk>


I have a micro11/73 with RX50 and RD53 drives. Is there a BSD variant
which I can run on the thing?

Regards,
Bob

-- 
------------------------------------------------------------------------------
Bob Manners                       (My REAL address is: rjm at swift.eng.ox.ac.uk)
BOB'S COMPUTER MUSEUM:               http://swift.eng.ox.ac.uk/rjm/museum.html
------------------------------------------------------------------------------


