From cyrille.lefevre-lists at laposte.net  Wed Jan  4 13:37:35 2012
From: cyrille.lefevre-lists at laposte.net (Cyrille Lefevre)
Date: Wed, 04 Jan 2012 04:37:35 +0100
Subject: [TUHS] hello, world
In-Reply-To: <CAO_Q6iw2KT1gSnSWb2234ufTUGS6sUpkhLfPJth==RkL8ZFQsw@mail.gmail.com>
References: <1324818619.20775.for-standards-violators@oclsc.org>
	<20111226211145.GA1335@minnie.tuhs.org>
	<CAO_Q6iw2KT1gSnSWb2234ufTUGS6sUpkhLfPJth==RkL8ZFQsw@mail.gmail.com>
Message-ID: <4F03C97F.8060001@laposte.net>

Le 27/12/2011 07:11, Adam a écrit :
>> Yes, a good reminder on the power that programming brings us. A question
>> though: what command would "bring the system down"?
>
> Great read.
> Would just like to add that on some Unixes a simple command with no
> args will bring a system down - killall.

happy new year to everyone.

that rememer the day where I was looking on IRIX killall manual in a
window which say you could "killall some_process_by_name" and try it
on a... solaris server in another window. the answer was fast :
well, humm, where is my second window ?

PS : solaris killall doesn't take args %-/

Regards,

Cyrille Lefevre
-- 
mailto:Cyrille.Lefevre-lists at laposte.net



From vasco at icpnet.pl  Fri Jan  6 20:42:54 2012
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Fri, 06 Jan 2012 11:42:54 +0100
Subject: [TUHS] qemu and virtualbox
Message-ID: <4F06D02E.5020705@icpnet.pl>

Hi,
For those who want to refresh old memories.
Hans Bezemer has made big progress as far as performance of qemu is 
concerned. His specific experience concerns Coherent, but other old 
unixes could use it.
Details can be found in comp.os.coherent news group

http://groups.google.com/group/comp.os.coherent/topics


The older tuhs message can be mentioned :

http://minnie.tuhs.org/pipermail/tuhs/2008-July/001815.html

I have also noticed , that Coherent boots well  and is very fast in 
newest virtualbox 4.1.8, running in XP.
Dell Optiplex 755 was used  with Core 2 Duo.
Details can be found in  forums for "Other quests"

http://www.virtualbox.org
 
It suggests , that other old Unixes could benefit in new virtualbox.

Regards
Andrzej


From madcrow.maxwell at gmail.com  Sat Jan  7 01:05:19 2012
From: madcrow.maxwell at gmail.com (Michael Kerpan)
Date: Fri, 6 Jan 2012 10:05:19 -0500
Subject: [TUHS] qemu and virtualbox
In-Reply-To: <4F06D02E.5020705@icpnet.pl>
References: <4F06D02E.5020705@icpnet.pl>
Message-ID: <CAHfSdrXO9z5ioHeGtUHOcwxaE+dfaF7Th=rR7yRnst_ScJLhTA@mail.gmail.com>

Very cool to see. I know that Xenix has recently started working on
QEMU as well, so it would seem that there were some major improvements
to legacy support recently.

Mike


From arnold at skeeve.com  Sun Jan  8 21:42:54 2012
From: arnold at skeeve.com (arnold at skeeve.com)
Date: Sun, 8 Jan 2012 03:42:54 -0800
Subject: [TUHS] uunet ftp sources archive available
Message-ID: <201201081142.q08Bgsow007933@freefriends.org>

Hi All.

I announceced this some years ago but it's been renewed, so I'll announce
it again.

In 2004 sometime I downloaded all the comp.sources stuff I could from
uunet.uu.net, which was still making it available for anonymous ftp.

I've made a tarball of it available from http://www.skeeve.com/Usenet.tar.bz2

Warren, if you don't have this in the TUHS archives, maybe you could add it?

Thanks,

Arnold


From arnold at skeeve.com  Fri Jan 20 06:34:20 2012
From: arnold at skeeve.com (Aharon Robbins)
Date: Thu, 19 Jan 2012 22:34:20 +0200
Subject: [TUHS] awkcc source code available
Message-ID: <201201192034.q0JKYKjJ003051@skeeve.com>

Hi. I announced this in comp.lang.awk in December and tried to BCC
the TUHS list but it didn't seem to happen.  Here's the announcement
I posted.

Arnold
-----------------------------------------------
From: arnold at skeeve.com (Aharon Robbins)
Newsgroups: comp.lang.awk
Subject: AWKCC source now available
Date: Fri, 16 Dec 2011 12:27:39 +0000 (UTC)
Message-ID: <jcfdfr$qt9$1 at dont-email.me>

[ BCC to TUHS list, Brian Kernighan & Chris Ramming ]

	awk 'BEGIN { print "Sherman, set the wayback machine for 1988" }'

Hello All.

This note is to announce that through the valiant efforts of Brian Kernighan,
Alcatel-Lucent has been persuaded to make the source for awkcc available.
It can be found at:

	https://open-innovation.alcatel-lucent.com/projects/awkcc

You have to register (no cost) before being able to download, but
it's easy.  The license terms are at the site.  It's a straightforward
"for personal use" kind of license.

For those who don't know, awkcc is an adaptation of Unix awk to translate
nawk programs into C.  It was originally implemented by Chris Ramming (then
at Bell Labs, although no longer) circa 1988, and the source dist includes
some doc that Chris wrote.

Given how fast machines are these days, this program is mostly of
historical interest.  But it's nice to have this bit of Unix / awk history
generally available.  And, if you really need to turn an awk program into
C, this may provide a starting point.

Enjoy!
-- 
Aharon (Arnold) Robbins 			arnold AT skeeve DOT com
P.O. Box 354		Home Phone: +972  8 979-0381
Nof Ayalon		Cell Phone: +972 50 729-7545
D.N. Shimshon 99785	ISRAEL


From grog at lemis.com  Tue Jan 31 11:36:31 2012
From: grog at lemis.com (Greg 'groggy' Lehey)
Date: Tue, 31 Jan 2012 12:36:31 +1100
Subject: [TUHS] History of man pages
Message-ID: <20120131013631.GB45125@dereel.lemis.com>

I've received a link http://manpages.bsd.lv/history.html claiming to
be about man pages; in fact, it's a lot more than that, including the
prehistory of troff.  Interesting stuff.

Greg
--
Sent from my desktop computer
Finger grog at FreeBSD.org for PGP public key.
See complete headers for address and phone numbers.
This message is digitally signed.  If your Microsoft MUA reports
problems, please read http://tinyurl.com/broken-mua
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 196 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20120131/ecd35fb0/attachment.sig>

From wkt at tuhs.org  Tue Jan 31 11:53:36 2012
From: wkt at tuhs.org (Warren Toomey)
Date: Tue, 31 Jan 2012 11:53:36 +1000
Subject: [TUHS] History of man pages
In-Reply-To: <20120131013631.GB45125@dereel.lemis.com>
References: <20120131013631.GB45125@dereel.lemis.com>
Message-ID: <20120131015335.GA31880@minnie.tuhs.org>

On Tue, Jan 31, 2012 at 12:36:31PM +1100, Greg 'groggy' Lehey wrote:
> I've received a link http://manpages.bsd.lv/history.html claiming to
> be about man pages; in fact, it's a lot more than that, including the
> prehistory of troff.  Interesting stuff.
> 
> Greg

Thanks Greg, yes a good read. Pity we've lost the PDP-11 asm
version of roff from 1st/2nd Edition Unix.
	Warren


From ron at ronnatalie.com  Tue Jan 31 23:04:09 2012
From: ron at ronnatalie.com (Ronald Natalie)
Date: Tue, 31 Jan 2012 08:04:09 -0500
Subject: [TUHS] History of man pages
In-Reply-To: <20120131015335.GA31880@minnie.tuhs.org>
References: <20120131013631.GB45125@dereel.lemis.com>
	<20120131015335.GA31880@minnie.tuhs.org>
Message-ID: <34F8DEFE-9667-4915-9A3F-703EE78C07E1@ronnatalie.com>

Gosh, I'd forgotten about that.    "Roff is the simplest of the text formatting programs but is utterly frozen."
They finally dragged me off troff in the mid-90's.


On Jan 30, 2012, at 8:53 PM, Warren Toomey wrote:

> On Tue, Jan 31, 2012 at 12:36:31PM +1100, Greg 'groggy' Lehey wrote:
>> I've received a link http://manpages.bsd.lv/history.html claiming to
>> be about man pages; in fact, it's a lot more than that, including the
>> prehistory of troff.  Interesting stuff.
>> 
>> Greg
> 
> Thanks Greg, yes a good read. Pity we've lost the PDP-11 asm
> version of roff from 1st/2nd Edition Unix.
> 	Warren
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs



