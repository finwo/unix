From robinb at ruffnready.co.uk  Sat Jun  2 00:18:48 2007
From: robinb at ruffnready.co.uk (robinb at ruffnready.co.uk)
Date: Fri, 01 Jun 2007 15:18:48 +0100
Subject: [pups] Surplus Kit
Message-ID: <E1Hu7xo-000M5z-66@pr-webmail-2.demon.net>

Hi Guys,
I'm having a bit of a clearout.  Moving house and so on.  I find that I have a pair of uVAX 3400s going spare.  It's a long time since I've looked at these but from memory they've each got three drives and I think one of them has an external disc cab.  There will be some serial cards in them and possibly, though I'm not sure, a SCSI tape drive card.  There is the normal TK70 cartridge drive.

They were both working when I got them and I have done nothing with them at all (they are in storage at the moment).  Both have, I think, got VMS on them.

These are available to collect in Cirencester in the UK.

Make me an offer someone.

Robin



From wkt at tuhs.org  Mon Jun  4 11:57:39 2007
From: wkt at tuhs.org (Warren Toomey)
Date: Mon, 4 Jun 2007 11:57:39 +1000
Subject: [pups] A tool to compare code trees, plus a favour
Message-ID: <20070604015739.GA16942@minnie.tuhs.org>

Hi all, back in 2004 I wrote a tool to compare sets of C code trees,
as a response to the SCO vs. IBM lawsuit. I've revisited and improved
the performance of the tool, and I have released a new version at
http://minnie.tuhs.org/Programs/Ctcompare/

The tool produces a tokenised representation of each source tree in
a format called CTF. The CTF representation of a proprietary source
tree can be exported without revealing too much of the source code.
See http://minnie.tuhs.org/Programs/Ctcompare/README.txt for the
full details.

I would dearly love to get hold of some CTF files of more recent
UNIX source trees, i.e. from SysVR4 onwards, and especially the
Unixware source trees which relate the the SCO vs. IBM lawsuit.
If you can make them available to me, I would appreciate it. I
understand that you might wish to donate these anonymously, so
soon I will write a web script to allow you to upload a CTF file
to my server "minnie" anonymously.

Many thanks in advance!
	Warren


From cowan at ccil.org  Mon Jun  4 13:40:42 2007
From: cowan at ccil.org (John Cowan)
Date: Sun, 3 Jun 2007 23:40:42 -0400
Subject: [pups] [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070604015739.GA16942@minnie.tuhs.org>
References: <20070604015739.GA16942@minnie.tuhs.org>
Message-ID: <20070604034042.GF22600@mercury.ccil.org>

Warren Toomey scripsit:

> If you can make them available to me, I would appreciate it. I
> understand that you might wish to donate these anonymously, so
> soon I will write a web script to allow you to upload a CTF file
> to my server "minnie" anonymously.

Be careful to make sure that what is uploaded anonymously cannot be
downloaded anonymously, or minnie will become infested with warez d00ds.

-- 
I suggest you call for help,                    John Cowan
or learn the difficult art of mud-breathing.    cowan at ccil.org
        --Great-Souled Sam                      http://www.ccil.org/~cowan


From jrvalverde at cnb.uam.es  Tue Jun  5 18:47:47 2007
From: jrvalverde at cnb.uam.es (Jose R. Valverde)
Date: Tue, 5 Jun 2007 10:47:47 +0200
Subject: [pups] [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070604015739.GA16942@minnie.tuhs.org>
References: <20070604015739.GA16942@minnie.tuhs.org>
Message-ID: <20070605104747.70d0569b@veda.cnb.uam.es>

Hi Warren,

	what version of BerkeleyDB are you using? I downloaded ctcompare
and it gave me a bunch of errors on compilation regarding database structures
and methods.

	Thanks
					j

On Mon, 4 Jun 2007 11:57:39 +1000
Warren Toomey <wkt at tuhs.org> wrote:
> Hi all, back in 2004 I wrote a tool to compare sets of C code trees,
> as a response to the SCO vs. IBM lawsuit. I've revisited and improved
> the performance of the tool, and I have released a new version at
> http://minnie.tuhs.org/Programs/Ctcompare/
> 
> The tool produces a tokenised representation of each source tree in
> a format called CTF. The CTF representation of a proprietary source
> tree can be exported without revealing too much of the source code.
> See http://minnie.tuhs.org/Programs/Ctcompare/README.txt for the
> full details.
> 
> I would dearly love to get hold of some CTF files of more recent
> UNIX source trees, i.e. from SysVR4 onwards, and especially the
> Unixware source trees which relate the the SCO vs. IBM lawsuit.
> If you can make them available to me, I would appreciate it. I
> understand that you might wish to donate these anonymously, so
> soon I will write a web script to allow you to upload a CTF file
> to my server "minnie" anonymously.
> 
> Many thanks in advance!
> 	Warren
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs


-- 
	These opinions are mine and only mine. Hey man, I saw them first!

			    José R. Valverde

	De nada sirve la Inteligencia Artificial cuando falta la Natural
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20070605/2c4d6881/attachment.sig>

From wkt at tuhs.org  Tue Jun  5 19:15:00 2007
From: wkt at tuhs.org (Warren Toomey)
Date: Tue, 5 Jun 2007 19:15:00 +1000
Subject: [pups] [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070605104747.70d0569b@veda.cnb.uam.es>
References: <20070604015739.GA16942@minnie.tuhs.org>
	<20070605104747.70d0569b@veda.cnb.uam.es>
Message-ID: <20070605091500.GA76430@minnie.tuhs.org>

On Tue, Jun 05, 2007 at 10:47:47AM +0200, Jose R. Valverde wrote:
> 	what version of BerkeleyDB are you using? I downloaded ctcompare
> and it gave me a bunch of errors on compilation regarding database structures
> and methods.

I'm using what is built into FreeBSD 6.x. According to
/usr/src/lib/libc/db/README on this system:

		This is version 1.85 of the Berkeley DB code.

Cheers,
	Warren


From wkt at tuhs.org  Mon Jun  4 11:57:39 2007
From: wkt at tuhs.org (Warren Toomey)
Date: Mon, 4 Jun 2007 11:57:39 +1000
Subject: [TUHS] A tool to compare code trees, plus a favour
Message-ID: <20070604015739.GA16942@minnie.tuhs.org>

Hi all, back in 2004 I wrote a tool to compare sets of C code trees,
as a response to the SCO vs. IBM lawsuit. I've revisited and improved
the performance of the tool, and I have released a new version at
http://minnie.tuhs.org/Programs/Ctcompare/

The tool produces a tokenised representation of each source tree in
a format called CTF. The CTF representation of a proprietary source
tree can be exported without revealing too much of the source code.
See http://minnie.tuhs.org/Programs/Ctcompare/README.txt for the
full details.

I would dearly love to get hold of some CTF files of more recent
UNIX source trees, i.e. from SysVR4 onwards, and especially the
Unixware source trees which relate the the SCO vs. IBM lawsuit.
If you can make them available to me, I would appreciate it. I
understand that you might wish to donate these anonymously, so
soon I will write a web script to allow you to upload a CTF file
to my server "minnie" anonymously.

Many thanks in advance!
	Warren


From cowan at ccil.org  Mon Jun  4 13:40:42 2007
From: cowan at ccil.org (John Cowan)
Date: Sun, 3 Jun 2007 23:40:42 -0400
Subject: [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070604015739.GA16942@minnie.tuhs.org>
References: <20070604015739.GA16942@minnie.tuhs.org>
Message-ID: <20070604034042.GF22600@mercury.ccil.org>

Warren Toomey scripsit:

> If you can make them available to me, I would appreciate it. I
> understand that you might wish to donate these anonymously, so
> soon I will write a web script to allow you to upload a CTF file
> to my server "minnie" anonymously.

Be careful to make sure that what is uploaded anonymously cannot be
downloaded anonymously, or minnie will become infested with warez d00ds.

-- 
I suggest you call for help,                    John Cowan
or learn the difficult art of mud-breathing.    cowan at ccil.org
        --Great-Souled Sam                      http://www.ccil.org/~cowan


From jrvalverde at cnb.uam.es  Tue Jun  5 18:47:47 2007
From: jrvalverde at cnb.uam.es (Jose R. Valverde)
Date: Tue, 5 Jun 2007 10:47:47 +0200
Subject: [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070604015739.GA16942@minnie.tuhs.org>
References: <20070604015739.GA16942@minnie.tuhs.org>
Message-ID: <20070605104747.70d0569b@veda.cnb.uam.es>

Hi Warren,

	what version of BerkeleyDB are you using? I downloaded ctcompare
and it gave me a bunch of errors on compilation regarding database structures
and methods.

	Thanks
					j

On Mon, 4 Jun 2007 11:57:39 +1000
Warren Toomey <wkt at tuhs.org> wrote:
> Hi all, back in 2004 I wrote a tool to compare sets of C code trees,
> as a response to the SCO vs. IBM lawsuit. I've revisited and improved
> the performance of the tool, and I have released a new version at
> http://minnie.tuhs.org/Programs/Ctcompare/
> 
> The tool produces a tokenised representation of each source tree in
> a format called CTF. The CTF representation of a proprietary source
> tree can be exported without revealing too much of the source code.
> See http://minnie.tuhs.org/Programs/Ctcompare/README.txt for the
> full details.
> 
> I would dearly love to get hold of some CTF files of more recent
> UNIX source trees, i.e. from SysVR4 onwards, and especially the
> Unixware source trees which relate the the SCO vs. IBM lawsuit.
> If you can make them available to me, I would appreciate it. I
> understand that you might wish to donate these anonymously, so
> soon I will write a web script to allow you to upload a CTF file
> to my server "minnie" anonymously.
> 
> Many thanks in advance!
> 	Warren
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs


-- 
	These opinions are mine and only mine. Hey man, I saw them first!

			    José R. Valverde

	De nada sirve la Inteligencia Artificial cuando falta la Natural
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20070605/2c4d6881/attachment-0001.sig>

From wkt at tuhs.org  Tue Jun  5 19:15:00 2007
From: wkt at tuhs.org (Warren Toomey)
Date: Tue, 5 Jun 2007 19:15:00 +1000
Subject: [TUHS] A tool to compare code trees, plus a favour
In-Reply-To: <20070605104747.70d0569b@veda.cnb.uam.es>
References: <20070604015739.GA16942@minnie.tuhs.org>
	<20070605104747.70d0569b@veda.cnb.uam.es>
Message-ID: <20070605091500.GA76430@minnie.tuhs.org>

On Tue, Jun 05, 2007 at 10:47:47AM +0200, Jose R. Valverde wrote:
> 	what version of BerkeleyDB are you using? I downloaded ctcompare
> and it gave me a bunch of errors on compilation regarding database structures
> and methods.

I'm using what is built into FreeBSD 6.x. According to
/usr/src/lib/libc/db/README on this system:

		This is version 1.85 of the Berkeley DB code.

Cheers,
	Warren


From slapinid at gmail.com  Sat Jun 16 00:58:17 2007
From: slapinid at gmail.com (Sergey Lapin)
Date: Fri, 15 Jun 2007 15:58:17 +0100 (BST)
Subject: [TUHS] Invite from Sergey Lapin (slapinid@gmail.com)
Message-ID: <20070615145817.2C0242B0723@www1.quechup.com>

Join Sergey on Quechup

You have been invited to join
SergeyLapin's
friends network

http://quechup.com/ - REGISTER NOW FOR FREE and find out why everyone's
joining

As a member of Quechup you can...
- Start a blog and share your thoughts with private groups, friends or the
world
- Get in touch with old friends.
- Meet new people in your area, or anywhere else in the world.
- Online instant messenger with full video and audio support.
- Socialize with 'friends-of-friends' and mutual acquaintances.
- Increase your social circle
... and so much more!

Become part of Sergey's Quechup.com friends.

New &amp; Coming Soon on Quechup
--------------------------------
- Games - play Solitaire, Mahjong, Dice, Bubble Up and more online
- Blogs - let people know what you're up to
- Video Posts - use your webcam to record video clips on Quechup
- Member Comments - on photos and blogs

------------------------------------------------------------------
You received this because Sergey Lapin (slapinid at gmail.com) knows and
agreed to invite you. You will only receive one invitation from
slapinid at gmail.com. Quechup will not spam or sell your email address, see
our privacy policy - http://quechup.com/privacy.php
Go to
http://quechup.com/emailunsubscribe.php/ZW09dHVoc0BtaW5uaWUudHVocy5vcmc%3D
if you do not wish to receive any more emails from Quechup.
------------------------------------------------------------------

This e-mail was sent on behalf of SergeyCopyright Quechup.com 2007.
Quechup.com is owned by iDate Ltd
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20070615/6382d385/attachment.html>

