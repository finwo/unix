From brad at anduin.eldar.org  Sat May  1 01:38:45 2010
From: brad at anduin.eldar.org (Brad Spencer)
Date: Fri, 30 Apr 2010 11:38:45 -0400 (EDT)
Subject: [TUHS] UniFlex
In-Reply-To: <20100430095823.78e5a8ac@cnb.csic.es> (jrvalverde@cnb.csic.es)
References: <u2u46b366131004162212lf8852e25uf900b73b040410be@mail.gmail.com>
	<130de1e4e4162da466b3dc04bbc53c70.squirrel@webmail.xs4all.nl>
	<20100425022942.GA15137@dereel.lemis.com>
	<201004261903.36146.wes.parish@paradise.net.nz>
	<20100426080700.GD15137@dereel.lemis.com>
	<1272274418.4bd55df2c23b4@www.paradise.net.nz>
	<k2v46b366131004261052pd86642cm2975037ba1b3fbc0@mail.gmail.com>
	<4BD79DE2.2010203@laposte.net> <20100430095823.78e5a8ac@cnb.csic.es>
Message-ID: <201004301538.o3UFcjtY002780@anduin.eldar.org>


   P.S. (sorry for following up to myself)

	   I had a look into the files, and the system comes with commented
   source code in assembler.

	   FWIW this is a full distribution, including development environment 
   (C, Cobol, Fortran-77,..), editors, kernel, VSAM database, etc... all of it 
   with source code and documentation,

	   From my first cursory look most of it is written in assemble, comes
   with sample test code and is documented enough to be understandable. The 
   pascal, cobol and fortran 77 compilers are written in pascal (!), the C
   compiler is written in assembler.

	   It looks like the environment must have been only vaguely UNIX-like
   but yet I find it mesmerizing enough considering where it ran and when. It
   adds another dimension to understand the impact UNIX had and how it spun off
   lookalikes and sprung the imagination of developers of the time expanding its
   heritage sideways.

				   j
   -- 
			   EMBnet/CNB
		   Scientific Computing Service
	   Solving all your computer needs for Scientific
			   Research.

		   http://bioportal.cnb.csic.es
		     http://www.es.embnet.org
   _______________________________________________
   TUHS mailing list
   TUHS at minnie.tuhs.org
   https://minnie.tuhs.org/mailman/listinfo/tuhs



Wow, what a blast from the past.  I used Flex on the Radio Shack Color
Computer in 64MB of memory in the '80s for a while.  Yes, the entire thing
was assembly and some Fortran.  I did not have the source to most of it
for my version, only the compiled binaries.  The version I used ran off
floppy disks, of course, and used a strange sector layout.  Something like
17 sectors of 256 bytes, instead of the 18 sectors 256 bytes that the
Color Computer usually used.

Neat system in some ways.  Way more "professional" then is peers, except
for OS/9 from Microware.

Flex was only very vaguely Unix, however.  OS/9, which I used a whole lot
on the Color Computer and Color Computer 3, was a lot more Unix like.  I
understand that the 6809 version is floating around the Net.  It was also
all 6809 assembly, multitasking and multiuser.  Very Unix V4/V5/V6 like in
a number of ways.




-- 
Brad Spencer - brad at anduin.eldar.org - KC8VKS
http://anduin.eldar.org  - & -  http://anduin.ipv6.eldar.org [IPv6 only]


From dugo at xs4all.nl  Wed May  5 04:26:52 2010
From: dugo at xs4all.nl (Jacob Goense)
Date: Tue, 4 May 2010 20:26:52 +0200
Subject: [TUHS] 386BSD on Bochs & Qemu...
In-Reply-To: <20100426032303.GC15137@dereel.lemis.com>
References: <u2u46b366131004162212lf8852e25uf900b73b040410be@mail.gmail.com>
	<130de1e4e4162da466b3dc04bbc53c70.squirrel@webmail.xs4all.nl>
	<20100425022942.GA15137@dereel.lemis.com>
	<5a7c3451f8bbb8efaaa6b9c809214a55.squirrel@webmail.xs4all.nl>
	<20100426032303.GC15137@dereel.lemis.com>
Message-ID: <97c86725a66a6581c02553876124c880.squirrel@webmail.xs4all.nl>

On Mon, April 26, 2010 05:23 "Greg 'groggy' Lehey" <grog at lemis.com> wrote:
> OK,  I mounted the CD-ROM and looked for copyright statements.  The
> only one I found was /COPYRGHT.TXT, a modified 8 paragraph BSD license
> with \r\n line delimiters, attached.
>
> Arguably it only applies to the OS sources, but it's the only license
> I can see.

Indeed, it can be argued that the statement refers to 386BSD Release 1.0
sec, not the 386BSD Reference CD-ROM as a whole, but it is hard to draw
a line, if any.

>  It probibits commercial distributions, but the important
> clause from our point of view is:
>
>    * 5. Non-commercial distribution of the complete source and/or binary
>    *	release at no charge to the user (such as from an official Internet
>    *	archive site) is permitted.
>
> I was going to take out just the source tree, but another clause
> states:
>
>    * 7. Non-commercial and/or commercial distribution of an incomplete,
>    *	altered, or otherwise modified source and/or binary release is not
>    *	permitted.

Same issue here, would excluding eg. .book & .articles make the Release
incomplete or not?

> Since this is the only copyright statement, I assume that this means I
> can put up the entire CD image, but not just part of it, so that's
> what I've done.  It's at http://www.lemis.com/grog/src/386BSD-1.0.bz2 .

The only thing I could find that makes some sort of demarcation is the
installer, which leaves some bits behind when doing a full install.

Without vehement arguing from stakeholders or copyright lawyers to the
contrary I assume the same and thank you for putting it up there.


/Jacob




From dugo at xs4all.nl  Wed May  5 05:37:15 2010
From: dugo at xs4all.nl (Jacob Goense)
Date: Tue, 4 May 2010 21:37:15 +0200
Subject: [TUHS] 386BSD on Bochs & Qemu...
In-Reply-To: <n2o46b366131004252222r20610f3bk29ef343230347be0@mail.gmail.com>
References: <u2u46b366131004162212lf8852e25uf900b73b040410be@mail.gmail.com>
	<130de1e4e4162da466b3dc04bbc53c70.squirrel@webmail.xs4all.nl>
	<20100425022942.GA15137@dereel.lemis.com>
	<5a7c3451f8bbb8efaaa6b9c809214a55.squirrel@webmail.xs4all.nl>
	<20100426032303.GC15137@dereel.lemis.com>
	<n2o46b366131004252222r20610f3bk29ef343230347be0@mail.gmail.com>
Message-ID: <69d171f5fd5a71aebb54fec721835024.squirrel@webmail.xs4all.nl>

On Mon, April 26, 2010 07:22 "Jason Stevens" <neozeed at gmail.com> wrote:
> Well I've been able to find this much out...
>
> The CD has some kind of weird 'live' CD filesystem to it... It would
> seem that 386BSD 1.0 demanded you have an Adaptec 1542 controller
> hooked up, and with special roms & whatnot it could 'boot' from the
> CD...
> Needless to say, this predates anything like IDE CDROM's or or what
> most emulators will emulate.

That "Bootable CD" button on the CD cover is just a marketing fact
AFAICT. I don't have the foggiest how that was done in the pre eltorito
days on an x86.

> That being said, they did include the 'boot' program which is touched
> on in the magazine series, as a MS-DOS bootloader.

Mock code aside, no source code for boot.exe. No source code for the
install program and no Tiny 386BSD Release 1.0 boot floppy image on the
CD. I can't get my head around why they bothered to document to the
point where you can almost port BSD to a Commodore 64 and leave these
bits out.

> So I've just slapped together a MS-DOS floppy, with the boot & 386bsd
> kernel and tried it on on Qemu, to an early kernel panic.

After numerous attect vectors I took the MS-DOS5 floppy route w/ Qemu
as well, but nothing could get it to boot cdrom. Then in a recalcitrant
mood I stuck the image to -hdb, an empty 1G drive to -hda, booted flop
and gave a BOOT.EXE 386BSD.DDB wd1d at the DOS prompt and booted into
The 386BSD SAMPLER. The installer ran fine as long as it didn't have
to deal with swap space. Not that it left me with a bootable system,
but it's a start. Will document on gunkies.org if I can get things
stable.

> I'll have to test later if it can 'mount' an ISO image that's been
> 'dd''d to a hard disk.....

Skip the dd'ing, Qemu eats the image raw ;)

/Jacob




From claunia at claunia.com  Wed May  5 05:41:22 2010
From: claunia at claunia.com (Natalia Portillo)
Date: Tue, 4 May 2010 20:41:22 +0100
Subject: [TUHS] 386BSD on Bochs & Qemu...
In-Reply-To: <69d171f5fd5a71aebb54fec721835024.squirrel@webmail.xs4all.nl>
References: <u2u46b366131004162212lf8852e25uf900b73b040410be@mail.gmail.com>
	<130de1e4e4162da466b3dc04bbc53c70.squirrel@webmail.xs4all.nl>
	<20100425022942.GA15137@dereel.lemis.com>
	<5a7c3451f8bbb8efaaa6b9c809214a55.squirrel@webmail.xs4all.nl>
	<20100426032303.GC15137@dereel.lemis.com>
	<n2o46b366131004252222r20610f3bk29ef343230347be0@mail.gmail.com>
	<69d171f5fd5a71aebb54fec721835024.squirrel@webmail.xs4all.nl>
Message-ID: <3144E4A4-EF81-4DD5-8F44-C66367616EC0@claunia.com>


El 04/05/2010, a las 20:37, Jacob Goense escribió:

> On Mon, April 26, 2010 07:22 "Jason Stevens" <neozeed at gmail.com> wrote:
>> Well I've been able to find this much out...
>> 
>> The CD has some kind of weird 'live' CD filesystem to it... It would
>> seem that 386BSD 1.0 demanded you have an Adaptec 1542 controller
>> hooked up, and with special roms & whatnot it could 'boot' from the
>> CD...
>> Needless to say, this predates anything like IDE CDROM's or or what
>> most emulators will emulate.
> 
> That "Bootable CD" button on the CD cover is just a marketing fact
> AFAICT. I don't have the foggiest how that was done in the pre eltorito
> days on an x86.

SCSI HBAs with integrated boot firmware in BIOS compatible way (that is, trapping INT 13h) can be used to boot ANYTHING in SCSI that behaves like a random access block device.

That means, floppies, LS-120, ZIP, hard disks, CD-ROMs.

From reed at reedmedia.net  Thu May  6 06:55:24 2010
From: reed at reedmedia.net (Jeremy C. Reed)
Date: Wed, 5 May 2010 15:55:24 -0500 (CDT)
Subject: [TUHS] wrong HISTORY in 4.4BSD manual pages?
Message-ID: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>

I was looking at several NetBSD manual pages and saw that some HISTORY 
sections had wrong .Bx or BSD reference like:

HISTORY
     The xstr command appeared in 3.0BSD.

I looked at a few and saw this was in 4.4BSD manual pages. By the way, 
when were these history sections added? (They aren't in 4.2BSD manual 
pages. I should look at 4.3 before asking ...)

I didn't see any that refered to original Berkeley UNIX Software Tape 
nor 2BSD.

But from looking at the 1BSD and 2BSD, I see:

apropos was in 2bsd

colcrt was in 1bsd but not in 2bsd
even though 2BSD iul, soelim, and ssp manuals referenced it
(why missing from 2BSD?)

colrm was in 1bsd but not in 2bsd

csh was in 2bsd (even 1bsd referenced the upcoming "csh")

ctags was in 2bsd, but as a shell script using ed

expand was in 1bsd and 2bsd

finger was in 2bsd

fmt was in 2bsd

from was in 2bsd

head was in 1bsd and 2bsd

lock was in 2bsd

last was in 1bsd and 2bsd

mkstr was in 1bsd and 2bsd

msgs was in 2bsd

printenv was in 2bsd

soelim was in first BSD and 2BSD

tset was in 1bsd and 2bsd

w was in 2bsd as finger -sf

whatis was in 2bsd

whereis was in 2bsd

xstr was in 2bsd

lastlog file format was in 1bsd and 2bsd (?? maybe different format??)

Any comments on the above?

Or is this simply because "2BSD" is not a operating system release per 
se, so "3.0BSD" is correct?

But this makes me wonder if my 2BSD versions are newer than first 
2BSD, so really 3BSD is correct for some of this.

I was going to ask a NetBSD list about this to fix these histories, but 
decided to consult TUHS instead. Okay to change history to fix history? 
:)


From reed at reedmedia.net  Thu May  6 11:40:12 2010
From: reed at reedmedia.net (Jeremy C. Reed)
Date: Wed, 5 May 2010 20:40:12 -0500 (CDT)
Subject: [TUHS] wrong HISTORY in 4.4BSD manual pages?
In-Reply-To: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>
References: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>
Message-ID: <alpine.NEB.2.01.1005052030470.17000@t1.m.reedmedia.net>

> Or is this simply because "2BSD" is not a operating system release per 
> se, so "3.0BSD" is correct?
> 
> But this makes me wonder if my 2BSD versions are newer than first 
> 2BSD, so really 3BSD is correct for some of this.

I was asked off list what version of 2BSD as it may have been the 4.x 
stuff backported. So it is the 2bsd.tar.gz from the minnie.tuhs.org 
archive (which is near identical to the spencer_2bsd.tar.gz). Its TAPE 
file says: May 10, 1979. The READ_ME says: Thu Apr 19 23:25:43 PST 1979. 
And don't see any date stamps included in any files newer than May 1979. 
(So not a backport since is 8 months older than 3BSD and 17 months older 
than 4BSD.)


From lm at bitmover.com  Thu May  6 13:02:47 2010
From: lm at bitmover.com (Larry McVoy)
Date: Wed, 5 May 2010 20:02:47 -0700
Subject: [TUHS] wrong HISTORY in 4.4BSD manual pages?
In-Reply-To: <alpine.NEB.2.01.1005052030470.17000@t1.m.reedmedia.net>
References: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>
	<alpine.NEB.2.01.1005052030470.17000@t1.m.reedmedia.net>
Message-ID: <20100506030247.GI23511@bitmover.com>

2.9BSD, as I recall, was the last release for PDP-11's.  Again, as I 
recall, it did a pile of work to take advantage of the larger address
space (I think there was 64KI and 64KD).

I don't recall every seeing a 3BSD release.

On Wed, May 05, 2010 at 08:40:12PM -0500, Jeremy C. Reed wrote:
> > Or is this simply because "2BSD" is not a operating system release per 
> > se, so "3.0BSD" is correct?
> > 
> > But this makes me wonder if my 2BSD versions are newer than first 
> > 2BSD, so really 3BSD is correct for some of this.
> 
> I was asked off list what version of 2BSD as it may have been the 4.x 
> stuff backported. So it is the 2bsd.tar.gz from the minnie.tuhs.org 
> archive (which is near identical to the spencer_2bsd.tar.gz). Its TAPE 
> file says: May 10, 1979. The READ_ME says: Thu Apr 19 23:25:43 PST 1979. 
> And don't see any date stamps included in any files newer than May 1979. 
> (So not a backport since is 8 months older than 3BSD and 17 months older 
> than 4BSD.)
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs

-- 
---
Larry McVoy                lm at bitmover.com           http://www.bitkeeper.com


From cowan at ccil.org  Thu May  6 15:56:59 2010
From: cowan at ccil.org (John Cowan)
Date: Thu, 6 May 2010 01:56:59 -0400
Subject: [TUHS] wrong HISTORY in 4.4BSD manual pages?
In-Reply-To: <20100506030247.GI23511@bitmover.com>
References: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>
	<alpine.NEB.2.01.1005052030470.17000@t1.m.reedmedia.net>
	<20100506030247.GI23511@bitmover.com>
Message-ID: <20100506055659.GA1342@mercury.ccil.org>

Larry McVoy scripsit:                                                       

> 2.9BSD, as I recall, was the last release for PDP-11's.

2.9BSD was the first 2.x release to have a kernel; it contained
a backport of 4.1BSD.  Earlier 2.x releases, like 1BSD, were just
overlays on AT&T Unix.  The current PDP-11 release, however, is 2.11BSD.
It's still being worked on: 2.11.447 was released on New Year's Day, 2009.

> Again, as I recall, it did a pile of work to take advantage of the
> larger address space (I think there was 64KI and 64KD).           

Correct.  That, plus a lot of in-memory overlays, was why the backport
was possible at all.

> I don't recall every seeing a 3BSD release.

3BSD was released for the VAX at the end of 1979, containing the first
Berkeley 32-bit kernel and ports of 2.xBSD userland.  It was superseded
by 4BSD (later called 4.0BSD) in October 1980.  The term 5BSD was avoided
by 4.1BSD and later releases to avoid confusion with System V.

-- 
On the Semantic Web, it's too hard to prove     John Cowan    cowan at ccil.org
you're not a dog.  --Bill de hOra               http://www.ccil.org/~cowan


From milov at uwlax.edu  Thu May  6 23:07:49 2010
From: milov at uwlax.edu (Milo Velimirovic)
Date: Thu, 6 May 2010 08:07:49 -0500
Subject: [TUHS] wrong HISTORY in 4.4BSD manual pages?
In-Reply-To: <20100506030247.GI23511@bitmover.com>
References: <alpine.NEB.2.01.1005051543520.17000@t1.m.reedmedia.net>
	<alpine.NEB.2.01.1005052030470.17000@t1.m.reedmedia.net>
	<20100506030247.GI23511@bitmover.com>
Message-ID: <C35E5584-CC10-45E1-B6F6-A56B12728176@uwlax.edu>

2.11BSD is available from the Archives in various forms. (From memory)  
the 11/44 and up, with the exception of the 11/60, had split I/D  
address spaces of 64KBytes each.

[mutters to self... really must resurrect the 11/44 or 11/84.]

On May 5, 2010, at 10:02 PM, Larry McVoy wrote:

> 2.9BSD, as I recall, was the last release for PDP-11's.  Again, as I
> recall, it did a pile of work to take advantage of the larger address
> space (I think there was 64KI and 64KD).
>
> I don't recall every seeing a 3BSD release.
>
> On Wed, May 05, 2010 at 08:40:12PM -0500, Jeremy C. Reed wrote:
>>> Or is this simply because "2BSD" is not a operating system release  
>>> per
>>> se, so "3.0BSD" is correct?
>>>
>>> But this makes me wonder if my 2BSD versions are newer than first
>>> 2BSD, so really 3BSD is correct for some of this.
>>
>> I was asked off list what version of 2BSD as it may have been the 4.x
>> stuff backported. So it is the 2bsd.tar.gz from the minnie.tuhs.org
>> archive (which is near identical to the spencer_2bsd.tar.gz). Its  
>> TAPE
>> file says: May 10, 1979. The READ_ME says: Thu Apr 19 23:25:43 PST  
>> 1979.
>> And don't see any date stamps included in any files newer than May  
>> 1979.
>> (So not a backport since is 8 months older than 3BSD and 17 months  
>> older
>> than 4BSD.)
>> _______________________________________________
>> TUHS mailing list
>> TUHS at minnie.tuhs.org
>> https://minnie.tuhs.org/mailman/listinfo/tuhs
>
> -- 
> ---
> Larry McVoy                lm at bitmover.com           http://www.bitkeeper.com
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs
>

--
Milo Velimirović
Unix Network Administrator - ITS Network Services
608.785.6618 Office -  608.386.2817 Cell
University of Wisconsin - La Crosse
La Crosse, Wisconsin 54601 USA   43 48 48 N 91 13 53 W







From BHuntsman at mail2.cu-portland.edu  Thu May 13 03:03:02 2010
From: BHuntsman at mail2.cu-portland.edu (Benjamin Huntsman)
Date: Wed, 12 May 2010 10:03:02 -0700
Subject: [TUHS] [tuhs] Tenth Edition's Appendix A
Message-ID: <621112A569DAE948AD25CCDCF1C07533299927@dolly.ntdom.cupdx>

By some chance, does anyone have Appendix A from the man pages/docs from Tenth Edition?

Thanks!!


