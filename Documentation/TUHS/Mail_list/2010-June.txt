From random832 at fastmail.us  Thu Jun 10 11:30:02 2010
From: random832 at fastmail.us (Random832)
Date: Wed, 09 Jun 2010 21:30:02 -0400
Subject: [TUHS] decoding 1972_stuff/s1-bits
Message-ID: <4C10401A.3000106@fastmail.us>

I'm sorry if this has been done before, but I could find no indication 
this was the case in the unix archive itself or in any of the months I 
checked in the mailing list.

I have figured out what the first 50176 bytes of s1-bits are.

It is an "INIT Tape" as described in Dennis_v1 Boot Procedures(VII) and 
Dennis_v3 bproc.8. It is apparently contemporary with s2 [all of the 
files on it /etc/getty /bin/ls and so on match up exactly with their 
counterparts on s2], and this would seem to make s2 the "/bin-/etc-/lib 
tape" described in bproc.8.

Also - is there any other known copy of the "bos" bootloader? I'm 
partway through hand-disassembling the one on the tape.

Anyone know how to get SIMH to send ^J for return?

Anyway - with the KE-11A enabled you need to use "d system sr" to set 
the switch register [which must be set to 1 to cold boot, 73700 to come 
up in single user mode, and any random value other than the special ones 
0 57500 10 20 40 1 2 to come up normally]

The RF disk is not even large enough to contain the whole s2 tape, and 
while even v1 supported mountable filesystems, there is no mkfs or mount 
on the tapes.


From reed at reedmedia.net  Thu Jun 24 10:37:03 2010
From: reed at reedmedia.net (Jeremy C. Reed)
Date: Wed, 23 Jun 2010 19:37:03 -0500 (CDT)
Subject: [TUHS] use Berkeley Unix prior to 1982?
Message-ID: <alpine.NEB.2.01.1006231933120.18580@t1.m.reedmedia.net>

If you used BSD before 1982, please let me know. Off-list is fine. I 
have a few questions to ask to help provide examples in a history book I 
am writing. Thanks.



