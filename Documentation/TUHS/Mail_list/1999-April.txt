From tfb at tfeb.org  Fri Apr  2 18:03:23 1999
From: tfb at tfeb.org (Tim Bradshaw)
Date: Fri, 2 Apr 1999 09:03:23 +0100 (BST)
Subject: Old (7th ed 32v?) manuals
Message-ID: <199904020803.JAA15445@lostwithiel.tfeb.org>

I have a set of printed (decent quality) manuals which were
distributed with 32bit machines (HLH Orions) which ran BSD in later
life (I have BSD manuals for them).  These ones must be from some
early port of <something>, perhaps 32v or I'm not sure what.  They're
dated 1979 and seem to describe some definitely pre-BSD Unix. As you
can probably tell I haven't been through them in detail or I'd have
more info.

I don't really want them as they're 4 folders which don't actually
define any system I have (unless I succeed in finding a free PDP11 of
reasonable physical size in the UK...) and really I have too much
stuff already...  But it seems a shame to just throw them out.  Is it
worth trying to preserve such things?  Could anyone offer them a home.
I can post them in the UK, and abroad if it's not too savagely
expensive.

--tim



From johnh at psych.usyd.edu.au  Wed Apr 28 17:50:18 1999
From: johnh at psych.usyd.edu.au (johnh at psych.usyd.edu.au)
Date: Wed, 28 Apr 1999 17:50:18 +1000 (EST)
Subject: Yet another PDP-11 web page
Message-ID: <199904280750.RAA26882@psychwarp.psych.usyd.edu.au>


I have created 'yet another PDP-11 page', hence forth to be known as YAPP
at :- www.psych.usyd.edu.au/pdp-11

I have covered all the production PDP-11's with a slant to earlier models,
and lots of images.

Enjoy.


From tfb at tfeb.org  Fri Apr  2 18:03:23 1999
From: tfb at tfeb.org (Tim Bradshaw)
Date: Fri, 2 Apr 1999 09:03:23 +0100 (BST)
Subject: Old (7th ed 32v?) manuals
Message-ID: <199904020803.JAA15445@lostwithiel.tfeb.org>

I have a set of printed (decent quality) manuals which were
distributed with 32bit machines (HLH Orions) which ran BSD in later
life (I have BSD manuals for them).  These ones must be from some
early port of <something>, perhaps 32v or I'm not sure what.  They're
dated 1979 and seem to describe some definitely pre-BSD Unix. As you
can probably tell I haven't been through them in detail or I'd have
more info.

I don't really want them as they're 4 folders which don't actually
define any system I have (unless I succeed in finding a free PDP11 of
reasonable physical size in the UK...) and really I have too much
stuff already...  But it seems a shame to just throw them out.  Is it
worth trying to preserve such things?  Could anyone offer them a home.
I can post them in the UK, and abroad if it's not too savagely
expensive.

--tim



From johnh at psych.usyd.edu.au  Wed Apr 28 17:50:18 1999
From: johnh at psych.usyd.edu.au (johnh at psych.usyd.edu.au)
Date: Wed, 28 Apr 1999 17:50:18 +1000 (EST)
Subject: Yet another PDP-11 web page
Message-ID: <199904280750.RAA26882@psychwarp.psych.usyd.edu.au>


I have created 'yet another PDP-11 page', hence forth to be known as YAPP
at :- www.psych.usyd.edu.au/pdp-11

I have covered all the production PDP-11's with a slant to earlier models,
and lots of images.

Enjoy.


