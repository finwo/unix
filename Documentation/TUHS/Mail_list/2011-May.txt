From neozeed at gmail.com  Mon May  2 04:38:27 2011
From: neozeed at gmail.com (Jason Stevens)
Date: Sun, 1 May 2011 14:38:27 -0400
Subject: [TUHS] Unix v6 running on a javascript pdp-11
Message-ID: <BANLkTikNDfWRWbOHTn4RuCWiYFwK6jfU-A@mail.gmail.com>

I thought you guys may enjoy this...

http://aiju.de/code/pdp11/

It's a PDP-11 with a teletype console, and a RK05 with Unix v6.

Because browsers are weird, here is some of the keycommands...


DEL is the interrupt key (^C on modern *nix), Pause the quit key (^\
or ^L) and PrtScr is EOF (^D).

Jason


From wkt at tuhs.org  Mon May  2 09:49:42 2011
From: wkt at tuhs.org (Warren Toomey)
Date: Mon, 2 May 2011 09:49:42 +1000
Subject: [TUHS] Unix v6 running on a javascript pdp-11
In-Reply-To: <BANLkTikNDfWRWbOHTn4RuCWiYFwK6jfU-A@mail.gmail.com>
References: <BANLkTikNDfWRWbOHTn4RuCWiYFwK6jfU-A@mail.gmail.com>
Message-ID: <20110501234942.GA11267@minnie.tuhs.org>

On Sun, May 01, 2011 at 02:38:27PM -0400, Jason Stevens wrote:
> I thought you guys may enjoy this...
> 
> http://aiju.de/code/pdp11/
> It's a PDP-11 with a teletype console, and a RK05 with Unix v6.

That's pretty good!

Also, stty -lcase if you want lowercase output.

Cheers,
	Warren


From neozeed at gmail.com  Sun May  8 23:42:14 2011
From: neozeed at gmail.com (Jason Stevens)
Date: Sun, 8 May 2011 09:42:14 -0400
Subject: [TUHS] DEMOS (Soviet Unix) for the PDP-11
Message-ID: <BANLkTinemFi=9bzmOtdF2aK9q2G7u970sg@mail.gmail.com>

I figured you guys may get a kick out of this...

You can download a 'ready to run' version here:

http://code.google.com/p/vak-opensource/downloads/detail?name=dvk-demos.zip&can=2&q=

Included is a modified version of SIMH's PDP-11 emulator with a new
disk controller.  Binaries are included for both Linux & Win32.
Source to his additions are here:
http://vak-opensource.googlecode.com/svn/trunk/bk/simh-dvk/


And the source to version 2.2 of Demos has been provided here:

ftp://ftp.besm6.org/pub/archives/d22.tar.gz

Oh control+n lets you input in russian, and control+o lets you input in english.

>From what I understand DEMOS is derived from 2.9 BSD..

I don't speak Russian and what I end up doing is cutting & pasting
into google translate... For some really bizarre translations.

Demos is Copyright 1991 by Research Institute "Scientific Centre", lab 462/2.


From steve at quintile.net  Wed May 25 08:19:00 2011
From: steve at quintile.net (Steve Simon)
Date: Tue, 24 May 2011 23:19:00 +0100
Subject: [TUHS] dwb picasso
Message-ID: <da8e0069c5214faca66eb95a7138be2b@quintile.net>

Anyone remember picasso, a vector graphics GUI app
that generated pic(1) source?

I know nothing of it, anyone got a screenshot even?

is the source available?

was its frontend X11 or blit terminal?

Was it related to the blit cip and xcip tools
or are they a different genus?

Thanks for any info.

-Steve


From jaapna at xs4all.nl  Wed May 25 18:56:55 2011
From: jaapna at xs4all.nl (Jaap Akkerhuis)
Date: Wed, 25 May 2011 10:56:55 +0200
Subject: [TUHS] dwb picasso
In-Reply-To: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
Message-ID: <95C6906E-0267-43F1-9325-2D953B3235D8@xs4all.nl>


On May 25, 2011, at 0:19, Steve Simon wrote:

> Anyone remember picasso, a vector graphics GUI app
> that generated pic(1) source?

Yup. Pic & PostScript (and a mix of the two).

> 
> I know nothing of it, anyone got a screenshot even?

Screen shots are in the DWB 3.4 distribution I seem to remember.

> 
> is the source available?

Not that I know.

> was its frontend X11 or blit terminal?

It was/is an X11 (Open Look) as far as I remember.

> 
> Was it related to the blit cip and xcip tools
> or are they a different genus?

I believe cip was the original inspiration. 

	jaap



From madcrow.maxwell at gmail.com  Wed May 25 22:58:42 2011
From: madcrow.maxwell at gmail.com (Michael Kerpan)
Date: Wed, 25 May 2011 08:58:42 -0400
Subject: [TUHS] dwb picasso
In-Reply-To: <95C6906E-0267-43F1-9325-2D953B3235D8@xs4all.nl>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
	<95C6906E-0267-43F1-9325-2D953B3235D8@xs4all.nl>
Message-ID: <BANLkTi=CYn740PUSjQz=Yj-1U=XWVG+twA@mail.gmail.com>

On Wed, May 25, 2011 at 4:56 AM, Jaap Akkerhuis <jaapna at xs4all.nl> wrote:
>

> Screen shots are in the DWB 3.4 distribution I seem to remember.

Is the DWB 3.4 stuff available anywhere? IIRC, DWB 3.3 was released
under the CPL, but 3.4 seems to be orphaned/lost.
>> is the source available?
>
> Not that I know.
Actually the core engine (though not the actual GUI app) seems to be
in the CPL release of DWB at http://www2.research.att.com/sw/download/

The manual for Picasso in the docs/ folder seems to make reference to
the Open Look version, but all I can find in terms of source code is
the batch/CLI version.

Mike


From jaapna at xs4all.nl  Thu May 26 02:25:10 2011
From: jaapna at xs4all.nl (Jaap Akkerhuis)
Date: Wed, 25 May 2011 18:25:10 +0200
Subject: [TUHS] dwb picasso
In-Reply-To: <BANLkTi=CYn740PUSjQz=Yj-1U=XWVG+twA@mail.gmail.com>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
	<95C6906E-0267-43F1-9325-2D953B3235D8@xs4all.nl>
	<BANLkTi=CYn740PUSjQz=Yj-1U=XWVG+twA@mail.gmail.com>
Message-ID: <6BC2EC7E-B9D6-49D2-B18F-00E5ABF77DD3@xs4all.nl>


>> Screen shots are in the DWB 3.4 distribution I seem to remember.

> 
> Is the DWB 3.4 stuff available anywhere? IIRC, DWB 3.3 was released
> under the CPL, but 3.4 seems to be orphaned/lost.


I have a hard copy of the DWB 3.4 manuals. The whole distribution was for sale.
Googling for it I found <http://www.faqs.org/faqs/text-faq/section-28.html>.
I don't think that the email or phone still works.

>>> is the source available?
>> 
>> Not that I know.
> Actually the core engine (though not the actual GUI app) seems to be
> in the CPL release of DWB at http://www2.research.att.com/sw/download/
> 
> The manual for Picasso in the docs/ folder seems to make reference to
> the Open Look version, but all I can find in terms of source code is
> the batch/CLI version.

It was in definitely in DWB 3.4. Nils-Peter Nelson and his crew produced it.

	jaap

From cyrille.lefevre-lists at laposte.net  Thu May 26 05:56:58 2011
From: cyrille.lefevre-lists at laposte.net (Cyrille Lefevre)
Date: Wed, 25 May 2011 21:56:58 +0200
Subject: [TUHS] dwb picasso
In-Reply-To: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
Message-ID: <4DDD5F0A.2060009@laposte.net>


Le 25/05/2011 00:19, Steve Simon a écrit :

well, too big message held on queue canceled, repost w/ attachment.

> Anyone remember picasso, a vector graphics GUI app
> that generated pic(1) source?

picasso seems to be part of the DocumentWorkBench which is now freely 
available from AT&T Software Technology :

http://www2.research.att.com/~gsf/cgi-bin/download.cgi?action=list&name=dwb

here is the manual page :

         PICASSO(1)           UNIX System V (DWB 3.2)           PICASSO(1)



         NAME
              picasso - a line drawing program

         SYNOPSIS
              picasso [ -bsize -Fpath -Tfonttables -Ipath -ln -Mn -mmargin
              -pmxn -txL ]  [ - ]  [ files ]

         DESCRIPTION
              picasso is a processor for a pic-like drawing language that
              produces PostScript output.  By default, this output is
              scaled to fit an 8 by 10 inch print area, and centered on
              the page.
...

compiles well under cygwin w/ the following patch :

ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2

Regards,

Cyrille Lefevre
-- 
mailto:Cyrille.Lefevre-lists at laposte.net





From gregg.drwho8 at gmail.com  Thu May 26 06:01:58 2011
From: gregg.drwho8 at gmail.com (Gregg Levine)
Date: Wed, 25 May 2011 16:01:58 -0400
Subject: [TUHS] dwb picasso
In-Reply-To: <4DDD5F0A.2060009@laposte.net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
	<4DDD5F0A.2060009@laposte.net>
Message-ID: <BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>

On Wed, May 25, 2011 at 3:56 PM, Cyrille Lefevre
<cyrille.lefevre-lists at laposte.net> wrote:
>
> Le 25/05/2011 00:19, Steve Simon a écrit :
>
> well, too big message held on queue canceled, repost w/ attachment.
>
>> Anyone remember picasso, a vector graphics GUI app
>> that generated pic(1) source?
>
> picasso seems to be part of the DocumentWorkBench which is now freely
> available from AT&T Software Technology :
>
> http://www2.research.att.com/~gsf/cgi-bin/download.cgi?action=list&name=dwb
>
> here is the manual page :
>
>        PICASSO(1)           UNIX System V (DWB 3.2)           PICASSO(1)
>
>
>
>        NAME
>             picasso - a line drawing program
>
>        SYNOPSIS
>             picasso [ -bsize -Fpath -Tfonttables -Ipath -ln -Mn -mmargin
>             -pmxn -txL ]  [ - ]  [ files ]
>
>        DESCRIPTION
>             picasso is a processor for a pic-like drawing language that
>             produces PostScript output.  By default, this output is
>             scaled to fit an 8 by 10 inch print area, and centered on
>             the page.
> ...
>
> compiles well under cygwin w/ the following patch :
>
> ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
>
> Regards,
>
> Cyrille Lefevre
> --
> mailto:Cyrille.Lefevre-lists at laposte.net
>
>
>
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs
>

Hello!
Well right now the server is still throwing an HTTP-1.1/500 Error message.

-----
Gregg C Levine gregg.drwho8 at gmail.com
"This signature fought the Time Wars, time and again."


From madcrow.maxwell at gmail.com  Thu May 26 11:44:25 2011
From: madcrow.maxwell at gmail.com (Michael Kerpan)
Date: Wed, 25 May 2011 21:44:25 -0400
Subject: [TUHS] dwb picasso
In-Reply-To: <4DDD5F0A.2060009@laposte.net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>
	<4DDD5F0A.2060009@laposte.net>
Message-ID: <BANLkTikgCdtoMQyvuF47Pk8sOej07eYxng@mail.gmail.com>

On Wed, May 25, 2011 at 3:56 PM, Cyrille Lefevre
<cyrille.lefevre-lists at laposte.net> wrote:
>
> Le 25/05/2011 00:19, Steve Simon a écrit :
>
> well, too big message held on queue canceled, repost w/ attachment.
>
>> Anyone remember picasso, a vector graphics GUI app
>> that generated pic(1) source?
>
> picasso seems to be part of the DocumentWorkBench which is now freely
> available from AT&T Software Technology :
>
> http://www2.research.att.com/~gsf/cgi-bin/download.cgi?action=list&name=dwb
>
> here is the manual page :
>
>        PICASSO(1)           UNIX System V (DWB 3.2)           PICASSO(1)
>
>
>
>        NAME
>             picasso - a line drawing program
>
>        SYNOPSIS
>             picasso [ -bsize -Fpath -Tfonttables -Ipath -ln -Mn -mmargin
>             -pmxn -txL ]  [ - ]  [ files ]
>
>        DESCRIPTION
>             picasso is a processor for a pic-like drawing language that
>             produces PostScript output.  By default, this output is
>             scaled to fit an 8 by 10 inch print area, and centered on
>             the page.
> ...
>
> compiles well under cygwin w/ the following patch :
>
> ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2

This looks like the batch version of Picasso rather than the
interactive version. Clearly the basic engine is there, but it looks
like the fun part (the GUI) is MIA.

Mike


From cyrille.lefevre-lists at laposte.net  Thu May 26 17:36:34 2011
From: cyrille.lefevre-lists at laposte.net (Cyrille Lefevre)
Date: Thu, 26 May 2011 09:36:34 +0200
Subject: [TUHS] dwb picasso
In-Reply-To: <BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>	<4DDD5F0A.2060009@laposte.net>
	<BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>
Message-ID: <4DDE0302.9070506@laposte.net>


Le 25/05/2011 22:01, Gregg Levine a écrit :
> On Wed, May 25, 2011 at 3:56 PM, Cyrille Lefevre wrote:
>> compiles well under cygwin w/ the following patch :
>>
>> ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
>
> Hello!
> Well right now the server is still throwing an HTTP-1.1/500 Error message.

my bad, its http and not ftp...

http://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2

Regards,

Cyrille Lefevre
-- 
mailto:Cyrille.Lefevre-lists at laposte.net




From hansolofalcon at att.net  Sun May 29 13:24:43 2011
From: hansolofalcon at att.net (Gregg C Levine)
Date: Sat, 28 May 2011 23:24:43 -0400
Subject: [TUHS] dwb picasso
In-Reply-To: <4DDE0302.9070506@laposte.net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>	<4DDD5F0A.2060009@laposte.net>	<BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>
	<4DDE0302.9070506@laposte.net>
Message-ID: <000301cc1daf$f49d3ed0$ddd7bc70$@net>

Hello!
When I replied to your posting, via my other e-mail address, several days
ago.... I meant not your site. I meant the one from AT&T. I kept checking it
each time the subject came up. Each time it registered a complaint.

---
Gregg C Levine hansolofalcon at att.net
“This signature is not the same one. Move along! Move along!”

> -----Original Message-----
> From: tuhs-bounces at minnie.tuhs.org [mailto:tuhs-bounces at minnie.tuhs.org]
On
> Behalf Of Cyrille Lefevre
> Sent: Thursday, May 26, 2011 3:37 AM
> To: Gregg Levine
> Cc: tuhs at minnie.tuhs.org
> Subject: Re: [TUHS] dwb picasso
> 
> 
> Le 25/05/2011 22:01, Gregg Levine a écrit :
> > On Wed, May 25, 2011 at 3:56 PM, Cyrille Lefevre wrote:
> >> compiles well under cygwin w/ the following patch :
> >>
> >> ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
> >
> > Hello!
> > Well right now the server is still throwing an HTTP-1.1/500 Error
message.
> 
> my bad, its http and not ftp...
> 
> http://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
> 
> Regards,
> 
> Cyrille Lefevre
> --
> mailto:Cyrille.Lefevre-lists at laposte.net
> 
> 
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs



From hansolofalcon at att.net  Sun May 29 13:37:11 2011
From: hansolofalcon at att.net (Gregg C Levine)
Date: Sat, 28 May 2011 23:37:11 -0400
Subject: [TUHS] dwb picasso
In-Reply-To: <000301cc1daf$f49d3ed0$ddd7bc70$@net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>	<4DDD5F0A.2060009@laposte.net>	<BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>	<4DDE0302.9070506@laposte.net>
	<000301cc1daf$f49d3ed0$ddd7bc70$@net>
Message-ID: <001201cc1db1$b2012c60$16038520$@net>

Hello!
It must have been my believing in things that most people don't want to
believe are a certainty, because the site is now up. I've downloaded them.
Both your patch and the package.

Now what do I do next with the patch? Extract it and apply it to the source
code? And then follow the usual steps for building stuff on Cygwin I should
imagine.(?)

---
Gregg C Levine hansolofalcon at att.net
“This signature is not the same one. Move along! Move along!”


> -----Original Message-----
> From: tuhs-bounces at minnie.tuhs.org [mailto:tuhs-bounces at minnie.tuhs.org]
On
> Behalf Of Gregg C Levine
> Sent: Saturday, May 28, 2011 11:25 PM
> To: tuhs at minnie.tuhs.org; 'Cyrille Lefevre'
> Subject: Re: [TUHS] dwb picasso
> 
> Hello!
> When I replied to your posting, via my other e-mail address, several days
> ago.... I meant not your site. I meant the one from AT&T. I kept checking
it
> each time the subject came up. Each time it registered a complaint.
> 
> ---
> Gregg C Levine hansolofalcon at att.net
> “This signature is not the same one. Move along! Move along!”
> 
> > -----Original Message-----
> > From: tuhs-bounces at minnie.tuhs.org [mailto:tuhs-bounces at minnie.tuhs.org]
> On
> > Behalf Of Cyrille Lefevre
> > Sent: Thursday, May 26, 2011 3:37 AM
> > To: Gregg Levine
> > Cc: tuhs at minnie.tuhs.org
> > Subject: Re: [TUHS] dwb picasso
> >
> >
> > Le 25/05/2011 22:01, Gregg Levine a écrit :
> > > On Wed, May 25, 2011 at 3:56 PM, Cyrille Lefevre wrote:
> > >> compiles well under cygwin w/ the following patch :
> > >>
> > >> ftp://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
> > >
> > > Hello!
> > > Well right now the server is still throwing an HTTP-1.1/500 Error
> message.
> >
> > my bad, its http and not ftp...
> >
> > http://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.patch.bz2
> >
> > Regards,
> >
> > Cyrille Lefevre
> > --
> > mailto:Cyrille.Lefevre-lists at laposte.net
> >
> >
> > _______________________________________________
> > TUHS mailing list
> > TUHS at minnie.tuhs.org
> > https://minnie.tuhs.org/mailman/listinfo/tuhs
> 
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs



From cyrille.lefevre-lists at laposte.net  Mon May 30 07:55:30 2011
From: cyrille.lefevre-lists at laposte.net (Cyrille Lefevre)
Date: Sun, 29 May 2011 23:55:30 +0200
Subject: [TUHS] dwb picasso
In-Reply-To: <001201cc1db1$b2012c60$16038520$@net>
References: <da8e0069c5214faca66eb95a7138be2b@quintile.net>	<4DDD5F0A.2060009@laposte.net>	<BANLkTin0qLd5McMe2ALGTfb2E3a8ExHBog@mail.gmail.com>	<4DDE0302.9070506@laposte.net>
	<000301cc1daf$f49d3ed0$ddd7bc70$@net>
	<001201cc1db1$b2012c60$16038520$@net>
Message-ID: <4DE2C0D2.4000903@laposte.net>


Le 29/05/2011 05:37, Gregg C Levine a écrit :
>
> Hello!
> It must have been my believing in things that most people don't want to
> believe are a certainty, because the site is now up. I've downloaded them.
> Both your patch and the package.

right

> Now what do I do next with the patch? Extract it and apply it to the source
> code? And then follow the usual steps for building stuff on Cygwin I should
> imagine.(?)

well, things changes since last year...

wget http://cyrillelefevre.free.fr/dwb/dwb.1993-02-04.2011-05-29.patch.bz2

mkdir dwb.1993-02-04
cd dwb.1993-02-04
tar xf ../dwb.1993-02-04.tgz
bzcat ../dwb.1993-02-04.patch.bz2 | patch -p1
make -j 1 -f dwb.mk all install

should make it ?

results in /opt/dwb

PS : under cygwin, patch come from patchutils

Regards,

Cyrille Lefevre
-- 
mailto:Cyrille.Lefevre-lists at laposte.net




