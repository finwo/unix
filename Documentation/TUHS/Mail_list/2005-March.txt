From jkunz at unixag-kl.fh-kl.de  Tue Mar  1 06:50:58 2005
From: jkunz at unixag-kl.fh-kl.de (Jochen Kunz)
Date: Mon, 28 Feb 2005 21:50:58 +0100
Subject: [TUHS] Sprite OS trouble.
Message-ID: <20050228215058.7fb645e6.jkunz@unixag-kl.fh-kl.de>

Hi.

I am trying to bring up a Sprite cluster. [1]

I was able to get the demo system running on a SPARCstation 1+ by dd-ing
the boot image to a disk. Now I wane label an additional disk, make LFS,
... make the new disk bootable to get more free disk space then I have
on the premade boot image. But I can't get a label on the disk with
labeldisk nor did I succeed using fsmakeprompt. The later crashes...

Next step is to bring a SPARCstation 2, an IPX and two ELCs into the
cluster.

Is there someone out there with Sprite experience who can help me?

Additionaly I was not able to get the PMAX image to work on my
DECstation 5000/240 nor my DECstation 3100. Any ideas? Do I really need
a DECstation 5000/200 for this?

[1] http://www.cs.berkeley.edu/Research/Projects/sprite/
A mixed architecture, distributed single system image OS capable of
process migration that presented the cluster to a user as a single,
large multiprocessor machine. Pmake and LFS (Log-Structured File System)
originate from Sprite.
-- 


tschüß,
       Jochen

Homepage: http://www.unixag-kl.fh-kl.de/~jkunz/


From newsham at lava.net  Thu Mar  3 04:21:22 2005
From: newsham at lava.net (Tim Newsham)
Date: Wed, 2 Mar 2005 08:21:22 -1000 (HST)
Subject: [TUHS] Lion's
Message-ID: <Pine.BSI.4.61.0503020820300.9136@malasada.lava.net>

For those looking for a copy of Lion's:
http://www.lulu.com/content/99701

From alex at lava-net.com  Thu Mar  3 09:51:41 2005
From: alex at lava-net.com (alex at lava-net.com)
Date: Wed, 2 Mar 2005 18:51:41 -0500
Subject: [TUHS] ultrix and y2k
Message-ID: <20050302235141.GA1686@lava-net.com>

Hi,
Since the ultrix-4.2 source was "liberated" has anyone atempted to fix 
some of the y2k issues? (I'd like to run it on some vaxen I have access 
to and it's kinda useless without y2k support).
Does this source even compile?
thanks.


From wes.parish at paradise.net.nz  Thu Mar  3 19:52:08 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Thu, 03 Mar 2005 22:52:08 +1300
Subject: [TUHS] ultrix and y2k
In-Reply-To: <20050302235141.GA1686@lava-net.com>
References: <20050302235141.GA1686@lava-net.com>
Message-ID: <200503032252.08628.wes.parish@paradise.net.nz>

On Thu, 03 Mar 2005 12:51, alex at lava-net.com wrote:
> Hi,
> Since the ultrix-4.2 source was "liberated"

"liberated"?  Where at?

Thanks

Wesley Parish

> has anyone atempted to fix 
> some of the y2k issues? (I'd like to run it on some vaxen I have access
> to and it's kinda useless without y2k support).
> Does this source even compile?
> thanks.
>
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> http://minnie.tuhs.org/mailman/listinfo/tuhs

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From asmodai at ao.mine.nu  Mon Mar 14 17:42:01 2005
From: asmodai at ao.mine.nu (Paul Ward)
Date: Mon, 14 Mar 2005 08:42:01 +0100
Subject: [TUHS] Intergraph 2700
Message-ID: <888557590.20050314084201@ao.mine.nu>

Hello all,

A friend of mine has liberated an old Intergraph 2700 workstation from
a basement.  The system looks to be in fine condition and boots up to
multiuser+X.

However, there appears to be a problem.

I am not sure whether the problem is hardware or software, but the
digitizer puck seems to have stopped sending anything back to the
workstation.

I have to say that I am not 100% sure how Intergraph digitizers are
wired up -- the cable from the digitizer is run via a split cable, so
one can be wired up to the mouse port, and the other to the RS232
port, right now I have plugged both in.

Is there anyone with Intergraph experience who can enlighten me in
regards to the cables, or maybe some diagnostics that can be done on
the puck?

The puck is a 12-button grey creature, and the digitizer is a Kurta
XLC (sorry, I am not infront of the system right now, and can't
remember the model number).

Another thing to note is that I can't recover the root password
without some sort of pointer device input, and thus cannot log in to
diagnose any possible software problems.

-- 
Best regards,
 Paul                          mailto:asmodai at ao.mine.nu
 http://ao.mine.nu/       (NeXTmail) mailto:nextmail at ao.mine.nu
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 456 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050314/520ad508/attachment.sig>

From Robertdkeys at aol.com  Wed Mar 16 09:33:11 2005
From: Robertdkeys at aol.com (Robertdkeys at aol.com)
Date: Tue, 15 Mar 2005 18:33:11 EST
Subject: [TUHS] Got old MVII critter and wanna bring up a 4.3BSD family
	thingy.....but no tape..
Message-ID: <1a7.33a2c76b.2f68cab7@aol.com>

Having fallen into that trap of another one dollar vax.....of the
Qbus variety, and, wanting to try to bring up another round of
some sort of 4.3BSD related thingy, but, with no tape drive or
tape cartridges, can anyone come up with a dd'able root image
that is known to work on an MVII critter, AND, have a footprint
of 1mb or less (i.e., a disklabel of only 1mb size or less)?
Sadly, all my tapes have decomposed to dust, and I am down
to one questionable TK50 tape drive.  I can get a boot of the
Quasijarus boot, off a floppy, but it won't read a miniroot that
was dd'd onto a swap partition correctly.  I can boot a NetBSD
1.4.1 and work back to a NetBSD-1.0A which is usable to
unroll file systems onto a drive, but, it won't boot the kernel
correctly into a 4.3BSDish system.  I still need the correct
boot blocks and boots.  I did try an image from vaxpower's
root, and that booted and ran mostly OK, but, it had a label
set up for a 4 gig drive, and my controller has early enough
proms that it won't handle anything larger than a 1 gb drive.
I tried a couple of other images from here and there, but the
boot blocks are not quite right to run on my vax.  So, anyone
have handy a dd'able root with boot blocks, set up for a 1gb
or smaller drive, that is known to work on an MVII critter for
some flavor of 4.3BSD (4.3, Tahoe, Reno, Quasijarus) that
I could use to get my machine up?  The only constraints
on the image are that it must have a label set up for a 1gb
or smaller drive, and it must have a usable ftp from usr/bin
dropped into bin, so I can ftp in the rest of the system after
booting up the root image.

One other thought might be to create a bootable dd image with
a root only system that contains all the requisite user bits on
say a 100mb or so root file system.  That is non-standard,
from the traditionalists point of view, but, at least the system
would come up essentially running, and complete.  It could
then be partitioned and cloned off onto a standardly partitioned
drive, as a next step.

Like a fool, the last time I ran the system, about 5 years ago,
I religiously copied off all the bits onto cd, except for a dd'able
boot/root image... drat!  Kick, Kick, Kick... etc.....(:+{{.....

Any pointers to such an image, or anyone that has a system
up that might create me such an image, would be greatly
appreciated.  With the EOL of many tapes at hand, I suspect
that it may be the only good way to bring up a 4.3BSDish
system on a Qbus box, and we might should save up a such
usable image in the TUHS archives.

Thanks

Bob Keys
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050315/4b09c00b/attachment.html>

From tih at hamartun.priv.no  Wed Mar 16 18:41:19 2005
From: tih at hamartun.priv.no (Tom Ivar Helbekkmo)
Date: Wed, 16 Mar 2005 09:41:19 +0100
Subject: [TUHS] Got old MVII critter and wanna bring up a 4.3BSD family
	thingy.....but no tape..
In-Reply-To: <1a7.33a2c76b.2f68cab7@aol.com> (Robertdkeys@aol.com's message of
	"Tue, 15 Mar 2005 18:33:11 EST")
References: <1a7.33a2c76b.2f68cab7@aol.com>
Message-ID: <m27jk8nftc.fsf@barsoom.hamartun.priv.no>

Robertdkeys at aol.com writes:

> Any pointers to such an image, or anyone that has a system up that
> might create me such an image, would be greatly appreciated.

Why not install a virtual system, using simh on some modern Unix, and
then use that to create the particular image you want?

-tih
-- 
Popularity is the hallmark of mediocrity.  --Niles Crane, "Frasier"

From wes.parish at paradise.net.nz  Sun Mar 27 19:10:55 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Sun, 27 Mar 2005 21:10:55 +1200
Subject: [TUHS] JPLDIS - still extant?
Message-ID: <200503272110.55929.wes.parish@paradise.net.nz>

I was looking around for stuff to play with on FreeDOS and I thought, do they 
have any DBMS yet?

So I googled for (dbase public domain) and got something about the aforesaid 
JPLDIS, the Jet Propulsion Laboratory's Display and Information System.

Anyone got any ideas of its continued existence?  (It's written in Fortran, I 
judge, and I think there are a few PD Fortrans out there that could be used 
to reanimate it. ;)

Thanks

Wesley Parish
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From kstailey at yahoo.com  Sun Mar 27 23:05:38 2005
From: kstailey at yahoo.com (Kenneth Stailey)
Date: Sun, 27 Mar 2005 05:05:38 -0800 (PST)
Subject: [TUHS] 
	Why you should not be wasting electricity powering old computers
Message-ID: <20050327130538.51547.qmail@web50602.mail.yahoo.com>

http://kimaura.com/peakoil/peakoil-56k.ram


From norman at nose.cs.utoronto.ca  Mon Mar 28 04:10:10 2005
From: norman at nose.cs.utoronto.ca (Norman Wilson)
Date: Sun, 27 Mar 2005 13:10:10 -0500
Subject: [TUHS]
	Why you should not be wasting electricity powering old computers
Message-ID: <20050327181116.8A22F14@minnie.tuhs.org>

Kenneth Stailey:

  http://kimaura.com/peakoil/peakoil-56k.ram

=======

Because it's too much bother to set up Real Audio on the
ancient or unusual operating systems that are the reason
such old computers are interesting?

In any case, let he who has gone the longest without
owning or operating an internal-combustion engine cast
the first stone.  (Mind your head, Jim.)

Norman Wilson
Toronto ON

From txomsy at yahoo.es  Mon Mar 28 08:01:08 2005
From: txomsy at yahoo.es (Jose R. Valverde)
Date: Mon, 28 Mar 2005 00:01:08 +0200
Subject: [TUHS] JPLDIS - still extant?
In-Reply-To: <200503272110.55929.wes.parish@paradise.net.nz>
References: <200503272110.55929.wes.parish@paradise.net.nz>
Message-ID: <20050328000108.79246e22.txomsy@yahoo.es>

On Sun, 27 Mar 2005 21:10:55 +1200
Wesley Parish <wes.parish at paradise.net.nz> wrote:
> I was looking around for stuff to play with on FreeDOS and I thought, do they 
> have any DBMS yet?
> 
> So I googled for (dbase public domain) and got something about the aforesaid 
> JPLDIS, the Jet Propulsion Laboratory's Display and Information System.
> 
> Anyone got any ideas of its continued existence?  (It's written in Fortran, I 
> judge, and I think there are a few PD Fortrans out there that could be used 
> to reanimate it. ;)

Sure, Open WatCom, offers (if I remember well) C, C++ and fortran compilers
for DOS (among others) derived from traditional WatCom compilers (see
www.openwatcom.com).

As for databases... you may be interested in looking over other links. Your
initial query was too narrow (public domain << open source). See e.g.

	http://www.iam.unibe.ch/~scg/Archive/Software/FreeDB/FreeDB.list.html
	http://www.swen.uwaterloo.ca/~mrbannon/cs798/

or search for 'open source dbms' or 'open source database'.

				j
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050328/05d5c66d/attachment.sig>

From kstailey at yahoo.com  Tue Mar 29 00:59:32 2005
From: kstailey at yahoo.com (Kenneth Stailey)
Date: Mon, 28 Mar 2005 06:59:32 -0800 (PST)
Subject: [TUHS]  Why you should not be wasting electricity powering old
	computers
In-Reply-To: 6667
Message-ID: <20050328145932.13069.qmail@web50604.mail.yahoo.com>


--- Kenneth Stailey <kstailey at yahoo.com> wrote:
> http://kimaura.com/peakoil/peakoil-56k.ram

Good Old 7-bit ASCII transcript of the dialog in that streaming video.

Doesn't have any of the charts so there are places you will find you can't
follow it since Roscoe is just point to a chart.

Excerpts from the US Congressional Record follow:

OIL PRODUCTION -- (House of Representatives - March 14, 2005)

The SPEAKER pro tempore. Under a previous order of the House, the gentleman
from Maryland (Mr. Gilchrest) is recognized for 5 minutes.

Mr. GILCHREST. Mr. Speaker, in just a few minutes, the gentleman from Maryland
(Mr. Bartlett) will address the House for some period of time talking about
energy sources, oil in particular, and the fact that many experts say that oil
production, especially in the United States, but actually throughout the world,
oil production of conventional oil under current patterns is expected to grow
at a rate much faster, that means the use of oil by the world community is
supposed to grow much faster than oil discovery production.

[Time: 19:45]

What is clear, because we are not sure exactly when that peak will come in oil
production, some say it is peaking right now, some say it will peak in 10
years, the amount of oil we get out of the ground will exceed the demand; but
what is clear is that at some point in this century, world oil production will
peak and then begin to decline. There is uncertainty about the date because
many countries that produce oil do not provide credible data on how big their
reserves are.

But more uncertainty calls for more caution, not less; and caution in this case
means working to develop alternatives. When production of conventional oil
peaks, we can expect a large increase in the price up to the price of the
substitutes, whether so-called unconventional oil or renewable fuels. Although
increasing domestic production may ease oil dependence slightly, the United
States is only 3 percent of the world's estimated oil reserves and uses 25
percent of the world's oil.

I want to explain just from the perspective of the United States the huge
increase in energy demand in the last century. I am going to use the word
``quadrillion.'' Quadrillion is a number. If I put 1 followed by 15 zeroes, I
have the number quadrillion. To measure energy use in a country, we use BTUs,
British thermal units. A new furnace, whether oil or natural gas, you see the
BTU to determine how much energy it is going to use. When you use BTUs to
determine how much energy a country uses, you use a short term for quadrillion
called ``quads.''

In 1910, the United States used 7 quads of BTUs. That is 7 quadrillion BTUs. In
1950, the United States used 35 quadrillion BTUs. In 2005, the United States
uses 100 quadrillion BTUs, and we are accelerating that. We are increasing
demand for oil for our energy needs. The world right now, 2005, uses 345
quadrillion BTUs, an enormous amount of energy.

We know today that our appliances, whether a washing machine, a refrigerator or
dishwasher, we know they are much more efficient than they ever were, certainly
20, 30, 40 years ago; and yet we are using more electricity, not less. We know
that automobiles and trucks and our transportation is much more efficient than
it was 20 years ago, and yet the demand is increasing. We burn more coal, more
natural gas. Each home, as efficient as each home is today, burns much more oil
and electricity because of the demand on energy needs. We are not decreasing by
getting efficient. Because our demand is greater, we are using more and more.

The question is if we are increasing demand and production is going to peak now
or in the next decade or two and our production goes down while the demand goes
up, especially with oil reserves, are we at the early stages of the twilight
for oil as an energy source? And if we are, what do we do?

Well, the gentleman from Maryland (Mr. Bartlett) will speak on a number of
aspects of oil production decline. We will talk much further about the details
of the solution to the problems of

[Page: H1409]

our energy decline, but I want to close with two last things: How do we harness
a new alternative energy source and make it replace what we have been using for
more than 2 centuries? How do we do that? We do it with initiative, ingenuity,
intellect, vision, and leadership. 

[portions deleted]

OIL DEMAND -- (House of Representatives - March 14, 2005)

MR. ROSCOE BARTLETT

A couple of Congresses ago, I was privileged to chair the Energy Subcommittee
on Science. One of the first things I wanted to do was to determine the
dimensions of the problem. We held a couple of hearings and had the world
experts in. Surprisingly from the most pessimistic to the most optimistic,
there was not much deviation in what the estimate is as to what the known
reserves are out there. It is about 1,000 gigabarrels. That sounds like an
awful lot of oil. But when you divide into that the amount of oil which we use,

[Page: H1410]

about 20 million barrels a day, and the amount of oil the rest of the world
uses, about 60 million barrels a day, as a matter of fact, the total now is a
bit over the 80 million that those two add up to. About 83 1/2 , I think. If
you divide that into the 1,000 gigabarrels, you come out at about 40 years of
oil remaining in the world. That is pretty good. Because up until the Carter
years, during the Carter years, in every decade we used as much oil as had been
used in all of previous history. Let me repeat that, because that is startling.
In every decade, we used as much oil as had been used in all of previous
history. The reason for that, of course, was that we were on the upward side of
this bell curve. The bell curve for usage, only part of it is shown on this
chart. That is the green one down here, the bell curve for usage. Notice that
we are out here now about 2005. Where is it going? The Energy Information
Agency says that we are going to keep on using more oil. This green line just
going up and up and up is a projection of the Energy Information Agency. But
that cannot be true. That cannot be true for a couple of reasons. We peaked in
our discovery of oil way back here in the late sixties, about 1970. In our
country it peaked much earlier than that, by the way. But the world is
following several years behind us. And the area under this red curve must be
the same as the area under the green curve. You cannot pump any more oil than
you have found, quite obviously. If you have not found it, you cannot pump it.
If you were to extend this on out where they have extended their green line,
even if it turned down right there at the end of that green line, the area
under the green curve is going to be very much larger than the area under the
red curve. That just cannot be. We will see in some subsequent charts that we
probably have reached peak oil.

Let me mention that M. King Hubbert looked at the world situation. He was
joined by another scientist, Colin Campbell, who is still alive, an American
citizen who lives in Scotland. Using M. King Hubbert's predictive techniques,
oil was predicted to reach a maximum in about 1995, without perturbations. But
there were some perturbations. One of the perturbations was 1973, the Arab oil
embargo. Other perturbations were the oil price shocks and a worldwide
recession that reduced the demand for oil. And so the peak that might have
occurred in 1995 will occur later. How much later? That is what we are looking
at this evening. There is a lot of evidence that suggests that if not now, then
very quickly we should see world production of oil peak.

[portions deleted]

What now? Where do we go now? One observer, Matt Savinar, who has thoroughly
researched the options, and this is not the most optimistic assessment, by the
way, but may be somewhat realistic, he starts out by saying, Dear Readers,
civilization as we know it is coming to an end soon. I hope not. This is not
the wacky proclamation of a doomsday cult, apocalypse Bible sect or conspiracy
theory society. Rather, it is a scientific conclusion of the best-paid, most
widely respected geologists, physicists and investment bankers in the world.
These are rational, professional, conservative individuals who are absolutely
terrified by the phenomenon known as global peak oil.

[portions deleted]

From wes.parish at paradise.net.nz  Thu Mar 31 17:25:28 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Thu, 31 Mar 2005 19:25:28 +1200
Subject: [TUHS] Not strictly TUHS, but
Message-ID: <200503311925.28644.wes.parish@paradise.net.nz>

wotthehell, I'm going to ask anyway.

Soemtime during the late 1980s, Clarkson U., came out with a GPLed MS-DOS word 
processor package called Galahad, released under the Galahad Public License, 
which is a rebadged GNU Emacs Public License.  I've sent them the CS 
Professor an email requesting the source.

I was wondering if anyone on this list might have the sources, because as yet 
I've had no reply.

Thanks

Wesley Parish
-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

