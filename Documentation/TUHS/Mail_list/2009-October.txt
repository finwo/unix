From Andy.Cowley at uwe.ac.uk  Mon Oct 12 02:57:40 2009
From: Andy.Cowley at uwe.ac.uk (Andy Cowley)
Date: Sun, 11 Oct 2009 17:57:40 +0100
Subject: [TUHS] Bell and EUUG tapes - who could read?
Message-ID: <4AD20E84.2000501@uwe.ac.uk>

Dear All,

  I have a number of old Unix™ ½ inch reel to reel tapes. I would dearly love to transfer
the contents, if readable, to a more modern medium. If anyone has the facility to do this
please contact me. I'm located in the United Kingdom.

They are 800 or 1600 BPI, 9 track and, I suspect, NRZI encoded. They should be compatible
with DEC reel-to-reel drives, probably most others.

They contain various PDP/LSI-11 versions of AT&T Bell Research Laboratories UNIX™
Edition 6 and 7. The Bell tapes (1, 2 and 3) are for PDP-11/45 with other drivers.
The EUUG (European Unix Users Group) tape has kernels for various LSI-11 models
including LSI-11/23. It also contains a large number of contributed utilities.

There is more info at http://www.andycowley.com/Unix/tapes.html

best

Andy Cowley



This email was independently scanned for viruses by McAfee anti-virus software and none were found
_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs

From neozeed at gmail.com  Tue Oct 27 03:29:25 2009
From: neozeed at gmail.com (Jason Stevens)
Date: Mon, 26 Oct 2009 13:29:25 -0400
Subject: [TUHS] UNIX/32v & speak....
Message-ID: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>

I was going to upload the various man pages of 32/V so I'd have a 'nice'
collection of them, and to help with my eventual conversion of the help text
into RTF for a windows helpfile when I was reading through the number
command...

http://gunkies.org/wiki/32v_1m_number


Number copies the standard input to the standard output, changing each
> decimal number to a fully spelled out version. Punctuation is added to make
> the output sound well when played through speak(1).


So it seems that VAX's could do audio at some point?  Does anyone know how
it worked?  Naturally the speak command seems to be missing, but it does
seem very interesting...!

I wonder if this was the start of the UNIX/IVR relationship....?

Thanks!

Jason
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20091026/7d4a2726/attachment.html>
-------------- next part --------------
_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs

From carl.lowenstein at gmail.com  Tue Oct 27 03:44:02 2009
From: carl.lowenstein at gmail.com (Carl Lowenstein)
Date: Mon, 26 Oct 2009 10:44:02 -0700
Subject: [TUHS] UNIX/32v & speak....
In-Reply-To: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>
References: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>
Message-ID: <5904d5730910261044g524b1c11i71f34a1de5c1cab6@mail.gmail.com>

On Mon, Oct 26, 2009 at 10:29 AM, Jason Stevens <neozeed at gmail.com> wrote:
> I was going to upload the various man pages of 32/V so I'd have a 'nice'
> collection of them, and to help with my eventual conversion of the help text
> into RTF for a windows helpfile when I was reading through the number
> command...
> http://gunkies.org/wiki/32v_1m_number
>
>> Number copies the standard input to the standard output, changing each
>> decimal number to a fully spelled out version. Punctuation is added to make
>> the output sound well when played through speak(1).
>
> So it seems that VAX's could do audio at some point?  Does anyone know how
> it worked?  Naturally the speak command seems to be missing, but it does
> seem very interesting...!

See "Votrax" in Wikipedia for more information.  "Vocal division of
Federal Screw Works".
A speech synthesizer, predecessor of Digital's DECtalk.

> I wonder if this was the start of the UNIX/IVR relationship....?

What's IVR?

    carl
-- 
    carl lowenstein         marine physical lab     u.c. san diego
                                                 clowenstein at ucsd.edu
_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs


From cowan at ccil.org  Tue Oct 27 03:40:19 2009
From: cowan at ccil.org (John Cowan)
Date: Mon, 26 Oct 2009 13:40:19 -0400
Subject: [TUHS] UNIX/32v & speak....
In-Reply-To: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>
References: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>
Message-ID: <20091026174019.GF22428@mercury.ccil.org>

Jason Stevens scripsit:

> So it seems that VAX's could do audio at some point?  Does anyone know how
> it worked?  Naturally the speak command seems to be missing, but it does
> seem very interesting...!

I think the speaker was an RS-232 device.

-- 
De plichten van een docent zijn divers,         John Cowan
die van het gehoor ook.                         cowan at ccil.org
      --Edsger Dijkstra                         http://www.ccil.org/~cowan
_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs


From IanK at vulcan.com  Tue Oct 27 07:51:22 2009
From: IanK at vulcan.com (Ian King)
Date: Mon, 26 Oct 2009 14:51:22 -0700
Subject: [TUHS] UNIX/32v & speak....
In-Reply-To: <20091026174019.GF22428@mercury.ccil.org>
References: <46b366130910261029g52e47beavf1011a123d5839c4@mail.gmail.com>
	<20091026174019.GF22428@mercury.ccil.org>
Message-ID: <FF6AB92D97A23A409701CDBF66F03FCD37AEFB68@505fuji>

There's also a Qbus device that was intended for IVR systems, called DECtalk.  That was hosted on a MicroVAX system.  (I have the voice synth card but not the D/A card....)  -- isk 

> -----Original Message-----
> From: tuhs-bounces at minnie.tuhs.org [mailto:tuhs-
> bounces at minnie.tuhs.org] On Behalf Of John Cowan
> Sent: Monday, October 26, 2009 10:40 AM
> To: Jason Stevens
> Cc: tuhs at tuhs.org
> Subject: Re: [TUHS] UNIX/32v & speak....
> 
> Jason Stevens scripsit:
> 
> > So it seems that VAX's could do audio at some point?  Does anyone
> know how
> > it worked?  Naturally the speak command seems to be missing, but it
> does
> > seem very interesting...!
> 
> I think the speaker was an RS-232 device.
> 
> --
> De plichten van een docent zijn divers,         John Cowan
> die van het gehoor ook.                         cowan at ccil.org
>       --Edsger Dijkstra
> http://www.ccil.org/~cowan
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> https://minnie.tuhs.org/mailman/listinfo/tuhs

_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs


From agrier at poofygoof.com  Thu Oct 29 05:28:05 2009
From: agrier at poofygoof.com (Aaron J. Grier)
Date: Wed, 28 Oct 2009 12:28:05 -0700
Subject: [TUHS] history of termios?
Message-ID: <20091028192805.GL2041@arwen.poofy.goof.com>

before I do some code spelunking, does anyone here know the history of
termios?  I've been doing some serial programming recently and wondering
how things got to the way they are...  why is VTIME an inter-character
timer instead of a timer for an entire VMIN block, for instance?

-- 
  Aaron J. Grier | "Not your ordinary poofy goof." | agrier at poofygoof.com
_______________________________________________
TUHS mailing list
TUHS at minnie.tuhs.org
https://minnie.tuhs.org/mailman/listinfo/tuhs


