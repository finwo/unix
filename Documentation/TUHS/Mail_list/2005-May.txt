From pete at dunnington.u-net.com  Fri May 20 08:16:25 2005
From: pete at dunnington.u-net.com (Pete Turnbull)
Date: Thu, 19 May 2005 23:16:25 +0100 (BST)
Subject: [pups] S.R.Bourne and the shell
Message-ID: <10505192316.ZM13193@mindy.dunnington.u-net.com>

I've found a copy of the book "The Unix System" and I've had the
paper "An Introduction to the UNIX Shell" for 20 years, but I thought
I'd seen a slim book by S.R.Bourne called "The UNIX Shell" or something
of the sort.  However, Google, Bibliofind, etc have turned up nothing;
has anyone seen this, or is my memory at fault once more?

-- 
Pete						Peter Turnbull
						Network Manager
						University of York

From andrew.lynch at knuut.de  Thu May 19 20:11:26 2005
From: andrew.lynch at knuut.de (Andrew Lynch)
Date: Thu, 19 May 2005 12:11:26 +0200
Subject: [pups] S.R.Bourne and the shell
In-Reply-To: pups-request@minnie.tuhs.org
        "PUPS Digest, Vol 12, Issue 1" (May 20,  9:09am)
References: <20050519230906.2F8E83D9@minnie.tuhs.org>
Message-ID: <1050519121126.ZM897@Sindy.generic>

On May 19,  11:16pm, Pete Turnbull wrote:
>
> I've found a copy of the book "The Unix System" and I've had the
> paper "An Introduction to the UNIX Shell" for 20 years, but I thought
> I'd seen a slim book by S.R.Bourne called "The UNIX Shell" or something
> of the sort.  However, Google, Bibliofind, etc have turned up nothing;
> has anyone seen this, or is my memory at fault once more?


Are you possibly thinking of an article that appeared in the 1978 Bell System
Technical Journal?

>From the bibliography in "The UNIX System":

Bourne, S. R.  1978. "UNIX Time-Sharing System: The UNIX Shell". Bell Sys.
Tech. J. 57(6) 1971-90.

The issues of BSTJ that I have seen could be described as slim books - roughly
A5 paperback, around 200 pages.

This article is supposed to have also appeared in Volume 2 of the UNIX
Programmer's Manual - which would imply that it is the same as "An Introduction
to the UNIX Shell" (which is what my 7th Ed Manual contains).

Andrew.

From pete at dunnington.u-net.com  Mon May 23 17:11:49 2005
From: pete at dunnington.u-net.com (Pete Turnbull)
Date: Mon, 23 May 2005 08:11:49 +0100 (BST)
Subject: [pups] S.R.Bourne and the shell
In-Reply-To: "Andrew Lynch" <andrew.lynch@knuut.de>
        "Re: [pups] S.R.Bourne and the shell" (May 19, 12:11)
References: <20050519230906.2F8E83D9@minnie.tuhs.org> 
	<1050519121126.ZM897@Sindy.generic>
Message-ID: <10505230811.ZM20273@mindy.dunnington.u-net.com>

On May 19 2005, 12:11, Andrew Lynch wrote:
> On May 19,  11:16pm, Pete Turnbull wrote:
> >
> > I've found a copy of the book "The Unix System" and I've had the
> > paper "An Introduction to the UNIX Shell" for 20 years, but I
thought
> > I'd seen a slim book by S.R.Bourne called "The UNIX Shell" or
something
> > of the sort.  However, Google, Bibliofind, etc have turned up
nothing;
> > has anyone seen this, or is my memory at fault once more?
>
> Are you possibly thinking of an article that appeared in the 1978
Bell System
> Technical Journal?

I probably am...

> This article is supposed to have also appeared in Volume 2 of the
UNIX
> Programmer's Manual - which would imply that it is the same as "An
Introduction
> to the UNIX Shell" (which is what my 7th Ed Manual contains).

Yes, that's in mine too.  I just thought the "slim book" had slightly
more in it, but perhaps that's because it has smaller pages, and
therefore the artice is spread over more of them.  I think our library
has a copy of the BSTJ, so I can check.

Someone emailed me off-list with a URL for an HTML-ised version;
thanks, but I have a real 7th Edition Manual with that paper, as well
as the troff source on my (original) 7th Edition distro.

-- 
Pete						Peter Turnbull
						Network Manager
						University of York

From ryansdoherty at yahoo.com  Tue May 31 01:44:56 2005
From: ryansdoherty at yahoo.com (Ryan Doherty)
Date: Mon, 30 May 2005 08:44:56 -0700 (PDT)
Subject: [pups] Looking for PDP-11 Newsweek ad circa 1981
In-Reply-To: <mailman.0.1117467705.65629.pups@minnie.tuhs.org>
Message-ID: <20050530154456.48975.qmail@web50201.mail.yahoo.com>

Hello,
 
I am looking for a copy (electronic or paper) of the digital PDP-11 advertisement that appeared in Newsweek in the early 1980s.  The ad states "Who needs a computer with thousands of software programs?", with sketches of people explaining how the computer can be used in their field (ie. "I need it for word processing", "designing bridges", "collecting the bills", etc).  Do you know where I can find a copy of it?
 
Sincerely,

Ryan Doherty

 
 

		
---------------------------------
Do You Yahoo!?
 Yahoo! Small Business - Try our new Resources site!
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050530/2825d179/attachment.html>

From jpeek at jpeek.com  Sun May  1 04:28:51 2005
From: jpeek at jpeek.com (Jerry Peek)
Date: Sat, 30 Apr 2005 11:28:51 -0700
Subject: [TUHS] Mention TUHS in Linux Magazine (US)? 
In-Reply-To: <200504301931.53182.wes.parish@paradise.net.nz> 
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
	<200504301931.53182.wes.parish@paradise.net.nz>
Message-ID: <14431.1114885731@jpeek.com>

On 30 April 2005 at 19:31, Wesley Parish <wes.parish at paradise.net.nz> wrote:
> And, FWIW, in one of the few GNUs Bulletins I actually have received, courtesy
> of the FSF, RMS (I think) was advising that with the dropping price in
> memory, GNU hackers could do without worrying about memory size, when it came
> to replicating Unix utilities..

That's been on my mind as I thought back to my days on VAXen
with 100 users logged on and a load average of 30.  Back then,
efficient programming was so very important.  Now, when the GNU
"cp" has more than 20 options -- and some of those with several
possible arguments -- one side of me thinks how bloated the GNU
utilities seem.  But, on the other hand, one of the things I'm
doing in this series of columns is to compare "how we used to
do it" vs. the usefulness of some of the new features.  For
instance, back then we could copy a directory tree recursively
with "tar" or "find", carefully handling devices and etc. along
the way.  Now we can do the same thing by typing a cp command
with a couple of options.  With powerful machines on our desks,
which sort of "efficiency" do we want these days?

I'm not trying to answer that question!  I'm trying to show
things in a balanced way and leave it to the reader to decide.
This has been debated and discussed so much over the years
that I can't shed any new light on it.  I just want readers to
keep it in mind, think about where we've been and where we are.

Jerry

From wes.parish at paradise.net.nz  Sun May  1 18:21:50 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Sun, 01 May 2005 20:21:50 +1200
Subject: [TUHS] Mention TUHS in Linux Magazine (US)?
In-Reply-To: <14431.1114885731@jpeek.com>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
 <200504301931.53182.wes.parish@paradise.net.nz> <14431.1114885731@jpeek.com>
Message-ID: <200505012021.50557.wes.parish@paradise.net.nz>

Again, FWIW, I've successfully - with the expenditure of a good long period of 
time - transferred an 80GB file system from one hard drive to another using 
the GNU cp - I didn't read enough of the man page though, and missed out on 
the -p switch to preserve my ownership on the files - I had to return the 
entire 80 GB to my ownership manually ... ;)  God, I felt like the Prize 
Dork!

But it did the massive job without blinking an eyelid, over the three or so 
hours it took to do it.  It's good stuff.  Just massive itself.

Wesley Parish

On Sun, 01 May 2005 06:28, Jerry Peek wrote:
> On 30 April 2005 at 19:31, Wesley Parish <wes.parish at paradise.net.nz> wrote:
> > And, FWIW, in one of the few GNUs Bulletins I actually have received,
> > courtesy of the FSF, RMS (I think) was advising that with the dropping
> > price in memory, GNU hackers could do without worrying about memory size,
> > when it came to replicating Unix utilities..
>
> That's been on my mind as I thought back to my days on VAXen
> with 100 users logged on and a load average of 30.  Back then,
> efficient programming was so very important.  Now, when the GNU
> "cp" has more than 20 options -- and some of those with several
> possible arguments -- one side of me thinks how bloated the GNU
> utilities seem.  But, on the other hand, one of the things I'm
> doing in this series of columns is to compare "how we used to
> do it" vs. the usefulness of some of the new features.  For
> instance, back then we could copy a directory tree recursively
> with "tar" or "find", carefully handling devices and etc. along
> the way.  Now we can do the same thing by typing a cp command
> with a couple of options.  With powerful machines on our desks,
> which sort of "efficiency" do we want these days?
>
> I'm not trying to answer that question!  I'm trying to show
> things in a balanced way and leave it to the reader to decide.
> This has been debated and discussed so much over the years
> that I can't shed any new light on it.  I just want readers to
> keep it in mind, think about where we've been and where we are.
>
> Jerry
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> http://minnie.tuhs.org/mailman/listinfo/tuhs

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

From j at cnb.uam.es  Mon May  9 18:41:10 2005
From: j at cnb.uam.es (J. R. Valverde)
Date: Mon, 9 May 2005 10:41:10 +0200
Subject: [TUHS] Sad news from IBM...
In-Reply-To: <200505012021.50557.wes.parish@paradise.net.nz>
References: <200504291759.j3THxL110157@opihi.ucsd.edu>
	<200504301931.53182.wes.parish@paradise.net.nz>
	<14431.1114885731@jpeek.com>
	<200505012021.50557.wes.parish@paradise.net.nz>
Message-ID: <20050509104110.76e8cfe3.j@cnb.uam.es>

I was reading Groklaw yesterday night when I came across this. It is a
very sad thought to know that possibly tons of old/ancient code is being
dumped in the trash bin.

More so now since the advent of software patents: it may become very
difficult to avoid a patent on a re-invention of the wheel if previous
knowledge has been dumped.

OK, the quote. It is from "the Todd Shaughnessy affidavit [PDF] from IBM 
that Magistrate Judge Brooke Wells requested they file when they turned 
over all the code and paperwork to SCO":

	28. As I have noted above, IBM does not maintain revision control 
	information for AIX source code pre-dating 1991. To the extent that 
	any code for the AIX operating system (that did not duplicate the 
	code already being produced in CMVC) was found during the search 
	described in Paragraph 26-27 above, it was produced. Paragraphs 
	29-31 below describe additional search efforts IBM undertook to 
	locate pre-1991 versions of AIX code. No versions of AIX pre-dating 
	1991 were found.

	29. In the 1980s and early 1990s, IBM prepared vital records backups 
	of AIX source code and transferred them to a remote storage location. 
	At some point in the 1990s, the AIX vital records tapes were transferred 
	to Austin, Texas. In late 2000, the tapes were determined to be obsolete, 
	and were not retained.

	30. The AIX development organization contacted other IBM employees who 
	were known or believed to have been involved with the development or 
	product release of AIX versions prior to 1991. In addition, IBM 
	managers and attorneys asked current members of the AIX development 
	organization whether they were aware of the location of pre-1991 
	releases of AIX source code. No one asked was aware of any remaining 
	copies of pre-1991 AIX source code.

Perhaps we should do something to raise awareness about the relevance of 
legacy (not only UNIX) source code. And in any case, it is a pity that all
that historical information had been lost forever.

I have always complained about this, and consider it the biggest drawback of
closed proprietary source code: it is OK that law protects developer interests 
with the goal of promoting innovation and the public benefit at large. But it
is a lose for everybody whenever any such "protected" code is dumped into the
bin banning anyone else from further benefitting from or exploiting it, and 
opening the road for opportunists to claim they "newly invented" it.

Sic. Sigh.
				j
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 189 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20050509/167fc183/attachment.sig>

From vasco at icpnet.pl  Tue May 17 03:18:37 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Mon, 16 May 2005 19:18:37 +0200
Subject: [TUHS] Location of a Coherent distribution?
Message-ID: <4288D5ED.8050703@icpnet.pl>

Hi Paul,

I have seen in the tuhs list, that You are going to upload 4.2.10 sources .
Can You tell me , where did You get if from ?
Or You mean sources of 4.2.14 once available at demon or mayn ?

Or Did You get it from MWC ?

Regards
Andrzej
P.S It does not mean I am interested . I have authoried sources of 4.2.10.



From vasco at icpnet.pl  Fri May 20 18:27:17 2005
From: vasco at icpnet.pl (Andrzej Popielewicz)
Date: Fri, 20 May 2005 10:27:17 +0200
Subject: [TUHS] Coherent 128 MB RAM
Message-ID: <428D9F65.9040605@icpnet.pl>

Hi,
I have added test version of binary kernel for Coherent 4.2.10 with
support for fixed 128 MB RAM(tested on 300 MHZ system).
Check

http://www.staff.amu.edu.pl/~apopiele/embed.html.

I have approval of the owner of Coherent, so it is legal to download and
use this kernel.


Andrzej


From wes.parish at paradise.net.nz  Fri May 20 19:40:48 2005
From: wes.parish at paradise.net.nz (Wesley Parish)
Date: Fri, 20 May 2005 21:40:48 +1200
Subject: [TUHS] Coherent 128 MB RAM
In-Reply-To: <428D9F65.9040605@icpnet.pl>
References: <428D9F65.9040605@icpnet.pl>
Message-ID: <200505202140.48927.wes.parish@paradise.net.nz>

Hi, Andrzej, some time ago on the (now-defunct it seems) Coherent newsgroup, 
you said you were trying to get the Coherent owner to agree to release the 
source under an Open Source license.

Has anything come of that, or is that private, "privileged information"?

Wesley Parish

On Fri, 20 May 2005 20:27, Andrzej Popielewicz wrote:
> Hi,
> I have added test version of binary kernel for Coherent 4.2.10 with
> support for fixed 128 MB RAM(tested on 300 MHZ system).
> Check
>
> http://www.staff.amu.edu.pl/~apopiele/embed.html.
>
> I have approval of the owner of Coherent, so it is legal to download and
> use this kernel.
>
>
> Andrzej
>
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> http://minnie.tuhs.org/mailman/listinfo/tuhs

-- 
Clinersterton beademung, with all of love - RIP James Blish
-----
Mau e ki, he aha te mea nui?
You ask, what is the most important thing?
Maku e ki, he tangata, he tangata, he tangata.
I reply, it is people, it is people, it is people.

