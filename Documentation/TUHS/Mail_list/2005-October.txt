From billcu1 at verizon.net  Sat Oct  8 08:06:53 2005
From: billcu1 at verizon.net (Bill Cunningham)
Date: Fri, 07 Oct 2005 18:06:53 -0400
Subject: [TUHS] ancient unixes
Message-ID: <000e01c5cb8b$6da13020$2f01a8c0@myhome.westell.com>

    Has anyone had the idea to take the ancient unix, at least in spirit
into the modern age?

Bill




From wkt at tuhs.org  Sat Oct  8 09:58:38 2005
From: wkt at tuhs.org (Warren Toomey)
Date: Sat, 8 Oct 2005 09:58:38 +1000
Subject: [TUHS] ancient unixes
In-Reply-To: <000e01c5cb8b$6da13020$2f01a8c0@myhome.westell.com>
References: <000e01c5cb8b$6da13020$2f01a8c0@myhome.westell.com>
Message-ID: <20051007235838.GA72556@minnie.tuhs.org>

On Fri, Oct 07, 2005 at 06:06:53PM -0400, Bill Cunningham wrote:
>     Has anyone had the idea to take the ancient unix, at least in spirit
> into the modern age?

Plan 9?
	Warren


From norman at nose.cs.utoronto.ca  Sat Oct  8 13:28:00 2005
From: norman at nose.cs.utoronto.ca (Norman Wilson)
Date: Fri, 07 Oct 2005 23:28:00 -0400
Subject: [TUHS] ancient unixes
Message-ID: <20051008033237.287F899@minnie.tuhs.org>

Bill Cunningham:

      Has anyone had the idea to take the ancient unix, at least in spirit
  into the modern age?

Warren Toomey:

  Plan 9?

=======

Plan 9 is to UNIX as SVr4.2.2.2.2.2.2 is to Sixth Edition.

If that's the spirit of the modern age, give me the good
old leather-bound days, without all that modern rhythm-
type dancing and hooting and waving.

Norman Wilson
Toronto ON

PS: This message is not intended to supply the minimum
daily requirement of serious thought.  Consult your doctor
or pharmacist, but not the one that just sent you electronic
junk mail or promises to make explicit drugs fast.


From txomsy at yahoo.es  Mon Oct 10 19:37:24 2005
From: txomsy at yahoo.es (Jose R Valverde)
Date: Mon, 10 Oct 2005 11:37:24 +0200 (CEST)
Subject: [TUHS] Ancient unixes
Message-ID: <20051010093724.48531.qmail@web26103.mail.ukl.yahoo.com>

Waddayamean?

I mean: what does it mean to you 'the spirit of
ancient 
Unix'?

If by that you mean the fact that they were simple, 
slim and efficient, doing one simple thing and doing 
it right, you may then consider the effort by 
ast in the 80's with MINIX. OK, it used it's own 
microkernel, but the basic idea is the same... and has

been followed on by Mach, BSD-lites, Flex, MacOS X,
Tru64, Linux on L4, etc...

As a matter of fact I always felt UNIX after v7 got it

wrong: e.g. network data is no longer another stream 
(I'd have loved it to be a file system with
directories 
representing network addresses and ports being files
or 
pipes). Thus, later unices increased complexity by 
abandoning the simlicity of the original design. If 
that is the case, Plan 9 is a good update. And so is 
Inferno.

Actually, I always felt that many additions to UNIX 
might have been better implemented outside the kernel 
if only the kernel had been expanded to allow
user-mode 
expansions. But that's already here with kernel
modules 
in Linux, BSDs, Solaris, etc... which are becoming
more 
and more microkernelized each day. As microkernels 
become bigger :-)

OTOH, if you mean adding 'modern' services, perhaps
QNX 
is doing it with its support for Real-time. Or adding 
dynamic libraries, networking, modern virtual memory 
(beyond swapping), etc... which at the plainest level 
is what more or less likeably all modern UNIX have 
done.

Extending into the future? Distributed computing, 
clusters, etc? Like some commercial UNIX, Amoeba, 
Inferno and the like?

If you only mean resurrecting these ancient UNIX on 
modern hardware, there have been initiatives to
rewrite 
v7 alike systems for other architectures (say OMU,
UZI, 
MINIX, Coherent, Xinu, etc.). But for that you already

have emulators that provide you the original flavor at

even higher speeds in a virtualized environment.

So? waddayamean?

I think the answer to your question is YES! Lots of 
people have tried to improve ancient UNIX more or less

successfully, and many people is still trying, using 
microkernels, no-kernels, adding RT, VM, dynamic 
libraries, kernel modules, etc... Each with their own 
approach.

This said, if I were to pick an initiative that gets 
closest to the wishes of the original designers, that 
should undoubtedly be Plan 9 and its successor, 
Inferno, as they are what the 'Original Designers' 
themselves have done when they tried to repeat it
doing 
it 'right' (or at least better) no matter what my 
personal opinions regarding the issue may be.

Regarding my opinion, yes, I would go for the good old

leather-bound days of IBM mainframes with MVS. (zOS?) 
which oddly enough are finally reaching the rest of us

with Xen and emulators like QEMU. If I were to wish, 
I'd like a no-kernel approach (everything independent,

cooperating, hot-substitutable, fully migratable 
processes) over a virtualizing system that allows me
to 
run several OSs and update/change any OS component on 
the fly without service interruption, and to migrate 
everything between machines on demand ('cos of
overload 
or hw failures or whatever, or just 'cos I wish to). 
Now, _that_ would IMHO be close to ultimate OS design:

something that can always be updated on the fly and
may 
survive any change, something that can adapt and
evolve 
without interruption or even the user noticing. But 
that is a complex enough concept to expect most system

programmers to grasp, let alone sysadmins, programmers

or users not to pervert. Not to talk of salesmen and 
marketroids!

                           j
--
Jose R. Valverde
EMBnet/CNB



		
______________________________________________ 
Renovamos el Correo Yahoo! 
Nuevos servicios, más seguridad 
http://correo.yahoo.es


From johnzulu at yahoo.com  Tue Oct 11 16:17:30 2005
From: johnzulu at yahoo.com (John Chung)
Date: Mon, 10 Oct 2005 23:17:30 -0700 (PDT)
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <mailman.3.1128996000.90839.tuhs@minnie.tuhs.org>
Message-ID: <20051011061730.93702.qmail@web30303.mail.mud.yahoo.com>

There has been a lot of talk about ancient unix
lately. I do know there are quite a few ports for the
ancient unix but the main question is it legal? It is
possible to port and distribute the port without the
warth of the company that owns the IP?

Regards,
John Chung


		
__________________________________ 
Yahoo! Music Unlimited 
Access over 1 million songs. Try it free.
http://music.yahoo.com/unlimited/


From grog at lemis.com  Tue Oct 11 16:32:27 2005
From: grog at lemis.com (Greg 'groggy' Lehey)
Date: Tue, 11 Oct 2005 16:02:27 +0930
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <20051011061730.93702.qmail@web30303.mail.mud.yahoo.com>
References: <mailman.3.1128996000.90839.tuhs@minnie.tuhs.org>
	<20051011061730.93702.qmail@web30303.mail.mud.yahoo.com>
Message-ID: <20051011063227.GB49168@wantadilla.lemis.com>

On Monday, 10 October 2005 at 23:17:30 -0700, John Chung wrote:
> There has been a lot of talk about ancient unix
> lately. I do know there are quite a few ports for the
> ancient unix but the main question is it legal? It is
> possible to port and distribute the port without the
> warth of the company that owns the IP?

Yes, it's legal.  Our good friends Caldera (now known as SCO) released
Ancient UNIX under a BSD-style license a few years back.  See
http://www.lemis.com/grog/UNIX/ for further details.

Greg
--
Finger grog at lemis.com for PGP public key.
See complete headers for address and phone numbers.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 187 bytes
Desc: not available
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20051011/1400c27a/attachment.sig>

From Hellwig.Geisse at mni.fh-giessen.de  Tue Oct 11 16:38:17 2005
From: Hellwig.Geisse at mni.fh-giessen.de (Hellwig.Geisse at mni.fh-giessen.de)
Date: Tue, 11 Oct 2005 08:38:17 +0200 (CEST)
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <20051011061730.93702.qmail@web30303.mail.mud.yahoo.com>
Message-ID: <XFMail.20051011083817.Hellwig.Geisse@mni.fh-giessen.de>

Hi John,

On 11-Oct-2005 John Chung wrote:
> There has been a lot of talk about ancient unix
> lately. I do know there are quite a few ports for the
> ancient unix but the main question is it legal? It is
> possible to port and distribute the port without the
> warth of the company that owns the IP?
> 
> Regards,
> John Chung

it is legal to use, modify, and distribute code
based on UNIX Versions 1..7 and 32V, to the best
of my knowledge; see
http://www.tuhs.org/Archive/Caldera-license.pdf

Regards,
Hellwig Geisse
 
----------------------------------
E-Mail: Hellwig.Geisse at mni.fh-giessen.de
Date: 11-Oct-2005
Time: 08:32:09

This message was sent by XFMail
----------------------------------


From duncangareth at yahoo.co.uk  Tue Oct 11 22:35:44 2005
From: duncangareth at yahoo.co.uk (Duncan Anderson)
Date: Tue, 11 Oct 2005 14:35:44 +0200
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <XFMail.20051011083817.Hellwig.Geisse@mni.fh-giessen.de>
References: <XFMail.20051011083817.Hellwig.Geisse@mni.fh-giessen.de>
Message-ID: <434BB1A0.1070603@yahoo.co.uk>

Hellwig.Geisse at mni.fh-giessen.de wrote:

>Hi John,
>
>On 11-Oct-2005 John Chung wrote:
>  
>
>>There has been a lot of talk about ancient unix
>>lately. I do know there are quite a few ports for the
>>ancient unix but the main question is it legal? It is
>>possible to port and distribute the port without the
>>warth of the company that owns the IP?
>>
>>Regards,
>>John Chung
>>    
>>
>
>it is legal to use, modify, and distribute code
>based on UNIX Versions 1..7 and 32V, to the best
>of my knowledge; see
>http://www.tuhs.org/Archive/Caldera-license.pdf
>
>Regards,
>Hellwig Geisse
>  
>
Does anyone know where and how one obtains the "ancient unix" sources, 
particularly 32V?

Who does one speak to?

regards
Duncan

		
___________________________________________________________ 
How much free photo storage do you get? Store your holiday 
snaps for FREE with Yahoo! Photos http://uk.photos.yahoo.com


From Hellwig.Geisse at mni.fh-giessen.de  Wed Oct 12 00:18:33 2005
From: Hellwig.Geisse at mni.fh-giessen.de (Hellwig.Geisse at mni.fh-giessen.de)
Date: Tue, 11 Oct 2005 16:18:33 +0200 (CEST)
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <434BB1A0.1070603@yahoo.co.uk>
Message-ID: <XFMail.20051011161833.Hellwig.Geisse@mni.fh-giessen.de>

Hi Duncan,

On 11-Oct-2005 Duncan Anderson wrote:
> Does anyone know where and how one obtains the "ancient unix" sources, 
> particularly 32V?
> 
> Who does one speak to?
> 
> regards
> Duncan

the best source I know of is
http://www.tuhs.org/Archive/
(not very surprising, I guess).
In subdirectory VAX/Distributions/32V
you find the file 32v_usr.tar.gz.
Unpack this file, then go to the
subdirectory /usr/src/sys.

Regards,
Hellwig
 
----------------------------------
E-Mail: Hellwig.Geisse at mni.fh-giessen.de
Date: 11-Oct-2005
Time: 16:04:42

This message was sent by XFMail
----------------------------------


From txomsy at yahoo.es  Wed Oct 12 00:26:20 2005
From: txomsy at yahoo.es (Jose R Valverde)
Date: Tue, 11 Oct 2005 16:26:20 +0200 (CEST)
Subject: [TUHS] Legality of porting ancient Unix.
Message-ID: <20051011142620.82167.qmail@web26103.mail.ukl.yahoo.com>

>Does anyone know where and how one obtains the
"ancient unix" sources, 
>particularly 32V?

Pardon?

Obviously, at TUHS!

        www.tuhs.org

particularly

       
ftp://minnie.tuhs.org/UnixArchive/VAX/Distributions/32V

                                j



		
______________________________________________ 
Renovamos el Correo Yahoo! 
Nuevos servicios, más seguridad 
http://correo.yahoo.es


From duncangareth at yahoo.co.uk  Wed Oct 12 00:30:17 2005
From: duncangareth at yahoo.co.uk (Duncan Anderson)
Date: Tue, 11 Oct 2005 16:30:17 +0200
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <XFMail.20051011161833.Hellwig.Geisse@mni.fh-giessen.de>
References: <XFMail.20051011161833.Hellwig.Geisse@mni.fh-giessen.de>
Message-ID: <434BCC79.7040303@yahoo.co.uk>

Hellwig.Geisse at mni.fh-giessen.de wrote:

>Hi Duncan,
>
>On 11-Oct-2005 Duncan Anderson wrote:
>  
>
>>Does anyone know where and how one obtains the "ancient unix" sources, 
>>particularly 32V?
>>
>>Who does one speak to?
>>
>>regards
>>Duncan
>>    
>>
>
>the best source I know of is
>http://www.tuhs.org/Archive/
>(not very surprising, I guess).
>In subdirectory VAX/Distributions/32V
>you find the file 32v_usr.tar.gz.
>Unpack this file, then go to the
>subdirectory /usr/src/sys.
>
>Regards,
>Hellwig
>  
>
Thanks.

I should have checked. The last time I looked, probably 2 years ago, I 
don't think it was there.

regards
Duncan

		
___________________________________________________________ 
To help you stay safe and secure online, we've developed the all new Yahoo! Security Centre. http://uk.security.yahoo.com


From duncangareth at yahoo.co.uk  Wed Oct 12 00:32:01 2005
From: duncangareth at yahoo.co.uk (Duncan Anderson)
Date: Tue, 11 Oct 2005 16:32:01 +0200
Subject: [TUHS] Legality of porting ancient Unix.
In-Reply-To: <20051011142620.82167.qmail@web26103.mail.ukl.yahoo.com>
References: <20051011142620.82167.qmail@web26103.mail.ukl.yahoo.com>
Message-ID: <434BCCE1.6020704@yahoo.co.uk>

Jose R Valverde wrote:

>>Does anyone know where and how one obtains the
>>    
>>
>"ancient unix" sources, 
>  
>
>>particularly 32V?
>>    
>>
>
>Pardon?
>
>Obviously, at TUHS!
>
>        www.tuhs.org
>
>particularly
>
>       
>ftp://minnie.tuhs.org/UnixArchive/VAX/Distributions/32V
>
>  
>
Right! Sorry, I should have checked. I haven't looked at the archives 
for years.

cheers
Duncan

	
	
		
___________________________________________________________ 
Yahoo! Messenger - NEW crystal clear PC to PC calling worldwide with voicemail http://uk.messenger.yahoo.com


From billcu1 at verizon.net  Wed Oct 12 05:46:04 2005
From: billcu1 at verizon.net (Bill Cunningham)
Date: Tue, 11 Oct 2005 15:46:04 -0400
Subject: [TUHS] Legality of porting ancient Unix.
References: <XFMail.20051011161833.Hellwig.Geisse@mni.fh-giessen.de>
	<434BCC79.7040303@yahoo.co.uk>
Message-ID: <002501c5ce9c$6b1b52e0$2f01a8c0@myhome.westell.com>


----- Original Message -----
From: "Duncan Anderson" <duncangareth at yahoo.co.uk>
To: <tuhs at minnie.tuhs.org>
Sent: Tuesday, October 11, 2005 10:30 AM
Subject: Re: [TUHS] Legality of porting ancient Unix.


> Hellwig.Geisse at mni.fh-giessen.de wrote:
>
> >Hi Duncan,
> >
> >On 11-Oct-2005 Duncan Anderson wrote:
> >
> >
> >>Does anyone know where and how one obtains the "ancient unix" sources,
> >>particularly 32V?
> >>
> >>Who does one speak to?
> >>
> >>regards
> >>Duncan

    Well Bell owned the old UNIXs. Ask Dennis or Ken. They probably know a
lot about that. Currently the Open Group ownes the UNIX trademark.

Bill





From billcu1 at verizon.net  Wed Oct 12 05:48:45 2005
From: billcu1 at verizon.net (Bill Cunningham)
Date: Tue, 11 Oct 2005 15:48:45 -0400
Subject: [TUHS] Legality of porting ancient Unix.
References: <XFMail.20051011083817.Hellwig.Geisse@mni.fh-giessen.de>
	<434BB1A0.1070603@yahoo.co.uk>
Message-ID: <003601c5ce9c$caf68220$2f01a8c0@myhome.westell.com>


----- Original Message ----- 
From: "Duncan Anderson" <duncangareth at yahoo.co.uk>
To: <tuhs at minnie.tuhs.org>
Sent: Tuesday, October 11, 2005 8:35 AM
Subject: Re: [TUHS] Legality of porting ancient Unix.


> Hellwig.Geisse at mni.fh-giessen.de wrote:
> 
> >Hi John,
> >
> >On 11-Oct-2005 John Chung wrote:
> >  
> >
> >>There has been a lot of talk about ancient unix
> >>lately. I do know there are quite a few ports for the
> >>ancient unix but the main question is it legal? It is
> >>possible to port and distribute the port without the
> >>warth of the company that owns the IP?
> >>
> >>Regards,
> >>John Chung
> >>    
> >>
> >
> >it is legal to use, modify, and distribute code
> >based on UNIX Versions 1..7 and 32V, to the best
> >of my knowledge; see
> >http://www.tuhs.org/Archive/Caldera-license.pdf
> >
> >Regards,
> >Hellwig Geisse

    Try The Open Group.

Bill




From txomsy at yahoo.es  Wed Oct 12 07:23:00 2005
From: txomsy at yahoo.es (Jose R Valverde)
Date: Tue, 11 Oct 2005 23:23:00 +0200 (CEST)
Subject: [TUHS] Legality of porting ancient UNIX
Message-ID: <20051011212300.48800.qmail@web26109.mail.ukl.yahoo.com>

Well, if I remember well, there was this little nifty
legal argument between ATT USL and UCB  BSDI in the
early '90s
that was settled out of court.

One of the factors that helped settle (again if I
remember well)
was that ATT had failed to adequately state its
Copyright 
on UNIX version 32V (may be more, my memory's weak)
that
had been distributed in source code, and hence those 
sources by the then current Copyright law, had fallen
in
the Public Domain.

Then, if my recollection is right (better look at the
documents on the case available on dmr's web page),
you
could do as you well damn please with those sources.

http://cm.bell-labs.com/cm/cs/who/dmr/bsdi/bsdisuit.html

>From one of the rulings:

"Consequently, I find that Plaintiff has failed to
demonstrate a likelihood that it can successfully
defend its copyright in 32V. Plaintiff's claims of
copyright violations are not a basis for injunctive
relief."

For others, the license otorgued by Caldera when they 
released the source (a BSD look-alike) would allow you
to as well to a large extent.

No need to go to the Open Group. Besides, they own the
trademark (i.e. you could not call the product UNIX
without their permission) but not the code (besides 
their own microkernel developments).

                          j



		
______________________________________________ 
Renovamos el Correo Yahoo! 
Nuevos servicios, más seguridad 
http://correo.yahoo.es


From imp at bsdimp.com  Wed Oct 12 07:39:16 2005
From: imp at bsdimp.com (M. Warner Losh)
Date: Tue, 11 Oct 2005 15:39:16 -0600 (MDT)
Subject: [TUHS] Legality of porting ancient UNIX
In-Reply-To: <20051011212300.48800.qmail@web26109.mail.ukl.yahoo.com>
References: <20051011212300.48800.qmail@web26109.mail.ukl.yahoo.com>
Message-ID: <20051011.153916.36665578.imp@bsdimp.com>

In message: <20051011212300.48800.qmail at web26109.mail.ukl.yahoo.com>
            Jose R Valverde <txomsy at yahoo.es> writes: One of the
: factors that helped settle (again if I remember well) was that ATT
: had failed to adequately state its Copyright on UNIX version 32V
: (may be more, my memory's weak) that had been distributed in source
: code, and hence those sources by the then current Copyright law, had
: fallen in the Public Domain.

The text you quoted makes it clear that the judge didn't rule that
they had fallen into the public domain.  He'd ruled that he didn't
think that AT&T would be successful at demonstrating a good copyright
title to the code if the matter went to trial.  A subtle difference,
to be sure, but an important one.

However, given that caldera released them with a bsd-like license, I
think the issue is effectively moot at this point...

Warner


From dubhthach at compsoc.nuigalway.ie  Thu Oct 13 08:50:52 2005
From: dubhthach at compsoc.nuigalway.ie (Paul Duffy)
Date: Wed, 12 Oct 2005 23:50:52 +0100 (IST)
Subject: [TUHS] Status of "32i"
Message-ID: <Pine.LNX.4.58.0510122349170.13259@frink.nuigalway.ie>

Hi,

Been reading through the list, just wondering did anything further come of
the whole 32V/i project? Last mail about it i see was back in April 2004.

-Paul

"There is no greater sorrow then to remember times of happiness when
miserable" -- Dante "The Inferno"


From brantley at coraid.com  Sat Oct 15 23:57:49 2005
From: brantley at coraid.com (Brantley Coile)
Date: Sat, 15 Oct 2005 09:57:49 -0400
Subject: [TUHS] Ancient unixes
Message-ID: <01b907ad0f646a3551b1b76472f03640@coraid.com>

this name `internet' name space was considered and rejected.  it's
harder than one would think to get details right for all networks, the
addess is only a small part of the information needed for the
connection, and keeping a name space for all the internet updated
would be very hard.  instead they use a network!machine!port syntax
with the dial command.

you can follow the full development of those ideas in the following papers.


http://cm.bell-labs.com/cm/who/dmr/spe.html
http://cm.bell-labs.com/sys/doc/net/net.html

remember.  seventh edition was relase in 1977.
Jimmy Carter was president, ``Anne Hall'' won best
picture, and the Chevy Nova was a big hit.



From brantley at coraid.com  Sun Oct 16 01:02:59 2005
From: brantley at coraid.com (Brantley Coile)
Date: Sat, 15 Oct 2005 11:02:59 -0400
Subject: [TUHS] Ancient unixes
In-Reply-To: <20051015.085932.17381905.imp@bsdimp.com>
Message-ID: <393475d42d1fea7d696658bfaeca5f0a@coraid.com>

oops.  sorry.  it is 

	http://cm.bell-labs.com/cm/cs/who/dmr/spe.html
-------------- next part --------------
An embedded message was scrubbed...
From: unknown sender
Subject: no subject
Date: no date
Size: 38
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20051015/4ebcd55d/attachment.mht>

From imp at bsdimp.com  Sun Oct 16 00:59:32 2005
From: imp at bsdimp.com (M. Warner Losh)
Date: Sat, 15 Oct 2005 08:59:32 -0600 (MDT)
Subject: [TUHS] Ancient unixes
In-Reply-To: <01b907ad0f646a3551b1b76472f03640@coraid.com>
References: <01b907ad0f646a3551b1b76472f03640@coraid.com>
Message-ID: <20051015.085932.17381905.imp@bsdimp.com>

In message: <01b907ad0f646a3551b1b76472f03640 at coraid.com>
            Brantley Coile <brantley at coraid.com> writes:
: http://cm.bell-labs.com/cm/who/dmr/spe.html

Is there maybe a typo here?  I get file not found.

Warner
--upas-qhcplmbvngjtdvuflagvgfqecp--


From hansolofalcon at worldnet.att.net  Sun Oct 16 01:49:16 2005
From: hansolofalcon at worldnet.att.net (Gregg C Levine)
Date: Sat, 15 Oct 2005 11:49:16 -0400
Subject: [TUHS] Ancient unixes
In-Reply-To: <393475d42d1fea7d696658bfaeca5f0a@coraid.com>
Message-ID: <001501c5d1a0$07ea5f20$6501a8c0@who7>

Hello from Gregg C Levine
Are you sure now?
I also get a 404 error message on the enclosed location.
---
Gregg C Levine hansolofalcon at worldnet.att.net
---
"Remember the Force will be with you. Always." Obi-Wan Kenobi 

> -----Original Message-----
> From: tuhs-bounces at minnie.tuhs.org
[mailto:tuhs-bounces at minnie.tuhs.org] On
> Behalf Of Brantley Coile
> Sent: Saturday, October 15, 2005 11:03 AM
> To: tuhs at minnie.tuhs.org
> Subject: Re: [TUHS] Ancient unixes
> 
> oops.  sorry.  it is
> 
> 	http://cm.bell-labs.com/cm/cs/who/dmr/spe.html



From brantley at coraid.com  Sun Oct 16 07:55:49 2005
From: brantley at coraid.com (Brantley Coile)
Date: Sat, 15 Oct 2005 17:55:49 -0400
Subject: [TUHS] Ancient unixes
In-Reply-To: <001501c5d1a0$07ea5f20$6501a8c0@who7>
Message-ID: <542ac21d33d242eb7648934ec208ef75@coraid.com>

Okay, for the third time. (maybe the charm?)

http://cm.bell-labs.com/cm/cs/who/dmr/spe.pdf
http://cm.bell-labs.com/cm/cs/who/dmr/ipcpaper.html

sorry for not checking the links before i posted the note.
-------------- next part --------------
An embedded message was scrubbed...
From: unknown sender
Subject: no subject
Date: no date
Size: 38
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20051015/d7a9e060/attachment.mht>

From hansolofalcon at worldnet.att.net  Sun Oct 16 01:49:16 2005
From: hansolofalcon at worldnet.att.net (Gregg C Levine)
Date: Sat, 15 Oct 2005 11:49:16 -0400
Subject: [TUHS] Ancient unixes
In-Reply-To: <393475d42d1fea7d696658bfaeca5f0a@coraid.com>
Message-ID: <001501c5d1a0$07ea5f20$6501a8c0@who7>

Hello from Gregg C Levine
Are you sure now?
I also get a 404 error message on the enclosed location.
---
Gregg C Levine hansolofalcon at worldnet.att.net
---
"Remember the Force will be with you. Always." Obi-Wan Kenobi 

> -----Original Message-----
> From: tuhs-bounces at minnie.tuhs.org
[mailto:tuhs-bounces at minnie.tuhs.org] On
> Behalf Of Brantley Coile
> Sent: Saturday, October 15, 2005 11:03 AM
> To: tuhs at minnie.tuhs.org
> Subject: Re: [TUHS] Ancient unixes
> 
> oops.  sorry.  it is
> 
> 	http://cm.bell-labs.com/cm/cs/who/dmr/spe.html

--upas-wpsmccvexzrkqetviipbreovzf--


From robertdkeys at aol.com  Wed Oct 19 13:00:34 2005
From: robertdkeys at aol.com (robertdkeys at aol.com)
Date: Tue, 18 Oct 2005 23:00:34 -0400
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
In-Reply-To: <001501c5d1a0$07ea5f20$6501a8c0@who7>
Message-ID: <8C7A270687203DA-1120-B77@MBLK-M39.sysops.aol.com>

Hello to the list, from a lurker, supposedly retired, but....
having run across one last MVIII crate for a lowly buck,
in surplus..... you know the rest.   It just has to run some
form of ancient BSD again.....(:+}}.....

Anyway, it is sans tape, and all my TK50 cartridges have
decomposed to dusty oxide, so it will have be resurrected
from something other than tape.

As luck has it, it has a scsi controller, and I happened to
have a scsi floppy from a decishbox long since gone to
the great bit crusher in the sky.  So, with some prodding
it booted off a NetBSD 1.4.1 vax floppy and was made
to run NetBSD 1.4.3 as a starter, to get the rest of the
early BSD bits onto the machine.  After making fsck trip
all over itself, and pushing some Tahoe and Reno bits
onto it, it now RUNS Tahoe (q0c variety) or Reno (archive
variety) quite happily.

The problem is that it won't install boot blocks that work.
None of the raboot/rdboot/bootra/bootrd combos get
any farther than the cryptic "loading boot" message.
The machine locks up hard then.  Yet, I can boot and run
things fine off a NetBSD 1.1A VAX tk50 boot file ("stand")
dd'd to that lonely scsi floppy off the far end of the bus,
with the usual b/3 duaX.... followed by "ra0a" as the root
and ^D to multiuser baby.  Years ago, the first time around,
I had this working fine, but, when I backed up the then
running system, I forgot (like a dummy) to dd off a set of
working boot blocks or a working bootable root.  Ahh, stupid
me.....(:+}}.....

The nearest that I can tell is that it is not writing (using the
disklabel executable) a correct set of block addresses to
find the real /boot and kernel.  Once, however done, the
kernel is actually loaded, it runs fine (like off NetBSD tape
boot "stand").

Any of you old timers got any thoughts as to where my suite
is going afoul of the real way?  It is probably something quite
simple, but, my greymatters just can't seem to get it figured
out.  Any insights are appreciated.

Thanks!

Bob Keys
Olde Pfarte with too many VAXentoyz.....(:+}}.....




From msokolov at ivan.Harhan.ORG  Wed Oct 19 13:52:49 2005
From: msokolov at ivan.Harhan.ORG (Michael Sokolov)
Date: Wed, 19 Oct 05 03:52:49 GMT
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
Message-ID: <0510190352.AA14346@ivan.Harhan.ORG>

robertdkeys at aol.com wrote:

> The problem is that it won't install boot blocks that work.
> None of the raboot/rdboot/bootra/bootrd combos get
> any farther than the cryptic "loading boot" message.

The "loading boot" message comes from the bootblock code and indicates
that the bootblocks are good and working.  If it stops there, it means
that you are missing the /boot file in the root filesystem (that's what
it's loading).

MS


From gregg.drwho8 at gmail.com  Wed Oct 19 14:25:13 2005
From: gregg.drwho8 at gmail.com (Gregg Levine)
Date: Wed, 19 Oct 2005 00:25:13 -0400
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
In-Reply-To: <0510190352.AA14346@ivan.Harhan.ORG>
References: <0510190352.AA14346@ivan.Harhan.ORG>
Message-ID: <18d205ed0510182125t372868abq6f5ed051bf67409f@mail.gmail.com>

Hello from Gregg C Levine
I have here a DEC VAXstation 3100M76.

It is in the same boat as the one Robert is writing about. I know I
can install NetBSD/vax on it using the net boot concept. But I'd like
to run one of the appropriate distributions from "our" collection. Any
suggestions?
--
Gregg C Levine gregg.drwho8 at gmail.com
"This signature was once found posting rude
messages in English in the Moscow subway."

On 10/18/05, Michael Sokolov <msokolov at ivan.harhan.org> wrote:
> robertdkeys at aol.com wrote:
>
> > The problem is that it won't install boot blocks that work.
> > None of the raboot/rdboot/bootra/bootrd combos get
> > any farther than the cryptic "loading boot" message.
>
> The "loading boot" message comes from the bootblock code and indicates
> that the bootblocks are good and working.  If it stops there, it means
> that you are missing the /boot file in the root filesystem (that's what
> it's loading).
>
> MS
> _______________________________________________
> TUHS mailing list
> TUHS at minnie.tuhs.org
> http://minnie.tuhs.org/mailman/listinfo/tuhs
>


From robertdkeys at aol.com  Thu Oct 20 03:28:31 2005
From: robertdkeys at aol.com (robertdkeys at aol.com)
Date: Wed, 19 Oct 2005 13:28:31 -0400
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
In-Reply-To: <0510190352.AA14346@ivan.Harhan.ORG>
References: <0510190352.AA14346@ivan.Harhan.ORG>
Message-ID: <8C7A2E9A944405A-3FC-1254@mblk-d42.sysops.aol.com>

 The /boot is there, so it is somewhere between the bootblocks
and /boot that the connection is lost.  The /boot is apparently
not correctly found.  But, it is there......
 
Bob Keys
 
-----Original Message-----

> The problem is that it won't install boot blocks that work.
> None of the raboot/rdboot/bootra/bootrd combos get
> any farther than the cryptic "loading boot" message.

The "loading boot" message comes from the bootblock code and indicates
that the bootblocks are good and working.  If it stops there, it means
that you are missing the /boot file in the root filesystem (that's what
it's loading).

MS
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20051019/761b59ef/attachment.html>

From arnold at skeeve.com  Thu Oct 20 17:36:52 2005
From: arnold at skeeve.com (Aharon Robbins)
Date: Thu, 20 Oct 2005 09:36:52 +0200
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
Message-ID: <200510200736.j9K7aqga003340@skeeve.com>

Wasn't there an "installboot" program that told the bootblock where
to find the /boot file?

Boy was it a lllloooonnnngggg time ago that I dealt with this stuff.

Arnold

> Date: Wed, 19 Oct 2005 13:28:31 -0400
> From: robertdkeys at aol.com
> Subject: Re: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
> To: msokolov at ivan.Harhan.ORG, tuhs at minnie.tuhs.org
>
>  The /boot is there, so it is somewhere between the bootblocks
> and /boot that the connection is lost.  The /boot is apparently
> not correctly found.  But, it is there......
>  
> Bob Keys
>  
> -----Original Message-----
>
> > The problem is that it won't install boot blocks that work.
> > None of the raboot/rdboot/bootra/bootrd combos get
> > any farther than the cryptic "loading boot" message.
>
> The "loading boot" message comes from the bootblock code and indicates
> that the bootblocks are good and working.  If it stops there, it means
> that you are missing the /boot file in the root filesystem (that's what
> it's loading).
>
> MS


From msokolov at ivan.Harhan.ORG  Fri Oct 21 02:57:53 2005
From: msokolov at ivan.Harhan.ORG (Michael Sokolov)
Date: Thu, 20 Oct 05 16:57:53 GMT
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
Message-ID: <0510201657.AA16789@ivan.Harhan.ORG>

Aharon Robbins <arnold at skeeve.com> wrote:

> Wasn't there an "installboot" program that told the bootblock where
> to find the /boot file?

The installboot program in the original 4.3BSD, whose function has been
incorporated into disklabel(8) in 4.3-Tahoe/Quasijarus, writes the boot
blocks to the disk, but it does not patch them with the location of
/boot, the bootblock code is smart enough to understand the filesystem.

As for Robert's problem, I don't know where he got screwed - but man,
use your head, what do you think your god-given brain is for?  You can
single-step through the code with the MicroVAX ROM monitor's N command,
you can put some printf's in the code to see where it dies, etc, the
possibilities are limitless.  Just debug it the same way you would debug
any other problem.  What do you think I do when I get a similar
mysterious snafu?  I debug it like a real programmer, don't go crying to
a mailing list.

MS


From robertdkeys at aol.com  Fri Oct 21 01:04:04 2005
From: robertdkeys at aol.com (robertdkeys at aol.com)
Date: Thu, 20 Oct 2005 11:04:04 -0400
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
In-Reply-To: <200510200736.j9K7aqga003340@skeeve.com>
References: <200510200736.j9K7aqga003340@skeeve.com>
Message-ID: <8C7A39EA5BD08D9-E80-4162@FWM-R06.sysops.aol.com>

 Yes, there is an installboot buried, uncompiled in the sources that is pretty
much the same identical source from 4.2-4.4.  I have dug that out, just in case.
But, disklabel, I was thinking was supposed to handle that.  Maybe it doesn't
do that correctly?  Could there be anything in the hardware settings (CMD
controller)?  I wouldn't think so, since all the other OS's work fine.  I have a
set of root dd images from a friend's working machine.  Maybe I should hexdump
the front end of the drives and compare them to see where things have gone
afoul.  I haven't actively read hexdumps in years, so that ought to be fun
to try.....(:+\\.....
 
Anyone remember which exact disklabel or installboot incantation was used?
I recall that the first boot sector had to be rdboot on a MicroVAX, but the
second boot block could be rdboot or raboot.  I tried both combos of rd+rd
or rd+ra and never got past the "loading boot" msg.  /boot is there in the
root fs.
 
What is curious is that these things supposedly boot fine in simh, but on
my real hardware (and a friend's VAX, too), they don't.
 
Could it be anything strange like odd bytes carried over in the bootblock or
disklabel area or start of the fs causing it to lock up?  Probably not, but,
stranger things have happened.....  Maybe a dd fill of the front few tracks
of the drive might help?
 
Since I am not installing from tape, but manually from another OS, could
there be anything in the install scripts I am inadvertently missing?
 
So close and yet so far....
 
Bob Keys
 
 
-----Original Message-----

Wasn't there an "installboot" program that told the bootblock where
to find the /boot file?

Boy was it a lllloooonnnngggg time ago that I dealt with this stuff.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://minnie.tuhs.org/pipermail/tuhs/attachments/20051020/b821b1b7/attachment.html>

From jkunz at unixag-kl.fh-kl.de  Fri Oct 21 17:52:41 2005
From: jkunz at unixag-kl.fh-kl.de (Jochen Kunz)
Date: Fri, 21 Oct 2005 09:52:41 +0200
Subject: [TUHS] Bringing up any 4.3BSD on a MicroVAX without tape....
In-Reply-To: <8C7A39EA5BD08D9-E80-4162@FWM-R06.sysops.aol.com>
References: <200510200736.j9K7aqga003340@skeeve.com>
	<8C7A39EA5BD08D9-E80-4162@FWM-R06.sysops.aol.com>
Message-ID: <20051021095241.198c5ef9.jkunz@unixag-kl.fh-kl.de>

On Thu, 20 Oct 2005 11:04:04 -0400
robertdkeys at aol.com wrote:

> Could there be anything in the hardware settings (CMD controller)?
I dim remember that the CMD SCSI MSCP controllers behave slightly
different then the DEC MSCP controllers. I think NetBSD had problems
with some CMD SCSI MSCP controllers at one point in time. Maybe you can
scan the port-vax at NetBSD mailing list archive...
-- 


tschüß,
       Jochen

Homepage: http://www.unixag-kl.fh-kl.de/~jkunz/



From txomsy at yahoo.es  Fri Oct 21 22:31:52 2005
From: txomsy at yahoo.es (Jose R Valverde)
Date: Fri, 21 Oct 2005 14:31:52 +0200 (CEST)
Subject: [TUHS]  Bringing up any 4.3BSD on a MicroVAX without tape....
Message-ID: <20051021123153.82844.qmail@web26110.mail.ukl.yahoo.com>

>It is in the same boat as the one Robert is writing 
>about. I know I can install NetBSD/vax on it using
the 
>net boot concept. But I'd like to run one of the 
>appropriate distributions from "our" collection. Any
>suggestions?

The most obivous ones are

 - Quasijarus
 - Ultrix-32M
   -- ultrix 1.2 is in the archives
   -- from ifctvax.harhan.org you can get sources for
      ultrix 2.0.0
      ultrix 4.2.0
   (see previous posts in the list)
 - 32V


                           j



		
______________________________________________ 
Renovamos el Correo Yahoo! 
Nuevos servicios, más seguridad 
http://correo.yahoo.es


